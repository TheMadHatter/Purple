﻿namespace Purple.AStar
{
	public interface IWeightAlterable<T>
	{
		T Weight { get; set; }
	}
}