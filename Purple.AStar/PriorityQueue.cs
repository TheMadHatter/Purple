﻿
#region

using System.Collections.Generic;
using System.Linq;

#endregion

namespace Purple.AStar.Algorithm
{
	class PriorityQueue<T, TX> where T : IWeightAddable<TX>
	{
		public List<T> InnerList;
		protected IComparer<T> MComparer;

		public PriorityQueue(IComparer<T> comparer, int size)
		{
			MComparer = comparer;
			InnerList = new List<T>(size);
		}

		protected virtual int OnCompare(int i, int j) { return MComparer.Compare(InnerList[i], InnerList[j]); }

		private int BinarySearch(T value)
		{
			int low = 0, high = InnerList.Count - 1;

			while (low <= high)
			{
				var midpoint = (low + high) / 2;

				// check to see if value is equal to item in array
				if (MComparer.Compare(value, InnerList[midpoint]) == 0)
					return midpoint;
				if (MComparer.Compare(value, InnerList[midpoint]) == -1)
					high = midpoint - 1;
				else
					low = midpoint + 1;
			}

			// item was not found
			return low;
		}
			
		public void Push(T item)
		{
			var location = BinarySearch(item);
			InnerList.Insert(location, item);
		}
			
		public T Pop()
		{
			if (!InnerList.Any())
				return default(T);
			var item = InnerList[0];
			InnerList.RemoveAt(0);
			return item;
		}

		public void Update(T element, TX newValue)
		{
			InnerList.RemoveAt(BinarySearch(element));
			element.WeightChange = newValue;
			Push(element);
		}
	}
}