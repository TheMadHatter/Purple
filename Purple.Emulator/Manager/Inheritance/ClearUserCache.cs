﻿using System;
using System.Collections.Generic;
using AzureSharp.HabboHotel.Users;

namespace AzureSharp.Manager.Inheritance
{
    class ClearUserCache : ICacheable
    {
        public void Parse()
        {
            var HabbosToRemvoe = new List<uint>();

            foreach (var Habbo in PurpleEmulator.UsersCached)
            {
                if (Habbo.Value == null)
                {
                    HabbosToRemvoe.Add(Habbo.Key);
                    return;
                }

                if (PurpleEmulator.GetGame().GetClientManager().Clients.ContainsKey(Habbo.Key)) continue;

                if ((DateTime.Now - Habbo.Value.LastUsed).TotalMilliseconds < 1800000) continue;

                HabbosToRemvoe.Add(Habbo.Key);
            }

            foreach (var HabboId in HabbosToRemvoe)
            {
                Habbo Habbo;

                if (!PurpleEmulator.UsersCached.TryRemove(HabboId, out Habbo))
                {
                    // No need to throw an exception, the user must've logged out before the function finished.
                }
            }
        }
    }
}