﻿using System;
using System.Collections.Generic;
using AzureSharp.HabboHotel.Rooms;

namespace AzureSharp.Manager.Inheritance
{
    // TODO: ...
    class ClearRoomsCache : ICacheable
    {
        public void Parse()
        {
            if (PurpleEmulator.GetGame()?.GetRoomManager()?.LoadedRoomData == null) return;

            var RoomsToRemove = new List<uint>();

            foreach (var Room in PurpleEmulator.GetGame().GetRoomManager().LoadedRoomData)
            {
                if (Room.Value.UsersNow <= 0) continue;

                if ((DateTime.Now - Room.Value.LastUsed).TotalMilliseconds < 1800000) continue;

                RoomsToRemove.Add(Room.Key);
            }

            foreach (var RoomId in RoomsToRemove)
            {
                RoomData Room;

                if (!PurpleEmulator.GetGame().GetRoomManager().LoadedRoomData.TryRemove(RoomId, out Room))
                {
                    // No need to throw an exception, the room must've unloaded before the function finished.
                }
            }
        }
    }
}