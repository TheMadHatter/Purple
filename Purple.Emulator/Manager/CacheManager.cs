﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using AzureSharp.Manager.Inheritance;


namespace AzureSharp.Manager
{
    sealed class CacheManager : IDisposable
    {

        /// Default.
        /// </summary>
        public CacheManager()
        {
            storage = new List<ICacheable>();
            storage.Add(new ClearRoomsCache());
            storage.Add(new ClearUserCache());

            backgroundWorker = new Thread(Run);
            backgroundWorker.Priority = ThreadPriority.Lowest;
            backgroundWorker.Start();
        }

        /// <summary>
        /// Used to grab the items which need to be reloaded.
        /// </summary>
        private List<ICacheable> storage { get; }

        /// <summary>
        /// Used for running our task without backing up the main thread. 
        /// </summary>
        private Thread backgroundWorker { get; set; }

        /// <summary>
        /// Is the thread ready to be disposed of?
        /// </summary>
        private bool IsReady { get; set; }

        /// <summary> 
        /// Safe/readonly access to the list of items which need to be reloaded.
        /// </summary>
        public List<ICacheable> Storage
        {
            get { return storage; }
        }

        /// <summary>
        /// Is the backgroundWorker running?
        /// </summary>
        public bool IsRunning
        {
            get { return backgroundWorker.IsAlive; }
        }

        /// <summary>
        /// Stop the background thread in an organized fashion.
        /// </summary>
        public void Dispose()
        {
            // Wait for the thread to finish its cycle.
            while (IsReady)
            {
                if (!IsRunning)
                {
                    break; // So we don't abort the thread twice.
                }

                backgroundWorker.Abort();
            }

            backgroundWorker = null;
        }

        private void Run()
        {
            while (IsRunning)
            {
                IsReady = false;

                foreach (var Items in storage)
                {
                    Items.Parse();
                }
                Logger.Debug("CacheManagers executed.");

                IsReady = true;
                Thread.Sleep(1000000);
            }
        }
    }
}