﻿#region

using System.Collections.Generic;
using AzureSharp.HabboHotel.Items;
using AzureSharp.HabboHotel.Rooms.Games;

#endregion

namespace AzureSharp.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class JoinTeam : IWiredItem
    {
        //private List<InteractionType> mBanned;
        public JoinTeam(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Delay = 0;
            //this.mBanned = new List<InteractionType>();
        }

        public Interaction Type
        {
            get { return Interaction.ActionJoinTeam; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; set; }

        public List<RoomItem> Items
        {
            get { return new List<RoomItem>(); }
            set { }
        }

        public int Delay { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            if (stuff[0] == null) return false;
            var roomUser = (RoomUser) stuff[0];
            var team = Delay / 500;
            var t = roomUser.GetClient().GetHabbo().CurrentRoom.GetTeamManagerForFreeze();
            if (roomUser.Team != Team.none)
            {
                t.OnUserLeave(roomUser);
                roomUser.Team = Team.none;
            }
            switch (team)
            {
                case 1:
                    roomUser.Team = Team.red;
                    break;

                case 2:
                    roomUser.Team = Team.green;
                    break;

                case 3:
                    roomUser.Team = Team.blue;
                    break;

                case 4:
                    roomUser.Team = Team.yellow;
                    break;
            }
            t.AddUser(roomUser);
            roomUser.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(Delay + 39);
            //InteractionType item = (InteractionType)stuff[1];

            return true;
        }
    }
}