#region

using System;
using System.Linq;
using AzureSharp.HabboHotel.Items;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;


#endregion

namespace AzureSharp.HabboHotel.Rooms
{
    internal class Trade
    {
        private readonly uint _oneId;
        private readonly uint _roomId;
        private readonly uint _twoId;
        private readonly TradeUser[] _users;
        private int _tradeStage;

        internal Trade(uint userOneId, uint userTwoId, uint roomId)
        {
            _oneId = userOneId;
            _twoId = userTwoId;
            _users = new TradeUser[2];
            _users[0] = new TradeUser(userOneId, roomId);
            _users[1] = new TradeUser(userTwoId, roomId);
            _tradeStage = 1;
            _roomId = roomId;
            var users = _users;
            foreach (var tradeUser in users.Where(tradeUser => !tradeUser.GetRoomUser().Statusses.ContainsKey("trd")))
            {
                tradeUser.GetRoomUser().AddStatus("trd", "");
                tradeUser.GetRoomUser().UpdateNeeded = true;
            }
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeStartMessageComposer"));
            serverMessage.AppendInteger(userOneId);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(userTwoId);
            serverMessage.AppendInteger(1);
            SendMessageToUsers(serverMessage);
        }

        internal bool AllUsersAccepted
        {
            get
            {
                {
                    return _users.All(t => t == null || t.HasAccepted);
                }
            }
        }

        internal bool ContainsUser(uint id)
        {
            {
                return _users.Any(t => t != null && t.UserId == id);
            }
        }

        internal TradeUser GetTradeUser(uint id)
        {
            {
                return _users.FirstOrDefault(t => t != null && t.UserId == id);
            }
        }

        internal void OfferItem(uint userId, UserItem item)
        {
            var tradeUser = GetTradeUser(userId);
            if (tradeUser == null || item == null || !item.BaseItem.AllowTrade || tradeUser.HasAccepted || _tradeStage != 1)
            {
                return;
            }
            ClearAccepted();
            if (!tradeUser.OfferedItems.Contains(item))
            {
                tradeUser.OfferedItems.Add(item);
            }
            UpdateTradeWindow();
        }

        internal void TakeBackItem(uint userId, UserItem item)
        {
            var tradeUser = GetTradeUser(userId);
            if (tradeUser == null || item == null || tradeUser.HasAccepted || _tradeStage != 1)
            {
                return;
            }
            ClearAccepted();
            tradeUser.OfferedItems.Remove(item);
            UpdateTradeWindow();
        }

        internal void Accept(uint userId)
        {
            var tradeUser = GetTradeUser(userId);
            if (tradeUser == null || _tradeStage != 1)
            {
                return;
            }
            tradeUser.HasAccepted = true;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(1);
            SendMessageToUsers(serverMessage);

            {
                if (!AllUsersAccepted)
                {
                    return;
                }
                SendMessageToUsers(new ServerMessage(LibraryParser.OutgoingRequest("TradeConfirmationMessageComposer")));
                _tradeStage++;
                ClearAccepted();
            }
        }

        internal void Unaccept(uint userId)
        {
            var tradeUser = GetTradeUser(userId);
            if (tradeUser == null || _tradeStage != 1 || AllUsersAccepted)
            {
                return;
            }
            tradeUser.HasAccepted = false;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(0);
            SendMessageToUsers(serverMessage);
        }

        internal void CompleteTrade(uint userId)
        {
            var tradeUser = GetTradeUser(userId);
            if (tradeUser == null || _tradeStage != 2)
            {
                return;
            }
            tradeUser.HasAccepted = true;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(1);
            SendMessageToUsers(serverMessage);
            if (!AllUsersAccepted)
            {
                return;
            }
            _tradeStage = 999;
            Finnito();
        }

        internal void ClearAccepted()
        {
            var users = _users;
            foreach (var tradeUser in users)
            {
                tradeUser.HasAccepted = false;
            }
        }

        internal void UpdateTradeWindow()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeUpdateMessageComposer"));

            {
                foreach (var tradeUser in _users.Where(tradeUser => tradeUser != null))
                {
                    serverMessage.AppendInteger(tradeUser.UserId);
                    serverMessage.AppendInteger(tradeUser.OfferedItems.Count);
                    foreach (var current in tradeUser.OfferedItems)
                    {
                        serverMessage.AppendInteger(current.Id);
                        serverMessage.AppendString(current.BaseItem.Type.ToString().ToLower());
                        serverMessage.AppendInteger(current.Id);
                        serverMessage.AppendInteger(current.BaseItem.SpriteId);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendBool(true);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendString("");
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        if (current.BaseItem.Type == 's')
                        {
                            serverMessage.AppendInteger(0);
                        }
                    }
                }
                SendMessageToUsers(serverMessage);
            }
        }

        internal void DeliverItems()
        {
            var offeredItems = GetTradeUser(_oneId).OfferedItems;
            var offeredItems2 = GetTradeUser(_twoId).OfferedItems;
            if (offeredItems.Any(current => GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().GetItem(current.Id) == null))
            {
                GetTradeUser(_oneId).GetClient().SendNotif("El tradeo ha fallado.");
                GetTradeUser(_twoId).GetClient().SendNotif("El tradeo ha fallado.");
                return;
            }
            if (offeredItems2.Any(current2 => GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().GetItem(current2.Id) == null))
            {
                GetTradeUser(_oneId).GetClient().SendNotif("El tradeo ha fallado.");
                GetTradeUser(_twoId).GetClient().SendNotif("El tradeo ha fallado.");
                return;
            }
            GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            foreach (var current3 in offeredItems)
            {
                GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().RemoveItem(current3.Id, false);
                GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().AddNewItem(current3.Id, current3.BaseItemId, current3.ExtraData, current3.GroupId, false, false, 0, 0, current3.SongCode);
                GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
                GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            }
            foreach (var current4 in offeredItems2)
            {
                GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().RemoveItem(current4.Id, false);
                GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().AddNewItem(current4.Id, current4.BaseItemId, current4.ExtraData, current4.GroupId, false, false, 0, 0, current4.SongCode);
                GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
                GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            }
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage.AppendInteger(1);
            var i = 1;
            if (offeredItems.Any(current5 => current5.BaseItem.Type.ToString().ToLower() != "s"))
            {
                i = 2;
            }
            serverMessage.AppendInteger(i);
            serverMessage.AppendInteger(offeredItems.Count);
            foreach (var current6 in offeredItems)
            {
                serverMessage.AppendInteger(current6.Id);
            }
            GetTradeUser(_twoId).GetClient().SendMessage(serverMessage);
            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage2.AppendInteger(1);
            i = 1;
            if (offeredItems2.Any(current7 => current7.BaseItem.Type.ToString().ToLower() != "s"))
            {
                i = 2;
            }
            serverMessage2.AppendInteger(i);
            serverMessage2.AppendInteger(offeredItems2.Count);
            foreach (var current8 in offeredItems2)
            {
                serverMessage2.AppendInteger(current8.Id);
            }
            GetTradeUser(_oneId).GetClient().SendMessage(serverMessage2);
            GetTradeUser(_oneId).GetClient().GetHabbo().GetInventoryComponent().UpdateItems(false);
            GetTradeUser(_twoId).GetClient().GetHabbo().GetInventoryComponent().UpdateItems(false);
        }

        internal void CloseTradeClean()
        {
            {
                foreach (var tradeUser in _users.Where(tradeUser => tradeUser != null && tradeUser.GetRoomUser() != null))
                {
                    tradeUser.GetRoomUser().RemoveStatus("trd");
                    tradeUser.GetRoomUser().UpdateNeeded = true;
                }
                SendMessageToUsers(new ServerMessage(LibraryParser.OutgoingRequest("TradeCompletedMessageComposer")));
                GetRoom().ActiveTrades.Remove(this);
            }
        }

        internal void CloseTrade(uint userId)
        {
            {
                foreach (var tradeUser in _users.Where(tradeUser => tradeUser != null && tradeUser.GetRoomUser() != null))
                {
                    tradeUser.GetRoomUser().RemoveStatus("trd");
                    tradeUser.GetRoomUser().UpdateNeeded = true;
                }
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeCloseMessageComposer"));
                serverMessage.AppendInteger(userId);
                serverMessage.AppendInteger(0);
                SendMessageToUsers(serverMessage);
            }
        }

        internal void SendMessageToUsers(ServerMessage message)
        {
            if (_users == null)
            {
                return;
            }

            {
                foreach (var tradeUser in _users.Where(tradeUser => tradeUser != null && tradeUser.GetClient() != null))
                {
                    tradeUser.GetClient().SendMessage(message);
                }
            }
        }

        private void Finnito()
        {
            try
            {
                DeliverItems();
                CloseTradeClean();
            }
            catch (Exception ex)
            {
                Logger.Error("Trade task", ex);
            }
        }

        private Room GetRoom()
        {
            return PurpleEmulator.GetGame().GetRoomManager().GetRoom(_roomId);
        }
    }
}