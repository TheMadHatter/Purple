#region

using System;
using AzureSharp.Messages;

#endregion

namespace AzureSharp.HabboHotel.Rooms
{
    internal class Chatlog
    {
        internal bool IsSaved;
        internal string Message;
        internal DateTime TimeStamp;
        internal uint UserId;

        internal Chatlog(uint user, string msg, DateTime time, bool fromDatabase, uint roomId)
        {
            UserId = user;
            Message = msg;
            TimeStamp = time;
            IsSaved = fromDatabase;
            if (!IsSaved) Save(roomId);
        }

        internal void Save(uint RoomId)
        {
            using (var queryChunk = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryChunk.SetQuery("INSERT INTO users_chatlogs (user_id, room_id, timestamp, message) VALUES (@user, @room, @time, @message)");
                queryChunk.AddParameter("user", UserId);
                queryChunk.AddParameter("room", RoomId);
                queryChunk.AddParameter("time", Utility.DateTimeToUnix(TimeStamp));
                queryChunk.AddParameter("message", Message);
                queryChunk.RunQuery();
            }
            IsSaved = true;
        }

        internal void Serialize(ref ServerMessage message)
        {
            var habbo = PurpleEmulator.GetHabboById(UserId);
            message.AppendInteger(Utility.DifferenceInMilliSeconds(TimeStamp, DateTime.Now));
            message.AppendInteger(UserId);
            message.AppendString(habbo == null ? "*User not found*" : habbo.UserName);
            message.AppendString(Message);
            message.AppendBool(true);
        }
    }
}