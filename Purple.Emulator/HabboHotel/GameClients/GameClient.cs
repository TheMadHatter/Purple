#region

using System;
using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.Connection.Connection;
using AzureSharp.Connection.Net;
using AzureSharp.HabboHotel.Misc;
using AzureSharp.HabboHotel.Users;
using AzureSharp.HabboHotel.Users.UserDataManagement;
using AzureSharp.Messages;
using AzureSharp.Messages.Handlers;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.GameClients
{
    public class GameClient
    {
        private ConnectionInformation _connection;
        private bool _disconnected;
        private Habbo _habbo;
        private GameClientMessageHandler _messageHandler;
        internal int CurrentRoomUserId;
        internal int DesignedHandler = 1;
        internal string MachineId;
        internal GamePacketParser PacketParser;
        internal byte PublicistCount;
        internal DateTime TimePingedReceived;

        internal GameClient(uint clientId, ConnectionInformation connection)
        {
            ConnectionId = clientId;
            _connection = connection;
            CurrentRoomUserId = -1;
            PacketParser = new GamePacketParser(this);
        }

        internal uint ConnectionId { get; private set; }

        internal ConnectionInformation GetConnection()
        {
            return _connection;
        }

        internal GameClientMessageHandler GetMessageHandler()
        {
            return _messageHandler;
        }

        internal Habbo GetHabbo()
        {
            return _habbo;
        }
            
        internal void StartConnection()
        {
            if (_connection == null) return;
            TimePingedReceived = DateTime.Now;
            (_connection.Parser as InitialPacketParser).PolicyRequest += PolicyRequest;
            (_connection.Parser as InitialPacketParser).SwitchParserRequest += SwitchParserRequest;
            _connection.StartPacketProcessing();
        }

        internal void InitHandler()
        {
            _messageHandler = new GameClientMessageHandler(this);
        }

        internal bool TryLogin(string authTicket)
        {
            try
            {
                var ip = GetConnection().GetIp();
                uint errorCode = 0;
                var userData = UserDataFactory.GetUserData(authTicket, out errorCode);
                if (errorCode == 1 || errorCode == 2)
                {
                    Logger.Debug("Disconnecting an user with an invalid SSO ticket.");
                    return false;
                }
                PurpleEmulator.GetGame().GetClientManager().RegisterClient(this, userData.UserId, userData.User.UserName);
                _habbo = userData.User;
                userData.User.LoadData(userData);
                var banReason = PurpleEmulator.GetGame().GetBanManager().GetBanReason(userData.User.UserName, ip, MachineId);

                if (!string.IsNullOrEmpty(banReason) || userData.User.UserName == null)
                {
                    SendNotifWithScroll(banReason);
                    using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.SetQuery($"SELECT ip_last FROM users WHERE id={GetHabbo().Id} LIMIT 1");
                        var @string = queryReactor.GetString();
                        queryReactor.SetQuery($"SELECT COUNT(0) FROM users_bans_access WHERE user_id={_habbo.Id} LIMIT 1");
                        var integer = queryReactor.GetInteger();
                        if (integer > 0) queryReactor.RunFastQuery("UPDATE users_bans_access SET attempts = attempts + 1, ip='" + @string + "' WHERE user_id=" + GetHabbo().Id + " LIMIT 1");
                        else queryReactor.RunFastQuery("INSERT INTO users_bans_access (user_id, ip) VALUES (" + GetHabbo().Id + ", '" + @string + "')");
                    }
                    return false;
                }

                userData.User.Init(this, userData);
                var queuedServerMessage = new QueuedServerMessage(_connection);
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UniqueMachineIDMessageComposer"));
                serverMessage.AppendString(MachineId);
                queuedServerMessage.AppendResponse(serverMessage);
                queuedServerMessage.AppendResponse(new ServerMessage(LibraryParser.OutgoingRequest("AuthenticationOKMessageComposer")));
                if (_habbo != null)
                {
                    var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("HomeRoomMessageComposer"));
                    serverMessage2.AppendInteger(_habbo.HomeRoom);
                    serverMessage2.AppendInteger(_habbo.HomeRoom);
                    queuedServerMessage.AppendResponse(serverMessage2);
                }
                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("MinimailCountMessageComposer"));
                serverMessage.AppendInteger(_habbo.MinimailUnreadMessages);
                queuedServerMessage.AppendResponse(serverMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FavouriteRoomsMessageComposer"));
                serverMessage.AppendInteger(30);

                if (userData.User.FavoriteRooms == null || !userData.User.FavoriteRooms.Any()) serverMessage.AppendInteger(0);
                else
                {
                    serverMessage.AppendInteger(userData.User.FavoriteRooms.Count);
                    foreach (var i in userData.User.FavoriteRooms) serverMessage.AppendInteger(i);
                }
                queuedServerMessage.AppendResponse(serverMessage);

                var rightsMessage = new ServerMessage(LibraryParser.OutgoingRequest("UserClubRightsMessageComposer"));
                rightsMessage.AppendInteger(userData.User.GetSubscriptionManager().HasSubscription ? 2 : 0);
                rightsMessage.AppendInteger(userData.User.Rank);
                rightsMessage.AppendInteger(0);
                queuedServerMessage.AppendResponse(rightsMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("EnableNotificationsMessageComposer"));
                serverMessage.AppendBool(true); //isOpen
                serverMessage.AppendBool(false);
                queuedServerMessage.AppendResponse(serverMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("EnableTradingMessageComposer"));
                serverMessage.AppendBool(true);
                queuedServerMessage.AppendResponse(serverMessage);
                userData.User.UpdateCreditsBalance();

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
                serverMessage.AppendInteger(2);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(userData.User.ActivityPoints);
                serverMessage.AppendInteger(5);
                serverMessage.AppendInteger(userData.User.Diamonds);
                queuedServerMessage.AppendResponse(serverMessage);

                if (userData.User.HasFuse("support_tool")) queuedServerMessage.AppendResponse(PurpleEmulator.GetGame().GetModerationTool().SerializeTool(this));
                queuedServerMessage.AppendResponse(PurpleEmulator.GetGame().GetAchievementManager().AchievementDataCached);

                if (!GetHabbo().NuxPassed && ExtraSettings.NEW_users_gifts_ENABLED) queuedServerMessage.AppendResponse(new ServerMessage(LibraryParser.OutgoingRequest("NuxSuggestFreeGiftsMessageComposer")));
                queuedServerMessage.AppendResponse(GetHabbo().GetAvatarEffectsInventoryComponent().GetPacket());
                queuedServerMessage.SendResponse();

                PurpleEmulator.GetGame().GetAchievementManager().TryProgressHabboClubAchievements(this);
                PurpleEmulator.GetGame().GetAchievementManager().TryProgressRegistrationAchievements(this);
                PurpleEmulator.GetGame().GetAchievementManager().TryProgressLoginAchievements(this);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("Bug during user login.", ex);
            }
            return false;
        }

        internal void SendNotifWithScroll(string message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("MOTDNotificationMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendString(message);
            SendMessage(serverMessage);
        }

        internal void SendNotifWithPicture(string msg, string title, string picture, string link = "", string linktitle = "")
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString(picture);
            serverMessage.AppendInteger(link != "" ? 4 : 2);
            serverMessage.AppendString("title");
            serverMessage.AppendString(title);
            serverMessage.AppendString("message");
            serverMessage.AppendString(msg);
            if (link != "")
            {
                //for now
            }
            SendMessage(serverMessage);
        }

        internal void SendBroadcastMessage(string message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
            serverMessage.AppendString(message);
            serverMessage.AppendString(string.Empty);
            SendMessage(serverMessage);
        }

        internal void SendModeratorMessage(string message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
            serverMessage.AppendString(message);
            serverMessage.AppendString(string.Empty);
            SendMessage(serverMessage);
        }

        internal void SendWhisper(string message, bool fromWired = false)
        {
            if (GetHabbo() == null || GetHabbo().CurrentRoom == null) return;
            var roomUserByHabbo = GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().UserName);
            if (roomUserByHabbo == null) return;
            var whisp = new ServerMessage(LibraryParser.OutgoingRequest("WhisperMessageComposer"));
            whisp.AppendInteger(roomUserByHabbo.VirtualId);
            whisp.AppendString(message);
            whisp.AppendInteger(0);
            whisp.AppendInteger(fromWired ? 34 : roomUserByHabbo.LastBubble);
            whisp.AppendInteger(0);
            whisp.AppendInteger(fromWired);
            SendMessage(whisp);
        }

        internal void SendNotif(string message, string title = "Notification", string picture = "")
        {
            SendMessage(GetBytesNotif(message, title, picture));
        }

        public static byte[] GetBytesNotif(string message, string title = "Notification", string picture = "")
        {
            using (var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer")))
            {
                serverMessage.AppendString(picture);
                serverMessage.AppendInteger(4);
                serverMessage.AppendString("title");
                serverMessage.AppendString(title);
                serverMessage.AppendString("message");
                serverMessage.AppendString(message);
                serverMessage.AppendString("linkUrl");
                serverMessage.AppendString("event:");
                serverMessage.AppendString("linkTitle");
                serverMessage.AppendString("ok");

                return serverMessage.GetReversedBytes();
            }
        }

        internal void Stop()
        {
            if (GetMessageHandler() != null)
                _messageHandler.Destroy();
            if (GetHabbo() != null)
                _habbo.OnDisconnect("disconnect");
            CurrentRoomUserId = -1;
            _messageHandler = null;
            _habbo = null;
            _connection = null;
        }

        internal void Disconnect(string reason)
        {
            if (GetHabbo() != null)
            {
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryReactor.RunFastQuery(GetHabbo().GetQueryString);
                GetHabbo().OnDisconnect(reason);
            }
            if (_disconnected) return;
            if (_connection != null) _connection.Dispose();
            _disconnected = true;
        }

        internal void SendMessage(ServerMessage message)
        {
            if (message == null) return;
            var bytes = message.GetReversedBytes();
            if (GetConnection() == null) return;
            GetConnection().SendData(bytes);
        }

        internal void SendMessage(byte[] bytes)
        {
            if (GetConnection() == null) return;
            GetConnection().SendData(bytes);
        }

        internal void SendMessage(StaticMessage type)
        {
            if (GetConnection() == null) return;
            GetConnection().SendData(StaticMessagesManager.Get(type));
        }

        private void SwitchParserRequest()
        {
            if (_messageHandler == null) InitHandler();
            PacketParser.SetConnection(_connection);
            var currentData = (_connection.Parser as InitialPacketParser).CurrentData;
            _connection.Parser.Dispose();
            _connection.Parser = PacketParser;
            _connection.Parser.HandlePacketData(currentData);
        }

        private void PolicyRequest()
        {
            _connection.SendData(CrossDomainPolicy.XmlPolicyBytes);
        }
    }
}