#region

using System.Collections.Generic;
using System.Data;
using Purple.Database.Queries;

#endregion


namespace AzureSharp.HabboHotel.Roles
{
    /// <summary>
    /// Class RoleManager.
    /// </summary>
    internal class RoleManager
    {
        public readonly Dictionary<uint, string> RankBadge = new Dictionary<uint, string>();
        private readonly Dictionary<uint, int> _rankFlood = new Dictionary<uint, int>();
        private readonly Dictionary<uint, List<string>> _rankPermissions = new Dictionary<uint, List<string>>();
        private readonly Dictionary<uint, List<string>> _userPermissions = new Dictionary<uint, List<string>>();


        public void ClearPermissions()
        {
            RankBadge.Clear();
            _userPermissions.Clear();
            _rankPermissions.Clear();
            _rankFlood.Clear();
        }


        public bool ContainsRank(uint rank)
        {
            return _rankPermissions.ContainsKey(rank);
        }


        public bool ContainsUser(uint userId)
        {
            return _userPermissions.ContainsKey(userId);
        }


        public int FloodTime(uint rankId)
        {
            return _rankFlood[rankId];
        }


        public List<string> GetRightsForHabbo(uint userId, uint rank)
        {
            return ContainsUser(userId) ? _userPermissions[userId] : _rankPermissions[rank];
        }


        public void LoadRights(Query dbClient)
        {
            ClearPermissions();
            dbClient.SetQuery("SELECT * FROM ranks ORDER BY id DESC;");
            var table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    RankBadge.Add((uint)row["id"], row["badgeid"].ToString());
                }
            }
            dbClient.SetQuery("SELECT * FROM permissions_ranks ORDER BY rank DESC;");
            table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    _rankFlood.Add((uint)row["rank"], (int)row["floodtime"]);

                    var list = new List<string>();
                    foreach (DataColumn dataColumn in row.Table.Columns)
                    {
                        if (Utility.EnumToBool(row[dataColumn.ColumnName].ToString()))
                        {
                            list.Add(dataColumn.ColumnName);
                        }
                    }

                    _rankPermissions.Add((uint)row["rank"], list);
                }
            }
        }


        public int RankCount()
        {
            return RankBadge.Count;
        }


        public bool RankHasRight(uint rankId, string role)
        {
            if (!ContainsRank(rankId))
            {
                return false;
            }
            var list = _rankPermissions[rankId];
            return list.Contains(role);
        }


        public string RanksBadge(uint rank)
        {
            return RankBadge[rank];
        }


        public bool UserHasPermission(uint userId, string role)
        {
            if (!ContainsUser(userId))
            {
                return false;
            }
            var list = _userPermissions[userId];
            return list.Contains(role);
        }


        public bool UserHasPersonalPermissions(uint userId)
        {
            return ContainsUser(userId);
        }


    }
}