#region

using System;
using System.Collections.Generic;
using System.Linq;
using Purple.Database.Queries;
using AzureSharp.HabboHotel.Achievements.Composer;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Achievements
{
    /// <summary>
    /// Class AchievementManager.
    /// </summary>
    public class AchievementManager
    {
        /// <summary>
        /// The achievement data cached
        /// </summary>
        internal ServerMessage AchievementDataCached;

        /// <summary>
        /// The achievements
        /// </summary>
        internal Dictionary<string, Achievement> Achievements;

        /// <summary>
        /// Initializes a new instance of the <see cref="AchievementManager"/> class.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        internal AchievementManager(Query dbClient)
        {
            Achievements = new Dictionary<string, Achievement>();
            LoadAchievements(dbClient);
        }

        /// <summary>
        /// Loads the achievements.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        internal void LoadAchievements(Query dbClient)
        {
            Achievements.Clear();
            AchievementLevelFactory.GetAchievementLevels(out Achievements, dbClient);
            AchievementDataCached = new ServerMessage(LibraryParser.OutgoingRequest("SendAchievementsRequirementsMessageComposer"));
            AchievementDataCached.AppendInteger(Achievements.Count);
            foreach (var Ach in Achievements.Values)
            {
                AchievementDataCached.AppendString(Ach.GroupName.Replace("ACH_", ""));
                AchievementDataCached.AppendInteger(Ach.Levels.Count);
                for (var i = 1; i < Ach.Levels.Count + 1; i++)
                {
                    AchievementDataCached.AppendInteger(i);
                    AchievementDataCached.AppendInteger(Ach.Levels[i].Requirement);
                }
            }
            AchievementDataCached.AppendInteger(0);
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="Session">The session.</param>
        /// <param name="Message">The message.</param>
        internal void GetList(GameClient Session, ClientMessage Message)
        {
            Session.SendMessage(AchievementListComposer.Compose(Session, Achievements.Values.ToList()));
        }

        /// <summary>
        /// Tries the progress login achievements.
        /// </summary>
        /// <param name="Session">The session.</param>
        internal void TryProgressLoginAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null) return;
            var loginACH = Session.GetHabbo().GetAchievementData("ACH_Login");
            if (loginACH == null)
            {
                ProgressUserAchievement(Session, "ACH_Login", 1, true);
                return;
            }
            var daysBtwLastLogin = Utility.GetUnixTimeStamp() - Session.GetHabbo().PreviousOnline;
            if (daysBtwLastLogin >= 51840 && daysBtwLastLogin <= 112320) ProgressUserAchievement(Session, "ACH_Login", 1, true);
        }

        /// <summary>
        /// Tries the progress registration achievements.
        /// </summary>
        /// <param name="Session">The session.</param>
        internal void TryProgressRegistrationAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null) return;
            var regACH = Session.GetHabbo().GetAchievementData("ACH_RegistrationDuration");
            if (regACH == null)
            {
                ProgressUserAchievement(Session, "ACH_RegistrationDuration", 1, true);
                return;
            }
            if (regACH.Level == 5) return;
            double sinceMember = Utility.GetUnixTimeStamp() - (int) Session.GetHabbo().CreateDate;
            var daysSinceMember = Convert.ToInt32(Math.Round(sinceMember / 86400));
            if (daysSinceMember == regACH.Progress) return;
            var dais = daysSinceMember - regACH.Progress;
            if (dais < 1) return;
            ProgressUserAchievement(Session, "ACH_RegistrationDuration", dais, false);
        }

        /// <summary>
        /// Tries the progress habbo club achievements.
        /// </summary>
        /// <param name="Session">The session.</param>
        internal void TryProgressHabboClubAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null || !Session.GetHabbo().GetSubscriptionManager().HasSubscription) return;
            var ClubACH = Session.GetHabbo().GetAchievementData("ACH_VipHC");
            if (ClubACH == null)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, true);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, true);
                return;
            }
            if (ClubACH.Level == 5) return;
            var Subscription = Session.GetHabbo().GetSubscriptionManager().GetSubscription();
            var SinceActivation = Utility.GetUnixTimeStamp() - Subscription.ActivateTime;
            if (SinceActivation < 31556926) return;
            if (SinceActivation >= 31556926)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 63113851)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 94670777)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 126227704)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 157784630)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
        }

        /// <summary>
        /// Progresses the user achievement.
        /// </summary>
        /// <param name="Session">The session.</param>
        /// <param name="AchievementGroup">The achievement group.</param>
        /// <param name="ProgressAmount">The progress amount.</param>
        /// <param name="FromZero">if set to <c>true</c> [from zero].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool ProgressUserAchievement(GameClient Session, string AchievementGroup, int ProgressAmount, bool FromZero = false)
        {
            if (!Achievements.ContainsKey(AchievementGroup) || Session == null) return false;
            Achievement achievement = null;
            achievement = Achievements[AchievementGroup];
            var userAchievement = Session.GetHabbo().GetAchievementData(AchievementGroup);
            if (userAchievement == null)
            {
                userAchievement = new UserAchievement(AchievementGroup, 0, 0);
                Session.GetHabbo().Achievements.Add(AchievementGroup, userAchievement);
            }
            var count = achievement.Levels.Count;
            if (userAchievement != null && userAchievement.Level == count) return false;

            {
                var num = userAchievement != null ? userAchievement.Level + 1 : 1;
                if (num > count) num = count;
                var targetLevelData = achievement.Levels[num];
                var num2 = 0;
                if (FromZero) num2 = ProgressAmount;
                else num2 = userAchievement != null ? userAchievement.Progress + ProgressAmount : ProgressAmount;
                var num3 = userAchievement != null ? userAchievement.Level : 0;
                var num4 = num3 + 1;
                if (num4 > count) num4 = count;
                if (num2 >= targetLevelData.Requirement)
                {
                    num3++;
                    num4++;
                    num2 = 0;
                    if (num == 1) Session.GetHabbo().GetBadgeComponent().GiveBadge($"{AchievementGroup}{num}", true, Session, false);
                    else
                    {
                        Session.GetHabbo().GetBadgeComponent().RemoveBadge(Convert.ToString($"{AchievementGroup}{num - 1}"), Session);
                        Session.GetHabbo().GetBadgeComponent().GiveBadge($"{AchievementGroup}{num}", true, Session, false);
                    }
                    if (num4 > count) num4 = count;
                    Session.GetHabbo().ActivityPoints += targetLevelData.RewardPixels;
                    Session.GetHabbo().NotifyNewPixels(targetLevelData.RewardPixels);
                    Session.GetHabbo().UpdateActivityPointsBalance();
                    Session.SendMessage(AchievementUnlockedComposer.Compose(achievement, num, targetLevelData.RewardPoints, targetLevelData.RewardPixels));
                    using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.SetQuery(string.Concat("REPLACE INTO users_achievements VALUES (", Session.GetHabbo().Id, ", @group, ", num3, ", ", num2, ")"));
                        queryReactor.AddParameter("group", AchievementGroup);
                        queryReactor.RunQuery();
                    }
                    userAchievement.Level = num3;
                    userAchievement.Progress = num2;
                    Session.GetHabbo().AchievementPoints += targetLevelData.RewardPoints;
                    Session.GetHabbo().NotifyNewPixels(targetLevelData.RewardPixels);
                    Session.GetHabbo().ActivityPoints += targetLevelData.RewardPixels;
                    Session.GetHabbo().UpdateActivityPointsBalance();
                    Session.SendMessage(AchievementScoreUpdateComposer.Compose(Session.GetHabbo().AchievementPoints));
                    var targetLevelData2 = achievement.Levels[num4];
                    Session.SendMessage(AchievementProgressComposer.Compose(achievement, num4, targetLevelData2, count, Session.GetHabbo().GetAchievementData(AchievementGroup)));
                    Talent talent = null;
                    if (PurpleEmulator.GetGame().GetTalentManager().TryGetTalent(AchievementGroup, out talent)) PurpleEmulator.GetGame().GetTalentManager().CompleteUserTalent(Session, talent);
                    return true;
                }
                userAchievement.Level = num3;
                userAchievement.Progress = num2;
                using (var queryreactor2 = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor2.SetQuery(string.Concat("REPLACE INTO users_achievements VALUES (", Session.GetHabbo().Id, ", @group, ", num3, ", ", num2, ")"));
                    queryreactor2.AddParameter("group", AchievementGroup);
                    queryreactor2.RunQuery();
                }

                if (Session == null || Session.GetHabbo() == null) return false;
                Session.SendMessage(AchievementProgressComposer.Compose(achievement, num, targetLevelData, count, Session.GetHabbo().GetAchievementData(AchievementGroup)));
                Session.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                Session.GetMessageHandler().GetResponse().AppendInteger(-1);
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Look);
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Gender.ToLower());
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Motto);
                Session.GetMessageHandler().GetResponse().AppendInteger(Session.GetHabbo().AchievementPoints);
                Session.GetMessageHandler().SendResponse();
                return false;
            }
        }

        /// <summary>
        /// Gets the achievement.
        /// </summary>
        /// <param name="achievementGroup">The achievement group.</param>
        /// <returns>Achievement.</returns>
        internal Achievement GetAchievement(string achievementGroup)
        {
            return Achievements.ContainsKey(achievementGroup) ? Achievements[achievementGroup] : null;
        }
    }
}