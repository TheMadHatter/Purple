﻿#region

using System;
using System.Threading;
using Purple.Database.Queries;
using AzureSharp.HabboHotel.Achievements;
using AzureSharp.HabboHotel.Catalogs;
using AzureSharp.HabboHotel.Commands;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Groups;
using AzureSharp.HabboHotel.Guides;
using AzureSharp.HabboHotel.Items;
using AzureSharp.HabboHotel.Items.Crafting;
using AzureSharp.HabboHotel.Misc;
using AzureSharp.HabboHotel.Navigators;
using AzureSharp.HabboHotel.Pets;
using AzureSharp.HabboHotel.Polls;
using AzureSharp.HabboHotel.Quests;
using AzureSharp.HabboHotel.Roles;
using AzureSharp.HabboHotel.RoomBots;
using AzureSharp.HabboHotel.Rooms;
using AzureSharp.HabboHotel.SoundMachine;
using AzureSharp.HabboHotel.Support;
using AzureSharp.HabboHotel.Users;
using AzureSharp.Manager;
using AzureSharp.Messages;
using AzureSharp.Security;

#endregion

namespace AzureSharp.HabboHotel
{
    /// <summary>
    /// Class Game.
    /// </summary>
    internal class Game
    {

        /// <summary>
        /// The game loop enabled
        /// </summary>
        internal static bool GameLoopEnabled = true;

        /// <summary>
        /// The _achievement manager
        /// </summary>
        private readonly AchievementManager _achievementManager;

        /// <summary>
        /// The _ban manager
        /// </summary>
        private readonly ModerationBanManager _banManager;

        /// <summary>
        /// The _bot manager
        /// </summary>
        private readonly BotManager _botManager;

        /// <summary>
        /// The _cache manager
        /// </summary>
        private readonly CacheManager _cacheManager;

        /// <summary>
        /// The _catalog
        /// </summary>
        private readonly Catalog _catalog;

        /// <summary>
        /// The _client manager
        /// </summary>
        private readonly GameClientManager _clientManager;

        /// <summary>
        /// The _clothing manager
        /// </summary>
        private readonly ClothingManager _clothingManager;

        /// <summary>
        /// The _clothing manager
        /// </summary>
        private readonly CrackableEggHandler _crackableEggHandler;

        /// <summary>
        /// The crafting manager
        /// </summary>
        private readonly CraftingManager _craftingManager;

        /// <summary>
        /// The _events
        /// </summary>
        private readonly RoomEvents _events;

        /// <summary>
        /// The _group manager
        /// </summary>
        private readonly GroupManager _groupManager;

        /// <summary>
        /// The _guide manager
        /// </summary>
        private readonly GuideManager _guideManager;

        private readonly HallOfFame _hallOfFame;

        /// <summary>
        /// The _hotel view
        /// </summary>
        private readonly HotelView _hotelView;

        /// <summary>
        /// The _item manager
        /// </summary>
        private readonly ItemManager _itemManager;

        /// <summary>
        /// The _moderation tool
        /// </summary>
        private readonly ModerationTool _moderationTool;

        /// <summary>
        /// The _navigator
        /// </summary>
        private readonly Navigator _navigator;

        /// <summary>
        /// The _pinata handler
        /// </summary>
        private readonly PinataHandler _pinataHandler;

        /// <summary>
        /// The _pixel manager
        /// </summary>
        private readonly CoinsManager _coinsManager;

        /// <summary>
        /// The _poll manager
        /// </summary>
        private readonly PollManager _pollManager;

        /// <summary>
        /// The _quest manager
        /// </summary>
        private readonly QuestManager _questManager;

        /// <summary>
        /// The _role manager
        /// </summary>
        private readonly RoleManager _roleManager;

        /// <summary>
        /// The _room manager
        /// </summary>
        private readonly RoomManager _roomManager;

        /// <summary>
        /// The _talent manager
        /// </summary>
        private readonly TalentManager _talentManager;

        private readonly TargetedOfferManager _targetedOfferManager;

        /// <summary>
        /// The _game loop
        /// </summary>
        private Thread _gameLoop;

        /// <summary>
        /// The client manager cycle ended
        /// </summary>
        internal bool ClientManagerCycleEnded, RoomManagerCycleEnded;

        private AntiMutant _antiMutant;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        internal Game()
        {
            Logger.Info(@"Starting AzureSharp...");

            _clientManager = new GameClientManager();
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                Logger.Info("Loading Bans...");
                _banManager = new ModerationBanManager();
                _banManager.LoadBans(queryReactor);

                Logger.Info("Loading Roles...");
                _roleManager = new RoleManager();
                _roleManager.LoadRights(queryReactor);

                Logger.Info("Loading Items...");
                _itemManager = new ItemManager();
                _itemManager.LoadItems(queryReactor);

                Logger.Info("Loading Catalog...");
                _catalog = new Catalog();

                Logger.Info("Loading Targeted Offers...");
                _targetedOfferManager = new TargetedOfferManager();

                Logger.Info("Loading Clothing...");
                _clothingManager = new ClothingManager();
                _clothingManager.Initialize(queryReactor);

                Logger.Info("Loading Crafting...");
                _craftingManager = new CraftingManager();
                _craftingManager.Initialize(queryReactor);

                Logger.Info("Loading Rooms...");
                _roomManager = new RoomManager();
                _roomManager.LoadModels(queryReactor);

                Logger.Info("Loading Navigator...");
                _navigator = new Navigator();
                _navigator.Initialize(queryReactor);

                Logger.Info("Loading Groups...");
                _groupManager = new GroupManager();
                _groupManager.InitGroups();

                Logger.Info("Loading PixelManager...");
                _coinsManager = new CoinsManager();

                Logger.Info("Loading HotelView...");
                _hotelView = new HotelView();

                Logger.Info("Loading Hall Of Fame...");
                _hallOfFame = new HallOfFame();

                Logger.Info("Loading ModerationTool...");
                _moderationTool = new ModerationTool();
                _moderationTool.LoadMessagePresets(queryReactor);
                _moderationTool.LoadPendingTickets(queryReactor);

                Logger.Info("Loading Bots...");
                _botManager = new BotManager();

                Logger.Info("Loading Quests...");
                _questManager = new QuestManager();
                _questManager.Initialize(queryReactor);

                Logger.Info("Loading Events...");
                _events = new RoomEvents();

                Logger.Info("Loading Talents...");
                _talentManager = new TalentManager();
                _talentManager.Initialize(queryReactor);

                //this.SnowStormManager = new SnowStormManager();

                Logger.Info("Loading Pinata...");
                _pinataHandler = new PinataHandler();
                _pinataHandler.Initialize(queryReactor);

                Logger.Info("Loading Crackable Eggs...");
                _crackableEggHandler = new CrackableEggHandler();
                _crackableEggHandler.Initialize(queryReactor);

                Logger.Info("Loading Polls...");
                _pollManager = new PollManager();
                _pollManager.Init(queryReactor);

                Logger.Info("Loading Achievements...");
                _achievementManager = new AchievementManager(queryReactor);

                Logger.Info("Loading StaticMessages ...");
                StaticMessagesManager.Load();

                Logger.Info("Loading Guides ...");
                _guideManager = new GuideManager();

                Logger.Info("Loading and Registering Commands...");
                CommandsManager.Register();

                _cacheManager = new CacheManager();

                Logger.Info("Loading AntiMutant...");
                _antiMutant = new AntiMutant();
                _antiMutant.Load();

                Logger.Info("Loading WordFilter...");
                Filter.Load();
            }
        }

        /// <summary>
        /// Gets a value indicating whether [game loop active ext].
        /// </summary>
        /// <value><c>true</c> if [game loop active ext]; otherwise, <c>false</c>.</value>
        internal bool GameLoopActiveExt { get; private set; }
        
        /// <summary>
        /// Databases the cleanup.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        internal static void DatabaseCleanup(Query dbClient)
        {
            dbClient.RunFastQuery("UPDATE users SET online = '0' WHERE online <> '0'");
            dbClient.RunFastQuery("UPDATE rooms_data SET users_now = 0 WHERE users_now <> 0");
            dbClient.RunFastQuery($"UPDATE server_status SET status = 1, users_online = 0, rooms_loaded = 0, server_ver = 'AzureSharp {PurpleEmulator.Version}', stamp = '{Utility.GetUnixTimeStamp()}' ");
        }

        /*internal _antiMutant GetAntiMutant()
        {
        //return this._antiMutant;
        }*/

        /// <summary>
        /// Gets the cache manager.
        /// </summary>
        /// <returns>CacheManager.</returns>
        internal CacheManager GetCacheManager()
        {
            return _cacheManager;
        }

        /// <summary>
        /// Gets the client manager.
        /// </summary>
        /// <returns>GameClientManager.</returns>
        internal GameClientManager GetClientManager()
        {
            return _clientManager;
        }

        /// <summary>
        /// Gets the ban manager.
        /// </summary>
        /// <returns>ModerationBanManager.</returns>
        internal ModerationBanManager GetBanManager()
        {
            return _banManager;
        }

        /// <summary>
        /// Gets the role manager.
        /// </summary>
        /// <returns>RoleManager.</returns>
        internal RoleManager GetRoleManager()
        {
            return _roleManager;
        }

        /// <summary>
        /// Gets the catalog.
        /// </summary>
        /// <returns>Catalog.</returns>
        internal Catalog GetCatalog()
        {
            return _catalog;
        }

        /// <summary>
        /// Gets the room events.
        /// </summary>
        /// <returns>RoomEvents.</returns>
        internal RoomEvents GetRoomEvents()
        {
            return _events;
        }

        /// <summary>
        /// Gets the guide manager.
        /// </summary>
        /// <returns>GuideManager.</returns>
        internal GuideManager GetGuideManager()
        {
            return _guideManager;
        }

        /// <summary>
        /// Gets the navigator.
        /// </summary>
        /// <returns>Navigator.</returns>
        internal Navigator GetNavigator()
        {
            return _navigator;
        }

        /// <summary>
        /// Gets the item manager.
        /// </summary>
        /// <returns>ItemManager.</returns>
        internal ItemManager GetItemManager()
        {
            return _itemManager;
        }

        /// <summary>
        /// Gets the anti mutant\.
        /// </summary>
        /// <returns>AntiMutant.</returns>
        internal AntiMutant GetAntiMutant()
        {
            return _antiMutant;
        }

        /// <summary>
        /// Gets the room manager.
        /// </summary>
        /// <returns>RoomManager.</returns>
        internal RoomManager GetRoomManager()
        {
            return _roomManager;
        }

        /// <summary>
        /// Gets the pixel manager.
        /// </summary>
        /// <returns>CoinsManager.</returns>
        internal CoinsManager GetCoinsManager()
        {
            return _coinsManager;
        }

        /// <summary>
        /// Gets the hotel view.
        /// </summary>
        /// <returns>HotelView.</returns>
        internal HotelView GetHotelView()
        {
            return _hotelView;
        }

        internal HallOfFame GetHallOfFame()
        {
            return _hallOfFame;
        }

        internal TargetedOfferManager GetTargetedOfferManager()
        {
            return _targetedOfferManager;
        }

        internal CraftingManager GetCraftingManager()
        {
            return _craftingManager;
        }

        /// <summary>
        /// Gets the achievement manager.
        /// </summary>
        /// <returns>AchievementManager.</returns>
        internal AchievementManager GetAchievementManager()
        {
            return _achievementManager;
        }

        /// <summary>
        /// Gets the moderation tool.
        /// </summary>
        /// <returns>ModerationTool.</returns>
        internal ModerationTool GetModerationTool()
        {
            return _moderationTool;
        }

        /// <summary>
        /// Gets the bot manager.
        /// </summary>
        /// <returns>BotManager.</returns>
        internal BotManager GetBotManager()
        {
            return _botManager;
        }

        /// <summary>
        /// Gets the quest manager.
        /// </summary>
        /// <returns>QuestManager.</returns>
        internal QuestManager GetQuestManager()
        {
            return _questManager;
        }

        /// <summary>
        /// Gets the group manager.
        /// </summary>
        /// <returns>GroupManager.</returns>
        internal GroupManager GetGroupManager()
        {
            return _groupManager;
        }

        /// <summary>
        /// Gets the talent manager.
        /// </summary>
        /// <returns>TalentManager.</returns>
        internal TalentManager GetTalentManager()
        {
            return _talentManager;
        }

        /// <summary>
        /// Gets the pinata handler.
        /// </summary>
        /// <returns>PinataHandler.</returns>
        internal PinataHandler GetPinataHandler()
        {
            return _pinataHandler;
        }

        internal CrackableEggHandler GetCrackableEggHandler()
        {
            return _crackableEggHandler;
        }

        /// <summary>
        /// Gets the poll manager.
        /// </summary>
        /// <returns>PollManager.</returns>
        internal PollManager GetPollManager()
        {
            return _pollManager;
        }

        /// <summary>
        /// Gets the clothing manager.
        /// </summary>
        /// <returns>ClothingManager.</returns>
        internal ClothingManager GetClothingManager()
        {
            return _clothingManager;
        }

        /// <summary>
        /// Continues the loading.
        /// </summary>
        internal void ContinueLoading()
        {
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                uint catalogPageLoaded;
                PetRace.Init(queryReactor);
                _catalog.Initialize(queryReactor, out catalogPageLoaded);
                SongManager.Initialize();
                LowPriorityWorker.Init(queryReactor);
                _roomManager.InitVotedRooms(queryReactor);
                _roomManager.LoadCompetitionManager();
            }
            StartGameLoop();
            _coinsManager.StartTimer();
        }

        /// <summary>
        /// Starts the game loop.
        /// </summary>
        internal void StartGameLoop()
        {
            GameLoopActiveExt = true;
            _gameLoop = new Thread(MainGameLoop) {Name = "Game Loop"};
            _gameLoop.Start();
        }

        /// <summary>
        /// Stops the game loop.
        /// </summary>
        internal void StopGameLoop()
        {
            GameLoopActiveExt = false;
            while (!RoomManagerCycleEnded || !ClientManagerCycleEnded) Thread.Sleep(25);
        }

        /// <summary>
        /// Destroys this instance.
        /// </summary>
        internal void Destroy()
        {
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) DatabaseCleanup(queryReactor);
            GetClientManager();
            Logger.Info("Client Manager destroyed");
        }

        /// <summary>
        /// Reloaditemses this instance.
        /// </summary>
        internal void Reloaditems()
        {
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                _itemManager.LoadItems(queryReactor);
            }
        }

        /// <summary>
        /// Mains the game loop.
        /// </summary>
        private void MainGameLoop()
        {
            LowPriorityWorker.StartProcessing();
            while (GameLoopActiveExt)
            {
                if (GameLoopEnabled)
                {
                    try
                    {
                        RoomManagerCycleEnded = false;
                        ClientManagerCycleEnded = false;
                        _roomManager.OnCycle();
                        _clientManager.OnCycle();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Exception in Game Loop!", ex);
                    }
                }
                Thread.Sleep(25);
            }
        }
    }
}