﻿using System.Collections.Generic;
using System.Data;
using Purple.Database.Queries;

namespace AzureSharp.HabboHotel.Items.Crafting
{
    class CraftingManager
    {
        internal List<string> CraftableItems;
        internal Dictionary<string, CraftingRecipe> CraftingRecipes;

        public CraftingManager()
        {
            CraftingRecipes = new Dictionary<string, CraftingRecipe>();
            CraftableItems = new List<string>();
        }

        internal void Initialize(Query dbClient)
        {
            CraftingRecipes.Clear();
            dbClient.SetQuery("SELECT * FROM crafting_recipes");
            var recipes = dbClient.GetTable();
            foreach (DataRow recipe in recipes.Rows)
            {
                var value = new CraftingRecipe((string) recipe["id"], (string) recipe["items"], (string) recipe["result"]);
                CraftingRecipes.Add((string) recipe["id"], value);
            }

            CraftableItems.Clear();
            dbClient.SetQuery("SELECT * FROM crafting_items");
            var items = dbClient.GetTable();
            foreach (DataRow item in items.Rows)
            {
                CraftableItems.Add((string) item["itemName"]);
            }
        }

        internal CraftingRecipe GetRecipe(string name)
        {
            if (CraftingRecipes.ContainsKey(name)) return CraftingRecipes[name];
            return null;
        }
    }
}