#region

using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Rooms;

#endregion

namespace AzureSharp.HabboHotel.Items.Interactor
{
    internal class InteractorFootball : IFurniInteractor
    {
        public void OnPlace(GameClient session, RoomItem item)
        {
        }

        public void OnRemove(GameClient session, RoomItem item)
        {
        }

        public void OnTrigger(GameClient session, RoomItem item, int request, bool hasRights)
        {
        }

        public void OnUserWalk(GameClient session, RoomItem item, RoomUser user)
        {
        }

        public void OnWiredTrigger(RoomItem item)
        {
        }
    }
}