﻿#region

using System.Data;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Catalogs
{
    internal class TargetedOfferManager
    {
        internal TargetedOffer CurrentOffer;

        public TargetedOfferManager()
        {
            LoadOffer();
        }

        public void LoadOffer()
        {
            CurrentOffer = null;
            DataRow row;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM catalog_targetedoffers WHERE enabled = '1' LIMIT 1");
                row = queryReactor.GetRow();
                if (row == null) return;
                CurrentOffer = new TargetedOffer((int) row["id"], (string) row["identifier"], (int) row["cost_credits"], (int) row["cost_duckets"], (int) row["cost_diamonds"], (int) row["purchase_limit"], (int) row["expiration_time"], (string) row["title"], (string) row["description"], (string) row["image"], (string) row["products"]);
            }
        }
    }

    internal class TargetedOffer
    {
        internal int CostCredits, CostDuckets, CostDiamonds;
        internal int ExpirationTime;
        internal int Id;
        internal string Identifier;
        internal string[] Products;
        internal int PurchaseLimit;
        internal string Title, Description, Image;

        public TargetedOffer(int id, string identifier, int costCredits, int costDuckets, int costDiamonds, int purchaseLimit, int expirationTime, string title, string description, string image, string products)
        {
            Id = id;
            Identifier = identifier;
            CostCredits = costCredits;
            CostDuckets = costDuckets;
            CostDiamonds = costDiamonds;
            PurchaseLimit = purchaseLimit;
            ExpirationTime = expirationTime;
            Title = title;
            Description = description;
            Image = image;
            Products = products.Split(';');
        }

        internal void GenerateMessage(ServerMessage message)
        {
            message.Init(LibraryParser.OutgoingRequest("TargetedOfferMessageComposer"));
            message.AppendInteger(1); //show
            message.AppendInteger(Id);
            message.AppendString(Identifier);
            message.AppendString(Identifier);
            message.AppendInteger(CostCredits);
            if (CostDiamonds > 0)
            {
                message.AppendInteger(CostDiamonds);
                message.AppendInteger(105);
            }
            else
            {
                message.AppendInteger(CostDuckets);
                message.AppendInteger(0);
            }
            message.AppendInteger(PurchaseLimit);
            var TimeLeft = ExpirationTime - Utility.GetUnixTimeStamp();
            message.AppendInteger(TimeLeft);
            message.AppendString(Title);
            message.AppendString(Description);
            message.AppendString(Image);
            message.AppendString("");
            message.StartArray();
            foreach (var Product in Products)
            {
                message.AppendString(Product);
                message.SaveArray();
            }
            message.EndArray();
        }
    }
}