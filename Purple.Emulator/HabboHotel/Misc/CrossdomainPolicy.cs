#region

using AzureSharp.Configuration;

#endregion

namespace AzureSharp.HabboHotel.Misc
{
    /// <summary>
    /// Class CrossdomainPolicy.
    /// </summary>
    internal static class CrossDomainPolicy
    {
        internal static byte[] XmlPolicyBytes;

        internal static void Set()
        {
            XmlPolicyBytes = PurpleEmulator.GetDefaultEncoding().GetBytes("<?xml version=\"1.0\"?>\r\n<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n<cross-domain-policy>\r\n<allow-access-from domain=\"*\" to-ports=\"" + ConfigurationData.GetInt("game.tcp.port.proxy", ConfigurationData.GetInt("game.tcp.port", 30000)) + "\" />\r\n</cross-domain-policy>\0");
        }
    }
}