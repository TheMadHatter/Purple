﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class GiveBadge. This class cannot be inherited.
    /// </summary>
    internal sealed class GiveBadge : Command
    {
        public GiveBadge()
        {
            MinParams = 2;
            Description = "Give user badge.";
            Usage = "[username] [badge]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }
            client.GetHabbo().GetBadgeComponent().GiveBadge(pms[1], true, client, false);
            session.SendNotif(TextManager.GetText("command_badge_give_done"));
            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName, "Badge", $"Badge given to user [{pms[1]}]");
        }
    }
}