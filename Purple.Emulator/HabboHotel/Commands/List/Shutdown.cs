﻿#region

using System.Threading.Tasks;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Shutdown. This class cannot be inherited.
    /// </summary>
    internal sealed class Shutdown : Command
    {
        public Shutdown()
        {
            MinParams = 0;
            Description = "Shutdown PurpleEmulator Emulator.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, string.Empty, "Shutdown", "Issued shutdown command!");
            new Task(PurpleEmulator.PerformShutDown).Start();
        }
    }
}