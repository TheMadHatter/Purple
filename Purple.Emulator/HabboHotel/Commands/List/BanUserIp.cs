﻿#region

using System;
using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class BanUser. This class cannot be inherited.
    /// </summary>
    internal sealed class BanUserIp : Command
    {

        public BanUserIp()
        {
            MinParams = -1;
            Description = "Ban user by IP.";
            Usage = "[ip] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                PurpleEmulator.GetGame().GetBanManager().BanUser(user, session.GetHabbo().UserName, 788922000.0, string.Join(" ", pms.Skip(1)), true, false);
            }
            catch(Exception ex)
            {
                Logger.Error("Error while banning", ex);
            }
        }
    }
}