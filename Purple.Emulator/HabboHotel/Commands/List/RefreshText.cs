﻿# region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

# endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshSongs. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshText : Command
    {
        public RefreshText()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_texts_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var client = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                TextManager.ClearText();
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}