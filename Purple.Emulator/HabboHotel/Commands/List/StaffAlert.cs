﻿#region

using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class StaffAlert : Command
    {
        public StaffAlert()
        {
            MinParams = -1;
            Description = "Send an alert to all online staff members.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var msg = string.Join(" ", pms);

            var message = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            message.AppendString("staffcloud");
            message.AppendInteger(2);
            message.AppendString("title");
            message.AppendString("Staff Internal Alert");
            message.AppendString("message");
            message.AppendString($"{msg}\r\n- <i>Sender: {session.GetHabbo().UserName}</i>");
            PurpleEmulator.GetGame().GetClientManager().StaffAlert(message, 0);
            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, string.Empty, "StaffAlert", $"Staff alert [{msg}]");
        }
    }
}