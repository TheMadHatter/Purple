﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Pets;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshPets. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshPets : Command
    {
        public RefreshPets()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_pets_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PetRace.Init(adapter);
                PetCommandHandler.Init(adapter);
                PetLocale.Init(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}