﻿#region

using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Stand. This class cannot be inherited.
    /// </summary>
    internal sealed class Stand : Command
    {
        public Stand()
        {
            MinParams = 0;
            Description = "User stands UP.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (user == null) return;

            if (user.IsSitting)
            {
                user.Statusses.Remove("sit");
                user.IsSitting = false;
                user.UpdateNeeded = true;
            }
            else if (user.IsLyingDown)
            {
                user.Statusses.Remove("lay");
                user.IsLyingDown = false;
                user.UpdateNeeded = true;
            }
        }
    }
}