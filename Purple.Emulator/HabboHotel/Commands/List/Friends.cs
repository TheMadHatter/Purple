﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Sit. This class cannot be inherited.
    /// </summary>
    internal sealed class Friends : Command
    {
        public Friends()
        {
            MinParams = 0;
            Description = "Enable/disable friend request.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            session.GetHabbo().HasFriendRequestsDisabled = !session.GetHabbo().HasFriendRequestsDisabled;
            session.SendWhisper(TextManager.GetText(session.GetHabbo().HasFriendRequestsDisabled ? "cmd_friends_disabled" : "cmd_friends_enabled"));
        }
    }
}