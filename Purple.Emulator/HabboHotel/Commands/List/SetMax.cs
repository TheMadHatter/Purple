﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SetMax. This class cannot be inherited.
    /// </summary>
    internal sealed class SetMax : Command
    {
        public SetMax()
        {
            MinParams = 1;
            Description = "Set max users in room.";
            Usage = "[max]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("user_control_any_room") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort maxUsers;
            if (!ushort.TryParse(pms[0], out maxUsers) || maxUsers == 0 || maxUsers > 200)
            {
                session.SendWhisper(TextManager.GetText("command_setmax_error_number"));
                return;
            }

            if (maxUsers > 100 && !session.GetHabbo().VIP)
            {
                session.SendWhisper(TextManager.GetText("command_setmax_error_max"));
                return;
            }
            if (maxUsers < 10 && !session.GetHabbo().VIP)
            {
                session.SendWhisper(TextManager.GetText("command_setmax_error_min"));
                return;
            }

            session.GetHabbo().CurrentRoom.SetMaxUsers(maxUsers);
        }
    }
}