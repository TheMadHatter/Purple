﻿#region

using System.Threading;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Polls;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class StartQuestion. This class cannot be inherited.
    /// </summary>
    internal sealed class StartQuestion : Command
    {
        public StartQuestion()
        {
            MinParams = 1;
            Description = "Starts a matching question.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("user_is_staff");
        }

        public override void Execute(GameClient client, string[] pms)
        {
            var id = uint.Parse(pms[0]);
            var poll = PurpleEmulator.GetGame().GetPollManager().TryGetPollById(id);
            if (poll == null || poll.Type != Poll.PollType.Matching)
            {
                client.SendWhisper("Poll doesn't exists or isn't a matching poll.");
                return;
            }
            poll.answersPositive = 0;
            poll.answersNegative = 0;
            MatchingPollAnswer(client, poll);
            var ShowPoll = new Thread(delegate() { MatchingPollResults(client, poll); });
            ShowPoll.Start();
        }

        internal static void MatchingPollAnswer(GameClient client, Poll poll)
        {
            if (poll == null || poll.Type != Poll.PollType.Matching) return;
            var message = new ServerMessage(LibraryParser.OutgoingRequest("MatchingPollMessageComposer"));
            message.AppendString("MATCHING_POLL");
            message.AppendInteger(poll.Id);
            message.AppendInteger(poll.Id);
            message.AppendInteger(15580);
            message.AppendInteger(poll.Id);
            message.AppendInteger(29);
            message.AppendInteger(5);
            message.AppendString(poll.PollName);
            client.GetHabbo().CurrentRoom.SendMessage(message);
        }

        internal static void MatchingPollResults(GameClient client, Poll poll)
        {
            var room = client.GetHabbo().CurrentRoom;
            if (poll == null || poll.Type != Poll.PollType.Matching || room == null) return;

            var users = room.GetRoomUserManager().GetRoomUsers();

            for (var i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                foreach (var roomUser in users)
                {
                    var user = PurpleEmulator.GetHabboById(roomUser.UserId);
                    if (user.answeredPool)
                    {
                        var result = new ServerMessage(LibraryParser.OutgoingRequest("MatchingPollResultMessageComposer"));
                        result.AppendInteger(poll.Id);
                        result.AppendInteger(2);
                        result.AppendString("0");
                        result.AppendInteger(poll.answersNegative);
                        result.AppendString("1");
                        result.AppendInteger(poll.answersPositive);
                        room.SendMessage(result);
                    }
                }
            }
            foreach (var roomUser in users)
            {
                var user = PurpleEmulator.GetHabboById(roomUser.UserId);
                if (user.answeredPool)
                {
                    user.answeredPool = false;
                }
            }
        }
    }
}