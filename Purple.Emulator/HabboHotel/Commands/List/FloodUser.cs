﻿#region

using System;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class FloodUser. This class cannot be inherited.
    /// </summary>
    internal sealed class FloodUser : Command
    {
        public FloodUser()
        {
            MinParams = 2;
            Description = "Give a user a mute in seconds";
            Usage = "[username] [length]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (client.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            int time;
            if (!int.TryParse(pms[1], out time))
            {
                session.SendWhisper(TextManager.GetText("enter_numbers"));
                return;
            }

            client.GetHabbo().FloodTime = Utility.GetUnixTimeStamp() + Convert.ToInt32(pms[1]);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FloodFilterMessageComposer"));
            serverMessage.AppendInteger(Convert.ToInt32(pms[1]));
            client.SendMessage(serverMessage);
        }
    }
}