﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshPromos : Command
    {
        public RefreshPromos()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_promo_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            PurpleEmulator.GetGame().GetHotelView().RefreshPromoList();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}