#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class DisconnectUser. This class cannot be inherited.
    /// </summary>
    internal sealed class DisconnectUser : Command
    {
        public DisconnectUser()
        {
            MinParams = 1;
            Description = "Disconnect a user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (user == null || user.GetHabbo() == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                user.GetConnection().Dispose();
                PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, user.GetHabbo().UserName, "dc", $"Disconnect User[{pms[1]}]");
            }
            catch
            {
            }
        }
    }
}