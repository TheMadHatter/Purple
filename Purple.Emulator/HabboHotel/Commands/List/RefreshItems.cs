﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshItems. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshItems : Command
    {
        public RefreshItems()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_items_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            FurniDataParser.SetCache();
            PurpleEmulator.GetGame().Reloaditems();
            FurniDataParser.Clear();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}