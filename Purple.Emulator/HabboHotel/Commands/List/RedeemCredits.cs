﻿#region

using System;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RedeemCredits.
    /// </summary>
    internal sealed class RedeemCredits : Command
    {

        internal RedeemCredits()
        {
            Description = "Change invetory items back to credits.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            try
            {
                session.GetHabbo().GetInventoryComponent().Redeemcredits(session);
                session.SendNotif(TextManager.GetText("command_redeem_credits"));
            }
            catch (Exception e)
            {
                Logger.Error("Failed to change the exchange to credits.", e);
                session.SendNotif(TextManager.GetText("command_redeem_credits"));
            }
        }
    }
}