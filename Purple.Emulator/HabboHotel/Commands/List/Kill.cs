﻿#region

using System;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.PathFinding;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Kill. This class cannot be inherited.
    /// </summary>
    internal sealed class Kill : Command
    {
        public Kill()
        {
            MinParams = 1;
            Description = "Kill an user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().VIP || session.GetHabbo().Rank > 1;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            if (room == null) return;

            var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().LastSelectedUser);
            if (user2 == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }

            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().UserName);
            if (PathFinder.GetDistance(user.X, user.Y, user2.X, user2.Y) > 1)
            {
                session.SendWhisper(TextManager.GetText("kil_command_error_1"));
                return;
            }
            if (user2.IsLyingDown || user2.IsSitting)
            {
                session.SendWhisper(TextManager.GetText("kil_command_error_2"));
                return;
            }
            if (!string.Equals(user2.GetUserName(), session.GetHabbo().UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                user2.Statusses.Add("lay", "0.55");
                user2.IsLyingDown = true;
                user2.UpdateNeeded = true;
                user.Chat(user.GetClient(), TextManager.GetText("command.kill.user"), true, 0, 3);
                user2.Chat(user2.GetClient(), TextManager.GetText("command.kill.userdeath"), true, 0, 3);
                return;
            }
            user.Chat(session, "I am sad", false, 0, 0);
        }
    }
}