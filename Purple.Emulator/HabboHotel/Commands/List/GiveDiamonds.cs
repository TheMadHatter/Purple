﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class GiveDiamonds. This class cannot be inherited.
    /// </summary>
    internal sealed class GiveDiamonds : Command
    {
        public GiveDiamonds()
        {
            MinParams = 2;
            Description = "Give user diamonds.";
            Usage = "[username] [diamonds]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_user_can_give_currency");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }

            int amount;
            if (!int.TryParse(pms[1], out amount))
            {
                session.SendWhisper(TextManager.GetText("enter_numbers"));
                return;
            }
            client.GetHabbo().Diamonds += amount;
            client.GetHabbo().UpdateSeasonalCurrencyBalance();
            client.SendNotif(string.Format(TextManager.GetText("staff_gives_diamonds"), session.GetHabbo().UserName, amount));
        }
    }
}