﻿#region

using System.Collections.Generic;
using System.Linq;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Rooms;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Unload. This class cannot be inherited.
    /// </summary>
    internal sealed class Unload : Command
    {
        /// <summary>
        /// The _re enter
        /// </summary>
        private readonly bool _reEnter;

        internal Unload()
        {
            Description = "Unload current room, all users will be removed.";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unload"/> class.
        /// </summary>
        /// <param name="reEnter">if set to <c>true</c> [re enter].</param>
        public Unload(bool reEnter = false)
        {
            MinParams = 0;
            _reEnter = reEnter;
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("user_is_staff") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id || session.GetHabbo().CurrentRoom.CheckRights(session);
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var roomId = session.GetHabbo().CurrentRoom.RoomId;
            var users = new List<RoomUser>(session.GetHabbo().CurrentRoom.GetRoomUserManager().UserList.Values);

            PurpleEmulator.GetGame().GetRoomManager().UnloadRoom(session.GetHabbo().CurrentRoom, "Unload command");

            if (!_reEnter) return;
            PurpleEmulator.GetGame().GetRoomManager().LoadRoom(roomId);

            var roomFwd = new ServerMessage(LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
            roomFwd.AppendInteger(roomId);

            var data = roomFwd.GetReversedBytes();

            foreach (var user in users.Where(user => user != null && user.GetClient() != null)) user.GetClient().SendMessage(data);
        }
    }
}