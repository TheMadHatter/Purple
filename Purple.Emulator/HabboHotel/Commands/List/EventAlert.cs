﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class EventAlert : Command
    {
        public EventAlert()
        {
            MinParams = -1;
            Description = "Start event and send alert to users.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            message.AppendString("events");
            message.AppendInteger(4);
            message.AppendString("title");
            message.AppendString(TextManager.GetText("alert_event_title"));
            message.AppendString("message");
            message.AppendString(TextManager.GetText("alert_event_message").Replace("{username}", session.GetHabbo().UserName).Replace("{extrainfo}", string.Join(" ", pms)));
            message.AppendString("linkUrl");
            message.AppendString("event:navigator/goto/" + session.GetHabbo().CurrentRoomId);
            message.AppendString("linkTitle");
            message.AppendString(TextManager.GetText("alert_event_goRoom"));

            PurpleEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(message);
        }
    }
}