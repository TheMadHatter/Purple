﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class TelePort. This class cannot be inherited.
    /// </summary>
    internal sealed class TelePort : Command
    {
        public TelePort()
        {
            MinParams = 0;
            Description = "Teleport in room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (user == null) return;
            if (!user.RidingHorse)
            {
                user.TeleportEnabled = !user.TeleportEnabled;
                room.GetGameMap().GenerateMaps(true);
                return;
            }
            session.SendWhisper(TextManager.GetText("command_error_teleport_enable"));
        }
    }
}