﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshSettings. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshSettings : Command
    {
        public RefreshSettings()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_settings_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PurpleEmulator.ConfigData = new ConfigData(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_settings_succesfully"));
        }
    }
}