﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassEnable. This class cannot be inherited.
    /// </summary>
    internal sealed class GoInvisible : Command
    {
        public GoInvisible()
        {
            MinParams = 0;
            Description = "Be invisible in the next room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            session.GetHabbo().SpectatorMode = true;
            session.SendNotif(TextManager.GetText("user_invisible"));
        }
    }
}