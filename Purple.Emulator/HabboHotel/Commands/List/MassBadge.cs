﻿#region

using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassBadge. This class cannot be inherited.
    /// </summary>
    internal sealed class MassBadge : Command
    {
        public MassBadge()
        {
            MinParams = 1;
            Description = "Hotel badge.";
            Usage = "[badge]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            foreach (var client in PurpleEmulator.GetGame().GetClientManager().Clients.Values.Where(client => client != null && client.GetHabbo() != null)) client.GetHabbo().GetBadgeComponent().GiveBadge(pms[0], true, client, false);

            session.SendNotif(TextManager.GetText("command_badge_give_done"));
            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, "All", "Badge", "Badge [" + pms[0] + "] given to all online users ATM");
        }
    }
}