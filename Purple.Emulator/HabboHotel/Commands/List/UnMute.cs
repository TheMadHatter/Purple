﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class UnMute. This class cannot be inherited.
    /// </summary>
    internal sealed class UnMute : Command
    {
        public UnMute()
        {
            MinParams = 1;
            Description = "Unmute user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null || client.GetHabbo() == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (client.GetHabbo().Rank >= 4)
            {
                session.SendWhisper("You are not allowed to mute that user.");
            }

            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName, "Unmute", "Unmuted user");
            client.GetHabbo().UnMute();
        }
    }
}