﻿#region

using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class PickAll. This class cannot be inherited.
    /// </summary>
    internal sealed class PickAll : Command
    {
        public PickAll()
        {
            MinParams = 0;
            Description = "Pickup all furni in the current room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            var roomItemList = room.GetRoomItemHandler().RemoveAllFurniture(session);
            if (session.GetHabbo().GetInventoryComponent() == null)
            {
                return;
            }

            room.GetRoomItemHandler().RemoveItemsByOwner(ref roomItemList, ref session);
        }
    }
}