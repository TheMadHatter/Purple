﻿#region

using System.Linq;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class FaceLess. This class cannot be inherited.
    /// </summary>
    internal sealed class FaceLess : Command
    {
        public FaceLess()
        {
            MinParams = 0;
            Description = "Faceless.";
        }

        public override bool CanExecute(GameClient session)
        {
            return !session.GetHabbo().VIP && session.GetHabbo().Rank == 1;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var head = session.GetHabbo().Look.Split('.').FirstOrDefault(element => element.StartsWith("hd-"));
            var color = "1";
            if (!string.IsNullOrEmpty(head))
            {
                color = head.Split('-')[2];
                session.GetHabbo().Look = session.GetHabbo().Look.Replace('.' + head, string.Empty);
            }
            session.GetHabbo().Look += ".hd-99999-" + color;

            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE users SET look = @look WHERE id = " + session.GetHabbo().Id);
                dbClient.AddParameter("look", session.GetHabbo().Look);
                dbClient.RunQuery();
            }
            var room = session.GetHabbo().CurrentRoom;
            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (user == null) return;

            var roomUpdate = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
            roomUpdate.AppendInteger(user.VirtualId);
            roomUpdate.AppendString(session.GetHabbo().Look);
            roomUpdate.AppendString(session.GetHabbo().Gender.ToLower());
            roomUpdate.AppendString(session.GetHabbo().Motto);
            roomUpdate.AppendInteger(session.GetHabbo().AchievementPoints);
            room.SendMessage(roomUpdate);
        }
    }
}