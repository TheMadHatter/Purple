﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshCatalogue. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshCatalogue : Command
    {
        public RefreshCatalogue()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_catalogue_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            {
                using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    FurniDataParser.SetCache();
                    PurpleEmulator.GetGame().GetItemManager().LoadItems(adapter);
                    PurpleEmulator.GetGame().GetCatalog().Initialize(adapter);
                    FurniDataParser.Clear();
                }
                PurpleEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(new ServerMessage(LibraryParser.OutgoingRequest("PublishShopMessageComposer")));
            }
        }
    }
}