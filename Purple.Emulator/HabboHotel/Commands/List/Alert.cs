﻿#region

using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Alert. This class cannot be inherited.
    /// </summary>
    internal sealed class Alert : Command
    {
        public Alert()
        {
            MinParams = -1;
            Description = "Send personal alert to user.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var userName = pms[0];
            var msg = string.Join(" ", pms.Skip(1));

            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(userName);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            client.SendNotif($"{msg} \r\r-{session.GetHabbo().UserName}");
        }
    }
}