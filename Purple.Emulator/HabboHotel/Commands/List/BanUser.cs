﻿#region

using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Support;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class BanUser. This class cannot be inherited.
    /// </summary>
    internal sealed class BanUser : Command
    {
        public BanUser()
        {
            MinParams = -2;
            Description = "Ban user by account.";
            Usage = "[username] [time] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                var length = int.Parse(pms[1]);

                var message = pms.Length < 3 ? string.Empty : string.Join(" ", pms.Skip(2));
                if (string.IsNullOrWhiteSpace(message)) message = TextManager.GetText("command_ban_user_no_reason");

                ModerationTool.BanUser(session, user.GetHabbo().Id, length, message);
                PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, user.GetHabbo().UserName, "Ban", $"USER:{pms[0]} TIME:{pms[1]} REASON:{message}");
            }
            catch
            {
                // error handle 
            }
        }
    }
}