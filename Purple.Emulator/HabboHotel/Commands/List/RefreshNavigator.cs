﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshNavigator. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshNavigator : Command
    {
        public RefreshNavigator()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_navigator_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PurpleEmulator.GetGame().GetNavigator().Initialize(adapter);
                PurpleEmulator.GetGame().GetRoomManager().LoadModels(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}