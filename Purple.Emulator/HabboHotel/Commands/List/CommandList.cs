﻿#region

using System.Linq;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class CommandList. This class cannot be inherited.
    /// </summary>
    internal sealed class CommandList : Command
    {
        public CommandList()
        {
            MinParams = -2;
            Description = "Shows this dialog.";
        }

        public override bool CanExecute(GameClient client)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var command = "-------------------------------- Commands --------------------------------\r\r";

            foreach (var kv in CommandsManager.CommandsDictionary.Where(kv => kv.Value.CanExecute(session)))
            {
                if (string.IsNullOrEmpty(kv.Value.Usage))
                {
                    command += $":{kv.Key} - {kv.Value.Description}\r\r";
                }
                else
                {
                    command += $":{kv.Key} {kv.Value.Usage} - {kv.Value.Description}\r\r";
                }
            }

            session.SendNotifWithScroll(command);
        }
    }
}