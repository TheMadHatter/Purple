﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshRanks. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshRanks : Command
    {
        public RefreshRanks()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_ranks_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PurpleEmulator.GetGame().GetRoleManager().LoadRights(adapter);
            }
            session.SendNotif(TextManager.GetText("command_refresh_ranks"));
        }
    }
}