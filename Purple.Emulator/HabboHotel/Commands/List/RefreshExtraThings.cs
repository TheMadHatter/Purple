﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshExtraThings : Command
    {
        public RefreshExtraThings()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_extra_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            PurpleEmulator.GetGame().GetHallOfFame().RefreshHallOfFame();
            PurpleEmulator.GetGame().GetRoomManager().GetCompetitionManager().RefreshCompetitions();
            PurpleEmulator.GetGame().GetTargetedOfferManager().LoadOffer();
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PurpleEmulator.GetGame().GetCraftingManager().Initialize(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}