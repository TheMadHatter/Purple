﻿#region

using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SummonAll. This class cannot be inherited.
    /// </summary>
    internal sealed class SummonAll : Command
    {
        public SummonAll()
        {
            MinParams = 0;
            Description = "Summon all users to room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var reason = string.Join(" ", pms);

            var messageBytes = GameClient.GetBytesNotif($"You have all been summoned by\r- {session.GetHabbo().UserName}:\r\n{reason}");
            foreach (var client in PurpleEmulator.GetGame().GetClientManager().Clients.Values)
            {
                if (session.GetHabbo().CurrentRoom == null || session.GetHabbo().CurrentRoomId == client.GetHabbo().CurrentRoomId) continue;

                client.GetMessageHandler().PrepareRoomForUser(session.GetHabbo().CurrentRoom.RoomId, session.GetHabbo().CurrentRoom.RoomData.PassWord);
                client.SendMessage(messageBytes);
            }
        }
    }
}