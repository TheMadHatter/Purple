﻿#region

using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Sit. This class cannot be inherited.
    /// </summary>
    internal sealed class Sit : Command
    {
        public Sit()
        {
            MinParams = 0;
            Description = "User can sit.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            session.GetMessageHandler().Sit();
        }
    }
}