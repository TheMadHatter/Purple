﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class UnBanUser. This class cannot be inherited.
    /// </summary>
    internal sealed class UnBanUser : Command
    {
        public UnBanUser()
        {
            MinParams = 1;
            Description = "Unban user by account.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = PurpleEmulator.GetHabboForName(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                adapter.SetQuery("DELETE FROM users_bans WHERE value = @name");
                adapter.AddParameter("name", user.UserName);
                adapter.RunQuery();
                PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, user.UserName, "Unban", $"User has been Unbanned [{pms[0]}]");
            }
        }
    }
}