﻿#region

using System;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Summon. This class cannot be inherited.
    /// </summary>
    internal sealed class Summon : Command
    {
        public Summon()
        {
            MinParams = 1;
            Description = "Summon user to room.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var userName = pms[0];
            if (string.Equals(userName, session.GetHabbo().UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                session.SendNotif(TextManager.GetText("summon_yourself"));
                return;
            }
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(userName);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }
            if (session.GetHabbo().CurrentRoom != null && session.GetHabbo().CurrentRoomId != client.GetHabbo().CurrentRoomId) client.GetMessageHandler().PrepareRoomForUser(session.GetHabbo().CurrentRoom.RoomId, session.GetHabbo().CurrentRoom.RoomData.PassWord);
        }
    }
}