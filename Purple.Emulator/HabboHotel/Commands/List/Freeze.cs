﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Freeze. This class cannot be inherited.
    /// </summary>
    internal sealed class Freeze : Command
    {
        public Freeze()
        {
            MinParams = 1;
            Description = "Freeze user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
            }
            else
            {
                user.Frozen = !user.Frozen;
            }
        }
    }
}