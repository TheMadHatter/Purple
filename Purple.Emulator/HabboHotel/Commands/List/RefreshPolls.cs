﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshPolls. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshPolls : Command
    {
        public RefreshPolls()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_polls_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                PurpleEmulator.GetGame().GetPollManager().Init(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}