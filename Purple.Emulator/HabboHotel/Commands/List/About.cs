﻿#region

using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class About. This class cannot be inherited.
    /// </summary>
    internal sealed class About : Command
    {
        public About()
        {
            MinParams = 0;
            Description = "Server information";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient client, string[] pms)
        {
            // Enter in the 'custom' your developers, for example:
            // const string custom = @"<b>Retro Hotel</b><br />- Gerard<br />- Jamal";
            const string custom = @"";

            // Please leave this line, we made this emulator with love <'3.
            client.SendNotifWithPicture("<center> <b><font color=\"#0174DF\" size=\"26\">        Azure</font> <font color=\"#000000\" size=\"26\">Sharp</font> <font color=\"000000\" size=\"12\"> " + PurpleEmulator.Version + "</font></b></center><br> <font color=\"000000\" size=\"9\">                                          Powered by Azure Group</font><br /><br /> <b>Azure Developers</b> <br />- TimNL<br />- Jamal<br />- Diesel<br />- Boris <br />- Lucca<br />- Antoine (Tyrex)<br />- Jaden<br />- IhToN<br />- Dominicus<br />- Cankiee<br />- Gerard" + (string.IsNullOrEmpty(custom) ? "" : "<br /><br />" + custom), "AzureSharp", "azure", "", "");
        }
    }
}