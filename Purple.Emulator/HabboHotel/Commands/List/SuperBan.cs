﻿#region

using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SuperBan. This class cannot be inherited.
    /// </summary>
    internal sealed class SuperBan : Command
    {
        public SuperBan()
        {
            MinParams = -1;
            Description = "Ban user by account for ever.";
            Usage = "[username] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }

            if (client.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendNotif(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            PurpleEmulator.GetGame().GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName, "Ban", "User has received a Super ban.");
            PurpleEmulator.GetGame().GetBanManager().BanUser(client, session.GetHabbo().UserName, 788922000.0, string.Join(" ", pms.Skip(1)), false, false);
        }
    }
}