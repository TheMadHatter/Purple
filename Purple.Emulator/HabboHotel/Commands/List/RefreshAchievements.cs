﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshAchievements. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshAchievements : Command
    {
        public RefreshAchievements()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_achievements_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            PurpleEmulator.GetGame().GetAchievementManager().LoadAchievements(PurpleEmulator.GetDatabaseManager().GetQueryReactor());
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}