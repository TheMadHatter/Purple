﻿#region

using AzureSharp.Configuration;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassDance. This class cannot be inherited.
    /// </summary>
    internal sealed class MassDance : Command
    {
        public MassDance()
        {
            MinParams = 1;
            Description = "Mass room dance.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort danceId;
            ushort.TryParse(pms[0], out danceId);

            if (danceId > 4)
            {
                session.SendWhisper(TextManager.GetText("command_dance_wrong_syntax"));
                return;
            }
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            var roomUsers = room.GetRoomUserManager().GetRoomUsers();

            foreach (var roomUser in roomUsers)
            {
                var message = new ServerMessage(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                message.AppendInteger(roomUser.VirtualId);
                message.AppendInteger(danceId);
                room.SendMessage(message);
                roomUser.DanceId = danceId;
            }
        }
    }
}