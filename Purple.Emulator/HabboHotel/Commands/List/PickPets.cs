﻿#region

using System.Linq;
using AzureSharp.HabboHotel.GameClients;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class PickPets. This class cannot be inherited.
    /// </summary>
    internal sealed class PickPets : Command
    {
        public PickPets()
        {
            MinParams = 0;
            Description = "Pickup all pets in the current room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id || session.GetHabbo().CurrentRoom.CheckRights(session);
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            foreach (var pet in
                room.GetRoomUserManager().GetPets().Where(pet => pet.OwnerId == session.GetHabbo().Id))
            {
                session.GetHabbo().GetInventoryComponent().AddPet(pet);
                room.GetRoomUserManager().RemoveBot(pet.VirtualId, false);
            }
            session.SendMessage(session.GetHabbo().GetInventoryComponent().SerializePetInventory());
        }
    }
}