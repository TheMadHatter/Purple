﻿#region

using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class HotelAlert : Command
    {
        public HotelAlert()
        {
            MinParams = -1;
            Description = "Send hotel alert.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("admin");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var str = string.Join(" ", pms);
            var message = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
            message.AppendString($"{str}\r\n- {session.GetHabbo().UserName}");
            PurpleEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(message);
        }
    }
}