#region

using System;
using System.Linq;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Rooms;
using AzureSharp.Messages;

#endregion

namespace AzureSharp.HabboHotel.Users.Messenger
{
    /// <summary>
    /// Class MessengerBuddy.
    /// </summary>
    internal class MessengerBuddy
    {
        /// <summary>
        /// The _appear offline
        /// </summary>
        private readonly bool _appearOffline;


        /// <summary>
        /// The _hide inroom
        /// </summary>
        private readonly bool _hideInroom;

        private readonly int _lastOnline;

        /// <summary>
        /// The _look
        /// </summary>
        private string _look;

        /// <summary>
        /// The _motto
        /// </summary>
        private string _motto;

        /// <summary>
        /// The client
        /// </summary>
        internal GameClient Client;

        /// <summary>
        /// The user name
        /// </summary>
        internal string UserName;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessengerBuddy"/> class.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="look">The look.</param>
        /// <param name="motto">The motto.</param>
        /// <param name="lastOnline">The last online.</param>
        /// <param name="appearOffline">if set to <c>true</c> [appear offline].</param>
        /// <param name="hideInroom">if set to <c>true</c> [hide inroom].</param>
        internal MessengerBuddy(uint userId, string userName, string look, string motto, int lastOnline, bool appearOffline, bool hideInroom)
        {
            Id = userId;
            UserName = userName;
            _look = look;
            _motto = motto;
            _lastOnline = lastOnline;
            _appearOffline = appearOffline;
            _hideInroom = hideInroom;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        internal uint Id { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is online.
        /// </summary>
        /// <value><c>true</c> if this instance is online; otherwise, <c>false</c>.</value>
        internal bool IsOnline
        {
            get { return Client != null && Client.GetHabbo() != null && Client.GetHabbo().GetMessenger() != null && !Client.GetHabbo().GetMessenger().AppearOffline; }
        }

        /// <summary>
        /// Gets a value indicating whether [in room].
        /// </summary>
        /// <value><c>true</c> if [in room]; otherwise, <c>false</c>.</value>
        internal bool InRoom
        {
            get { return CurrentRoom != null; }
        }

        /// <summary>
        /// Gets or sets the current room.
        /// </summary>
        /// <value>The current room.</value>
        internal Room CurrentRoom { get; set; }

        /// <summary>
        /// Updates the user.
        /// </summary>
        internal void UpdateUser()
        {
            Client = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(Id);
            if (Client == null || Client.GetHabbo() == null) return;
            CurrentRoom = Client.GetHabbo().CurrentRoom;
            _look = Client.GetHabbo().Look;
            _motto = Client.GetHabbo().Motto;
        }

        /// <summary>
        /// Serializes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="session">The session.</param>
        internal void Serialize(ServerMessage message, GameClient session)
        {
            if (Id == 0)
            {
                message.AppendInteger(Id);
                message.AppendString(UserName);
                message.AppendInteger(1);
                message.AppendBool(true);
                message.AppendBool(false);
                message.AppendString(_look);
                message.AppendInteger(0); // category
                message.AppendString(string.Empty);
                message.AppendString(string.Empty); // realname
                message.AppendString(string.Empty); // shit?
                message.AppendBool(true); // offline message
                message.AppendBool(false); // pocket user
                message.AppendBool(false); // pocketHabboUser
                message.AppendShort(0);
            }
            else
            {
                var value = session.GetHabbo().Relationships.FirstOrDefault(x => x.Value.UserId == Convert.ToInt32(Id)).Value;
                var i = value == null ? 0 : value.Type;
                message.AppendInteger(Id);
                message.AppendString(UserName);
                message.AppendInteger(IsOnline);
                message.AppendBool(IsOnline);
                message.AppendBool(InRoom);
                message.AppendString(IsOnline ? _look : string.Empty);
                message.AppendInteger(0); // category
                message.AppendString(IsOnline ? _motto : string.Empty);
                message.AppendString(string.Empty); // realname
                message.AppendString(string.Empty); // shit?
                message.AppendBool(true); // offline message
                message.AppendBool(false); // pocket user
                message.AppendBool(false); // pocketHabboUser
                message.AppendShort(i);
            }
        }
    }
}