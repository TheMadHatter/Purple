﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Timers;
using AzureSharp.Configuration;
using Purple.Database.Queries;
using AzureSharp.HabboHotel.Achievements;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.HabboHotel.Groups.Structs;
using AzureSharp.HabboHotel.Navigators;
using AzureSharp.HabboHotel.Rooms;
using AzureSharp.HabboHotel.Users.Badges;
using AzureSharp.HabboHotel.Users.Inventory;
using AzureSharp.HabboHotel.Users.Messenger;
using AzureSharp.HabboHotel.Users.Relationships;
using AzureSharp.HabboHotel.Users.Subscriptions;
using AzureSharp.HabboHotel.Users.UserDataManagement;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;


#endregion

namespace AzureSharp.HabboHotel.Users
{
    public class Habbo
    {
        private readonly List<int> _myGroups;
        private AvatarEffectsInventoryComponent _avatarEffectsInventoryComponent;
        private BadgeComponent _badgeComponent;
        internal UserClothing _clothingManager;
        private bool _habboinfoSaved;
        private InventoryComponent _inventoryComponent;
        private bool _loadedMyGroups;
        private GameClient _mClient;
        private HabboMessenger _messenger;
        private SubscriptionManager _subscriptionManager;
        internal YoutubeManager _youtubeManager;
        internal Dictionary<string, UserAchievement> Achievements;
        internal HashSet<uint> AnsweredPolls;
        internal bool answeredPool = false;
        internal bool AppearOffline;
        internal int BobbaFiltered = 0;
        internal int BuildersExpire;
        internal int BuildersItemsMax;
        internal int BuildersItemsUsed;
        internal double CreateDate;
        internal int Credits, AchievementPoints, ActivityPoints;
        internal uint CurrentQuestId;
        internal uint CurrentRoomId;
        internal int CurrentTalentLevel;
        internal int Diamonds;
        internal bool Disconnected;
        internal uint DutyLevel;
        internal List<uint> FavoriteRooms;
        internal int FavouriteGroup;
        internal int FloodTime;
        internal uint FriendCount;
        public GameClient GuideOtherUser;
        internal bool HasFriendRequestsDisabled;
        internal bool HideInRoom;
        internal uint HomeRoom;
        internal uint HopperId;
        internal uint Id;
        internal bool IsHopping;
        internal bool IsTeleporting;
        internal int LastChange;
        internal DateTime LastGiftOpenTime;
        internal DateTime LastGiftPurchaseTime;
        internal int LastOnline;
        internal uint LastQuestCompleted;
        public uint LastSelectedUser = 0;
        internal int LastSqlQuery;
        public DateTime LastUsed = DateTime.Now;
        internal bool LoadingChecksPassed;
        internal uint LoadingRoom;
        internal uint MinimailUnreadMessages;
        internal bool Muted;
        internal List<uint> MutedUsers;
        internal Dictionary<int, NaviLogs> NavigatorLogs;
        internal bool NuxPassed;
        internal bool OnDuty;
        internal bool OwnRoomsSerialized = false;
        internal UserPreferences Preferences;
        internal int PreviousOnline;
        internal Dictionary<uint, int> Quests;
        internal uint Rank;
        internal HashSet<uint> RatedRooms;
        internal LinkedList<uint> RecentlyVisitedRooms;
        internal Dictionary<int, Relationship> Relationships;
        internal string ReleaseName;
        internal int Respect, DailyRespectPoints, DailyPetRespectPoints, DailyCompetitionVotes;
        internal DateTime SpamFloodTime;
        internal bool SpamProtectionBol;
        internal int SpamProtectionCount = 1, SpamProtectionTime, SpamProtectionAbuse;
        internal bool SpectatorMode;
        internal List<string> Tags;
        internal Dictionary<int, UserTalent> Talents;
        internal string TalentStatus;
        internal uint TeleporterId;
        internal uint TeleportingRoomId;
        internal DateTime TimeLoggedOn;
        public bool timer_Elapsed;
        internal bool TradeLocked;
        internal int TradeLockExpire;
        internal List<GroupUser> UserGroups;
        internal string UserName, RealName, Motto, Look, Gender;
        internal HashSet<RoomData> UsersRooms;
        internal bool VIP;


        internal Habbo(uint id, string userName, string realName, uint rank, string motto, string look, string gender, int credits, int activityPoints, double lastActivityPointsUpdate, bool muted, uint homeRoom, int respect, int dailyRespectPoints, int dailyPetRespectPoints, bool hasFriendRequestsDisabled, uint currentQuestId, int currentQuestProgress, int achievementPoints, int regTimestamp, int lastOnline, bool appearOffline, bool hideInRoom, bool vip, double createDate, bool online, string citizenShip, int diamonds, List<GroupUser> groups, int favId, int lastChange, bool tradeLocked, int tradeLockExpire, bool nuxPassed, int buildersExpire, int buildersItemsMax, int buildersItemsUsed, int releaseVersion, bool onDuty, Dictionary<int, NaviLogs> naviLogs, int dailyCompetitionVotes, uint dutyLevel)
        {
            Id = id;
            UserName = userName;
            RealName = realName;
            _myGroups = new List<int>();
            BuildersExpire = buildersExpire;
            BuildersItemsMax = buildersItemsMax;
            BuildersItemsUsed = buildersItemsUsed;
            if (rank < 1u) rank = 1u;
            ReleaseName = string.Empty;

            OnDuty = onDuty;
            DutyLevel = dutyLevel;

            Rank = rank;
            Motto = motto;
            Look = look.ToLower();
            VIP = rank > 5 || vip;
            LastChange = lastChange;
            TradeLocked = tradeLocked;
            NavigatorLogs = naviLogs;
            TradeLockExpire = tradeLockExpire;
            Gender = gender.ToLower();
            Credits = credits;
            ActivityPoints = activityPoints;
            Diamonds = diamonds;
            AchievementPoints = achievementPoints;
            Muted = muted;
            LoadingRoom = 0u;
            CreateDate = createDate;
            LoadingChecksPassed = false;
            FloodTime = 0;
            NuxPassed = nuxPassed;
            CurrentRoomId = 0u;
            TimeLoggedOn = DateTime.Now;
            HomeRoom = homeRoom;
            HideInRoom = hideInRoom;
            AppearOffline = appearOffline;
            FavoriteRooms = new List<uint>();
            MutedUsers = new List<uint>();
            Tags = new List<string>();
            Achievements = new Dictionary<string, UserAchievement>();
            Talents = new Dictionary<int, UserTalent>();
            Relationships = new Dictionary<int, Relationship>();
            RatedRooms = new HashSet<uint>();
            Respect = respect;
            DailyRespectPoints = dailyRespectPoints;
            DailyPetRespectPoints = dailyPetRespectPoints;
            IsTeleporting = false;
            TeleporterId = 0u;
            UsersRooms = new HashSet<RoomData>();
            HasFriendRequestsDisabled = hasFriendRequestsDisabled;
            LastOnline = Utility.GetUnixTimeStamp();
            PreviousOnline = lastOnline;
            RecentlyVisitedRooms = new LinkedList<uint>();
            CurrentQuestId = currentQuestId;
            IsHopping = false;

            FavouriteGroup = PurpleEmulator.GetGame().GetGroupManager().GetGroup(favId) != null ? favId : 0;
            UserGroups = groups;
            if (DailyPetRespectPoints > 99) DailyPetRespectPoints = 99;
            if (DailyRespectPoints > 99) DailyRespectPoints = 99;
            LastGiftPurchaseTime = DateTime.Now;
            LastGiftOpenTime = DateTime.Now;
            TalentStatus = citizenShip;
            CurrentTalentLevel = GetCurrentTalentLevel();
            DailyCompetitionVotes = dailyCompetitionVotes;
        }

        public bool CanChangeName
        {
            get { return (ExtraSettings.CHANGE_NAME_STAFF && HasFuse("user_can_change_name")) || (ExtraSettings.CHANGE_NAME_VIP && VIP) || (ExtraSettings.CHANGE_NAME_EVERYONE && Utility.GetUnixTimeStamp() > LastChange + 604800); }
        }
			
        internal string HeadPart
        {
            get
            {
                var strtmp = Look.Split('.');
                var tmp2 = strtmp.FirstOrDefault(x => x.Contains("hd-"));
                var lookToReturn = tmp2 ?? "";

                if (Look.Contains("ha-")) lookToReturn += $".{strtmp.FirstOrDefault(x => x.Contains("ha-"))}";
                if (Look.Contains("ea-")) lookToReturn += $".{strtmp.FirstOrDefault(x => x.Contains("ea-"))}";
                if (Look.Contains("hr-")) lookToReturn += $".{strtmp.FirstOrDefault(x => x.Contains("hr-"))}";
                if (Look.Contains("he-")) lookToReturn += $".{strtmp.FirstOrDefault(x => x.Contains("he-"))}";
                if (Look.Contains("fa-")) lookToReturn += $".{strtmp.FirstOrDefault(x => x.Contains("fa-"))}";

                return lookToReturn;
            }
        }

        internal bool InRoom
        {
            get { return CurrentRoomId >= 1 && CurrentRoom != null; }
        }
			
        internal Room CurrentRoom
        {
            get { return CurrentRoomId <= 0u ? null : PurpleEmulator.GetGame().GetRoomManager().GetRoom(CurrentRoomId); }
        }
			
        internal bool IsHelper
        {
            get { return TalentStatus == "helper" || Rank >= 4; }
        }

        internal bool IsCitizen
        {
            get { return CurrentTalentLevel > 4; }
        }
			
        internal string GetQueryString
        {
            get
            {
                _habboinfoSaved = true;
                return string.Concat("UPDATE users SET online='0', last_online = '", Utility.GetUnixTimeStamp(), "', activity_points = '", ActivityPoints, "', diamonds = '", Diamonds, "', credits = '", Credits, "' WHERE id = '", Id, "'; UPDATE users_stats SET achievement_score = ", AchievementPoints, " WHERE id=", Id, " LIMIT 1; ");
            }
        }
			
        internal List<int> MyGroups
        {
            get
            {
                if (!_loadedMyGroups) _LoadMyGroups();

                return _myGroups;
            }
        }
			
        public void timer_ElapsedEvent(object source, ElapsedEventArgs e)
        {
            timer_Elapsed = true;
        }


        internal void InitInformation(UserData data)
        {
            _subscriptionManager = new SubscriptionManager(Id, data);
            _badgeComponent = new BadgeComponent(Id, data);
            Quests = data.Quests;
            _messenger = new HabboMessenger(Id);
            _messenger.Init(data.Friends, data.Requests);
            SpectatorMode = false;
            Disconnected = false;
            UsersRooms = data.Rooms;
            Relationships = data.Relations;
            AnsweredPolls = data.SuggestedPolls;
        }
			
        internal void Init(GameClient client, UserData data)
        {
            _mClient = client;
            _subscriptionManager = new SubscriptionManager(Id, data);
            _badgeComponent = new BadgeComponent(Id, data);
            _inventoryComponent = new InventoryComponent(Id, client, data);
            _inventoryComponent.SetActiveState(client);
            _avatarEffectsInventoryComponent = new AvatarEffectsInventoryComponent(Id, client, data);
            Quests = data.Quests;
            _messenger = new HabboMessenger(Id);
            _messenger.Init(data.Friends, data.Requests);
            FriendCount = Convert.ToUInt32(data.Friends.Count);
            SpectatorMode = false;
            Disconnected = false;
            UsersRooms = data.Rooms;
            MinimailUnreadMessages = data.MiniMailCount;
            Relationships = data.Relations;
            AnsweredPolls = data.SuggestedPolls;
            _clothingManager = new UserClothing(Id);
            Preferences = new UserPreferences(Id);
            _youtubeManager = new YoutubeManager(Id);
        }
			
        internal void UpdateRooms()
        {
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                UsersRooms.Clear();
                dbClient.SetQuery("SELECT * FROM rooms_data WHERE owner = @name ORDER BY id ASC LIMIT 50");
                dbClient.AddParameter("name", UserName);
                var table = dbClient.GetTable();
                foreach (DataRow dataRow in table.Rows) UsersRooms.Add(PurpleEmulator.GetGame().GetRoomManager().FetchRoomData(Convert.ToUInt32(dataRow["id"]), dataRow));
            }
        }
			
        internal void LoadData(UserData data)
        {
            LoadAchievements(data.Achievements);
            LoadTalents(data.Talents);
            LoadFavorites(data.FavouritedRooms);
            LoadMutedUsers(data.Ignores);
            LoadTags(data.Tags);
        }
			
        internal void SerializeQuests(ref QueuedServerMessage response)
        {
            PurpleEmulator.GetGame().GetQuestManager().GetList(_mClient, null);
        }
			
        internal bool HasFuse(string fuse)
        {
            if (PurpleEmulator.GetGame().GetRoleManager().UserHasPersonalPermissions(Id))
            {
                return PurpleEmulator.GetGame().GetRoleManager().UserHasPermission(Id, fuse);
            }
            return PurpleEmulator.GetGame().GetRoleManager().RankHasRight(Rank, fuse);
        }
			
        internal void LoadFavorites(List<uint> roomId)
        {
            FavoriteRooms = roomId;
        }
			
        internal void LoadMutedUsers(List<uint> usersMuted)
        {
            MutedUsers = usersMuted;
        }

        internal void LoadTags(List<string> tags)
        {
            Tags = tags;
        }
			
        internal void SerializeClub()
        {
            var client = GetClient();
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("SubscriptionStatusMessageComposer"));
            serverMessage.AppendString("club_habbo");
            if (client.GetHabbo().GetSubscriptionManager().HasSubscription)
            {
                double num = client.GetHabbo().GetSubscriptionManager().GetSubscription().ExpireTime;
                var num2 = num - Utility.GetUnixTimeStamp();

                {
                    var num3 = (int) Math.Ceiling(num2 / 86400.0);
                    var i = (int) Math.Ceiling((Utility.GetUnixTimeStamp() - (double) client.GetHabbo().GetSubscriptionManager().GetSubscription().ActivateTime) / 86400.0);
                    var num4 = num3 / 31;
                    if (num4 >= 1) num4--;
                    serverMessage.AppendInteger(num3 - num4 * 31);
                    serverMessage.AppendInteger(1);
                    serverMessage.AppendInteger(num4);
                    serverMessage.AppendInteger(1);
                    serverMessage.AppendBool(true);
                    serverMessage.AppendBool(true);
                    serverMessage.AppendInteger(i);
                    serverMessage.AppendInteger(i);
                    serverMessage.AppendInteger(10);
                }
            }
            else
            {
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendBool(false);
                serverMessage.AppendBool(false);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
            }
            client.SendMessage(serverMessage);
            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("UserClubRightsMessageComposer"));
            serverMessage2.AppendInteger(GetSubscriptionManager().HasSubscription ? 2 : 0);
            serverMessage2.AppendInteger(Rank);
            serverMessage2.AppendBool(Rank >= Convert.ToUInt32(PurpleEmulator.GetDbConfig().DbData["ambassador.minrank"]));
            client.SendMessage(serverMessage2);
        }

        /// <summary>
        /// Loads the achievements.
        /// </summary>
        /// <param name="achievements">The achievements.</param>
        internal void LoadAchievements(Dictionary<string, UserAchievement> achievements)
        {
            Achievements = achievements;
        }

        /// <summary>
        /// Loads the talents.
        /// </summary>
        /// <param name="talents">The talents.</param>
        internal void LoadTalents(Dictionary<int, UserTalent> talents)
        {
            Talents = talents;
        }

        /// <summary>
        /// Called when [disconnect].
        /// </summary>
        /// <param name="reason">The reason.</param>
        internal void OnDisconnect(string reason)
        {
            if (Disconnected) return;
            Disconnected = true;
            if (_inventoryComponent != null)
            {
                _inventoryComponent.RunDbUpdate();
                _inventoryComponent.SetIdleState();
            }
            var navilogs = string.Empty;
            if (NavigatorLogs.Any())
            {
                navilogs = NavigatorLogs.Values.Aggregate(navilogs, (current, navi) => current + $"{navi.Id},{navi.Value1},{navi.Value2};");
                navilogs = navilogs.Remove(navilogs.Length - 1);
            }
            PurpleEmulator.GetGame().GetClientManager().UnregisterClient(Id, UserName);

            Logger.Debug(UserName + " disconnected from game. Reason: " + reason);
            var GetOnlineSeconds = DateTime.Now - TimeLoggedOn;
            var SecondsToGive = GetOnlineSeconds.Seconds;
            if (!_habboinfoSaved)
            {
                _habboinfoSaved = true;
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("UPDATE users SET activity_points = " + ActivityPoints + ", credits = " + Credits + ", diamonds = " + Diamonds + ", online='0', last_online = '" + Utility.GetUnixTimeStamp() + "', builders_items_used = " + BuildersItemsUsed + ", navilogs = @navilogs  WHERE id = " + Id + " LIMIT 1;UPDATE users_stats SET achievement_score=" + AchievementPoints + " WHERE id=" + Id + " LIMIT 1;");
                    queryReactor.AddParameter("navilogs", navilogs);
                    queryReactor.RunQuery();
                    queryReactor.RunFastQuery("UPDATE users_stats SET online_seconds = online_seconds + " + SecondsToGive + " WHERE id = " + Id);
                    if (HasFuse("moderator")) queryReactor.RunFastQuery($"UPDATE moderation_tickets SET status='open', moderator_id=0 WHERE status='picked' AND moderator_id={Id}");
                }
            }
            if (InRoom && CurrentRoom != null) CurrentRoom.GetRoomUserManager().RemoveUserFromRoom(_mClient, false, false);
            if (_messenger != null)
            {
                _messenger.AppearOffline = true;
                _messenger.Destroy();
                _messenger = null;
            }

            if (_avatarEffectsInventoryComponent != null) _avatarEffectsInventoryComponent.Dispose();
            _mClient = null;
        }

        /// <summary>
        /// Initializes the messenger.
        /// </summary>
        internal void InitMessenger()
        {
            var client = GetClient();
            if (client == null) return;
            client.SendMessage(_messenger.SerializeCategories());
            client.SendMessage(_messenger.SerializeFriends());

            client.SendMessage(_messenger.SerializeRequests());
            if (PurpleEmulator.OfflineMessages.ContainsKey(Id))
            {
                var list = PurpleEmulator.OfflineMessages[Id];
                foreach (var current in list) client.SendMessage(_messenger.SerializeOfflineMessages(current));
                PurpleEmulator.OfflineMessages.Remove(Id);
                OfflineMessage.RemoveAllMessages(PurpleEmulator.GetDatabaseManager().GetQueryReactor(), Id);
            }
            if (_messenger.Requests.Count > PurpleEmulator.FriendRequestLimit)
            {
                client.SendNotif(TextManager.GetText("user_friend_request_max"));
            }

            _messenger.OnStatusChanged(false);
        }

        /// <summary>
        /// Updates the credits balance.
        /// </summary>
        internal void UpdateCreditsBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null || _mClient.GetMessageHandler().GetResponse() == null) return;
            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("CreditsBalanceMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendString($"{Credits}.0");
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Updates the activity points balance.
        /// </summary>
        internal void UpdateActivityPointsBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null || _mClient.GetMessageHandler().GetResponse() == null) return;
            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(3);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(Diamonds);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(105);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(Diamonds);
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Updates the seasonal currency balance.
        /// </summary>
        internal void UpdateSeasonalCurrencyBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null || _mClient.GetMessageHandler().GetResponse() == null) return;
            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(3);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(Diamonds);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(105);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(Diamonds);
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Notifies the new pixels.
        /// </summary>
        /// <param name="change">The change.</param>
        internal void NotifyNewPixels(int change)
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null || _mClient.GetMessageHandler().GetResponse() == null) return;
            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ActivityPointsNotificationMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(change);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Notifies the new diamonds.
        /// </summary>
        /// <param name="change">The change.</param>
        internal void NotifyNewDiamonds(int change)
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null || _mClient.GetMessageHandler().GetResponse() == null) return;

            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ActivityPointsNotificationMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(Diamonds);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(change);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Notifies the voucher.
        /// </summary>
        /// <param name="isValid">if set to <c>true</c> [is valid].</param>
        /// <param name="productName">Name of the product.</param>
        /// <param name="productDescription">The product description.</param>
        internal void NotifyVoucher(bool isValid, string productName, string productDescription)
        {
            if (isValid)
            {
                _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("VoucherValidMessageComposer"));
                _mClient.GetMessageHandler().GetResponse().AppendString(productName);
                _mClient.GetMessageHandler().GetResponse().AppendString(productDescription);
                _mClient.GetMessageHandler().SendResponse();
                return;
            }
            _mClient.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("VoucherErrorMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendString("1");
            _mClient.GetMessageHandler().SendResponse();
        }

        /// <summary>
        /// Mutes this instance.
        /// </summary>
        internal void Mute()
        {
            if (!Muted) Muted = true;
        }

        /// <summary>
        /// Uns the mute.
        /// </summary>
        internal void UnMute()
        {
            if (Muted) GetClient().SendNotif("You were unmuted.");

            Muted = false;
            if (CurrentRoom != null && CurrentRoom.MutedUsers.ContainsKey(Id)) CurrentRoom.MutedUsers.Remove(Id);
        }

        /// <summary>
        /// Gets the subscription manager.
        /// </summary>
        /// <returns>SubscriptionManager.</returns>
        internal SubscriptionManager GetSubscriptionManager()
        {
            return _subscriptionManager;
        }

        internal YoutubeManager GetYoutubeManager()
        {
            return _youtubeManager;
        }

        /// <summary>
        /// Gets the messenger.
        /// </summary>
        /// <returns>HabboMessenger.</returns>
        internal HabboMessenger GetMessenger()
        {
            return _messenger;
        }

        /// <summary>
        /// Gets the badge component.
        /// </summary>
        /// <returns>BadgeComponent.</returns>
        internal BadgeComponent GetBadgeComponent()
        {
            return _badgeComponent;
        }

        /// <summary>
        /// Gets the inventory component.
        /// </summary>
        /// <returns>InventoryComponent.</returns>
        internal InventoryComponent GetInventoryComponent()
        {
            return _inventoryComponent;
        }

        /// <summary>
        /// Gets the avatar effects inventory component.
        /// </summary>
        /// <returns>AvatarEffectsInventoryComponent.</returns>
        internal AvatarEffectsInventoryComponent GetAvatarEffectsInventoryComponent()
        {
            return _avatarEffectsInventoryComponent;
        }

        /// <summary>
        /// Runs the database update.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        internal void RunDbUpdate(Query dbClient)
        {
            dbClient.RunFastQuery(string.Concat("UPDATE users SET last_online = '", Utility.GetUnixTimeStamp(), "', activity_points = '", ActivityPoints, "', credits = '", Credits, "', diamonds = '", Diamonds, "' WHERE id = '", Id, "' LIMIT 1; "));
        }

        /// <summary>
        /// Gets the quest progress.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns>System.Int32.</returns>
        internal int GetQuestProgress(uint p)
        {
            int result;
            Quests.TryGetValue(p, out result);
            return result;
        }

        /// <summary>
        /// Gets the achievement data.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns>UserAchievement.</returns>
        internal UserAchievement GetAchievementData(string p)
        {
            UserAchievement result;
            Achievements.TryGetValue(p, out result);
            return result;
        }

        /// <summary>
        /// Gets the talent data.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>UserTalent.</returns>
        internal UserTalent GetTalentData(int t)
        {
            UserTalent result;
            Talents.TryGetValue(t, out result);
            return result;
        }

        /// <summary>
        /// Gets the current talent level.
        /// </summary>
        /// <returns>System.Int32.</returns>
        internal int GetCurrentTalentLevel()
        {
            return Talents.Values.Select(current => PurpleEmulator.GetGame().GetTalentManager().GetTalent(current.TalentId).Level).Concat(new[] {1}).Max();
        }

        /// <summary>
        /// _s the load my groups.
        /// </summary>
        internal void _LoadMyGroups()
        {
            DataTable dTable;
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery($"SELECT id FROM groups_data WHERE owner_id = {Id}");
                dTable = dbClient.GetTable();
            }

            foreach (DataRow dRow in dTable.Rows) _myGroups.Add(Convert.ToInt32(dRow["id"]));

            _loadedMyGroups = true;
        }

        /// <summary>
        /// Gots the poll data.
        /// </summary>
        /// <param name="pollId">The poll identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool GotPollData(uint pollId)
        {
            return AnsweredPolls.Contains(pollId);
        }

        /// <summary>
        /// Checks the trading.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool CheckTrading()
        {
            if (!TradeLocked) return true;
            if (TradeLockExpire - Utility.GetUnixTimeStamp() > 0) return false;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryReactor.RunFastQuery($"UPDATE users SET trade_lock = '0' WHERE id = {Id}");
            TradeLocked = false;
            return true;
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <returns>GameClient.</returns>
        private GameClient GetClient()
        {
            return PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(Id);
        }
    }
}