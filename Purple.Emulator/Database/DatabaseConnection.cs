#region

using System;
using System.Data;
using Purple.Database.Queries;
using MySql.Data.MySqlClient;

#endregion

namespace Purple.Database
{
    public class DatabaseConnection : IDisposable
    {
        private readonly Query _adapter;
        private readonly MySqlConnection _mysqlConnection;

        public DatabaseConnection(string connectionStr)
        {
            _mysqlConnection = new MySqlConnection(connectionStr);
            _adapter = new Query(this);
        }

        public void Dispose()
        {
            if (_mysqlConnection.State == ConnectionState.Open)
            {
                _mysqlConnection.Close();
            }
        }

        public void Connect()
        {
            Open();
        }

        public void Disconnect()
        {
            Close();
        }

        public Query GetQueryReactor()
        {
            return _adapter;
        }

        public bool IsAvailable()
        {
            return false;
        }

        public void Prepare()
        {
        }

        public void ReportDone()
        {
            Dispose();
        }
        public MySqlCommand CreateNewCommandMySql()
        {
            return _mysqlConnection.CreateCommand();
        }

        public MySqlTransaction GetTransactionMySql()
        {
            return _mysqlConnection.BeginTransaction();
        }

        public void Open()
        {
            _mysqlConnection.Open();
        }

        public void Close()
        {
            _mysqlConnection.Close();
        }
    }
}