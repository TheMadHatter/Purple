#region

using System;
using System.Data;
using MySql.Data.MySqlClient;

#endregion

namespace Purple.Database.Queries
{
    public class Query : IDisposable
    {
        protected DatabaseConnection Client;
        protected MySqlCommand CommandMySql;

        public Query(DatabaseConnection client)
        {
            Client = client;
            CommandMySql = client.CreateNewCommandMySql();
        }

        public void AddParameter(string parameterName, object val)
        {
            CommandMySql.Parameters.AddWithValue(parameterName, val);
        }

        public bool FindsResult()
        {
            using (var reader = CommandMySql.ExecuteReader()) return reader.HasRows;
        }

        public int GetInteger()
        {
            var result = 0;
            try
            {
                var obj2 = CommandMySql.ExecuteScalar();
                if (obj2 != null) int.TryParse(obj2.ToString(), out result);
            }
            catch (Exception exception)
            {
				Console.WriteLine (exception.ToString ());
            }
            return result;
        }

        public DataRow GetRow()
        {
            var dataSet = new DataSet();
            using (var adapter = new MySqlDataAdapter(CommandMySql)) adapter.Fill(dataSet);
            return (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count == 1) ? dataSet.Tables[0].Rows[0] : null;
        }

        public string GetString()
        {
            return CommandMySql.ExecuteScalar()?.ToString();
        }

        public DataTable GetTable()
        {
            var dataTable = new DataTable();
            using (var adapter = new MySqlDataAdapter(CommandMySql)) adapter.Fill(dataTable);
            return dataTable;
        }

        public void RunFastQuery(string query)
        {
            SetQuery(query);
            RunQuery();
        }

        public void SetQuery(string query)
        {
            CommandMySql.Parameters.Clear();
            CommandMySql.CommandText = query;
        }

        public void AddParameter(string name, byte[] data)
        {
            CommandMySql.Parameters.Add(new MySqlParameter(name, MySqlDbType.Blob, data.Length));
        }

        public long InsertQuery()
        {
            CommandMySql.ExecuteScalar();
            return CommandMySql.LastInsertedId;
        }

        public void RunQuery()
        {
            CommandMySql.ExecuteNonQuery();
        }

        public void Dispose()
        {
            CommandMySql.Dispose();
            Client.ReportDone();
        }
    }
}