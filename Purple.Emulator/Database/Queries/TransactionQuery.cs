#region

using Purple.Database.Exceptions;
using MySql.Data.MySqlClient;

#endregion

namespace Purple.Database.Queries
{
    public class TransactionQuery : Query
    {
        private bool _finishedTransaction;
        private MySqlTransaction _transactionmysql;

        public TransactionQuery(DatabaseConnection client) : base(client)
        {
            InitTransaction();
        }

        public new void Dispose()
        {
            if (!_finishedTransaction) throw new TransactionException("The transaction needs to be completed by commit() or rollback() before you can dispose this item.");
            base.Dispose();
        }

        public void DoCommit()
        {
            try
            {
                _transactionmysql.Commit();
                _finishedTransaction = true;
            }
            catch (MySqlException ex)
            {
                throw new TransactionException(ex.Message);
            }
        }

        public void DoRollBack()
        {
            try
            {
                _transactionmysql.Rollback();
                _finishedTransaction = true;
            }
            catch (MySqlException ex)
            {
                throw new TransactionException(ex.Message);
            }
        }

        public bool GetAutoCommit()
        {
            return false;
        }

        private void InitTransaction()
        {
            CommandMySql = Client.CreateNewCommandMySql();
            _transactionmysql = Client.GetTransactionMySql();
            CommandMySql.Transaction = _transactionmysql;
            CommandMySql.Connection = _transactionmysql.Connection;

            _finishedTransaction = false;
        }
    }
}