using System;
using Purple.Database.Queries;

namespace Purple.Database
{
    public sealed class DatabaseManager : IDisposable
    {
        private readonly string _connectionStr;

        public DatabaseManager(string connectionStr)
        {
            _connectionStr = connectionStr;
        }

        public Query GetQueryReactor()
        {
            var databaseClient = new DatabaseConnection(_connectionStr);
            databaseClient.Connect();
            databaseClient.Prepare();
            return databaseClient.GetQueryReactor();
        }

        public void Destroy()
        {
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}