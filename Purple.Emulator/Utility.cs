﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzureSharp.Util;

namespace AzureSharp
{
    internal static class Utility
    {
        private static readonly HashSet<char> AllowedSpecialChars = new HashSet<char>(new[] { '-', '.', ' ', 'Ã', '©', '¡', '­', 'º', '³', 'Ã', '‰', '_' });

        internal static bool EnumToBool(string @enum)
        {
            return @enum == "1";
        }

        internal static int BoolToInteger(bool @bool)
        {
            return @bool ? 1 : 0;
        }

        internal static string BoolToEnum(bool @bool)
        {
            return @bool ? "1" : "0";
        }

        internal static int GetRandomNumber(int min, int max)
        {
            return RandomNumber.Get(min, max);
        }

        internal static int GetUnixTimeStamp()
        {
            var totalSeconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            return (int) totalSeconds;
        }

        internal static DateTime UnixToDateTime(double unixTimeStamp)
        {
            var result = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            result = result.AddSeconds(unixTimeStamp).ToLocalTime();
            return result;
        }

        internal static DateTime UnixToDateTime(int unixTimeStamp)
        {
            var result = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            result = result.AddSeconds(unixTimeStamp).ToLocalTime();
            return result;
        }

        internal static int DateTimeToUnix(DateTime target)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt32((target - d).TotalSeconds);
        }

        internal static long Now()
        {
            var totalMilliseconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
            return (long) totalMilliseconds;
        }

        internal static int DifferenceInMilliSeconds(DateTime time, DateTime from)
        {
            return Convert.ToInt32(@from.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds - time.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
        }

        internal static bool IsValidAlphaNumeric(string inputStr)
        {
            inputStr = inputStr.ToLower();
            if (string.IsNullOrEmpty(inputStr)) return false;
            return inputStr.All(IsValid);
        }

        internal static bool IsNum(string @int)
        {
            double num;
            return double.TryParse(@int, out num);
        }

        internal static bool IsValid(char c)
        {
            return char.IsLetterOrDigit(c) || AllowedSpecialChars.Contains(c);
        }

        internal static string FilterFigure(string figure)
        {
            return figure.Any(character => !IsValid(character)) ? "lg-3023-1335.hr-828-45.sh-295-1332.hd-180-4.ea-3168-89.ca-1813-62.ch-235-1332" : figure;
        }

        public static bool ContainsAny(this string haystack, params string[] needles)
        {
            return needles.Any(haystack.Contains);
        }
			
        internal static string TimeSpanToString(TimeSpan span)
        {
            return string.Concat(span.Seconds, " s, ", span.Milliseconds, " ms");
        }
    }
}
