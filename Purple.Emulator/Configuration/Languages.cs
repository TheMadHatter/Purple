﻿#region

using System.Collections.Generic;
using System.Data;

#endregion

namespace AzureSharp.Configuration
{
    /// <summary>
    /// Class TextManager
    /// </summary>
    internal class TextManager
    {
        /// <summary>
        /// The texts
        /// </summary>
        static readonly Dictionary<string, string> Texts = new Dictionary<string, string>();

        /// <summary>
        /// Gets the text
        /// </summary>
        public static string GetText(string var)
        {
            string output;

            if (Texts != null && Texts.ContainsKey(var)) output = Texts[var];
            else output = var;

            return output;
        }

        /// <summary>
        /// Clear the cache.
        /// </summary>
        public static void ClearText()
        {
            Texts.Clear();
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TextManager"/> class.
        /// </summary>
        /// <param name="language">The TextManager</param>
        public static void Load()
        {
            if (Texts.Count > 0)
            {
                ClearText();
            }
            using (var client = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                client.SetQuery("SELECT name, text FROM server_text ORDER BY name ASC;");
                var table = client.GetTable();
                if (table != null)
                {
                    foreach (DataRow dataRow in table.Rows)
                    {
                        Texts.Add(dataRow["name"].ToString(), dataRow["text"].ToString());
                    }
                }
            }
        }
    }
}