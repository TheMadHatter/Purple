﻿#region

using System;
using System.Collections.Generic;
using System.IO;

#endregion

namespace AzureSharp.Configuration
{
    internal static class ConfigurationData
    {
        private static Dictionary<string, string> Data = new Dictionary<string, string>();

        public static string GetString(string name, string @default = "")
        {
            if (Data.ContainsKey(name)) return Data[name];
            Logger.Warn($"Could not find the configuration setting {name}. Using the default value: {@default}");
            return @default;
        }

        public static int GetInt(string name, int @default = 0)
        {
            if (Data.ContainsKey(name))
            {
                try
                {
                    return int.Parse(Data[name]);
                }
                catch
                {
                    Logger.Warn($"Invalid int value for the configuration setting {name}. Using the default value: {@default}");
                }
            }
            else
            {
                Logger.Warn($"Could not find the configuration settings {name}. Using the default value: {@default}");
            }
            return @default;
        }

        public static bool GetBool(string name, bool @default = false)
        {
            if (Data.ContainsKey(name))
            {
                try
                {
                    return bool.Parse(Data[name]);
                }
                catch
                {
                    Logger.Warn($"Invalid bool value for the configuration setting {name}. Using the default value: {@default}");
                }
            }
            else
            {
                Logger.Warn($"Could not find the configuration settings {name}. Using the default value: {@default}");
            }
            return @default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationData"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="mayNotExist">if set to <c>true</c> [may not exist].</param>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        internal static void Load(string filePath, bool mayNotExist = false)
        {
            if (!File.Exists(filePath))
            {
                if (!mayNotExist) throw new ArgumentException($"Configuration file are not found:'{filePath}'.");
            }
            else
            {
                try
                {
                    using (var streamReader = new StreamReader(filePath))
                    {
                        string text;
                        while ((text = streamReader.ReadLine()) != null)
                        {
                            if (text.Length < 1 || text.StartsWith("#")) continue;
                            var num = text.IndexOf('=');
                            if (num == -1) continue;
                            var key = text.Substring(0, num);
                            var value = text.Substring(num + 1);

                            Data.Add(key, value);
                        }
                        streamReader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new ArgumentException($"Could not process configuration file: {ex.Message}");
                }
            }
        }
    }
}