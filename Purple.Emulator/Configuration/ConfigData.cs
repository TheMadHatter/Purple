#region

using System.Collections.Generic;
using System.Data;
using Purple.Database.Queries;

#endregion

namespace AzureSharp.Configuration
{
    internal class ConfigData
    {
        internal Dictionary<string, string> DbData;
        internal ConfigData(Query dbClient)
        {
            try
            {
                DbData = new Dictionary<string, string>();
                DbData.Clear();
                dbClient.SetQuery("SELECT * FROM server_settings");
                var table = dbClient.GetTable();
                foreach (DataRow dataRow in table.Rows) DbData.Add(dataRow[0].ToString(), dataRow[1].ToString());
            }
            catch
            {
            }
        }
    }
}