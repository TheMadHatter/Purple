#region

using System;
using System.Data;
using System.Net.Sockets;
using System.Text;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.Connection.Net
{
    internal class MusConnection
    {
        private readonly Socket Conn;
        private readonly byte[] dataBuffering = new byte[1024]; // 1024

        internal MusConnection(Socket Conn)
        {
            this.Conn = Conn;
            Conn.BeginReceive(dataBuffering, 0, dataBuffering.Length, SocketFlags.None, RecieveData, null);
        }

        internal void RecieveData(IAsyncResult iAr)
        {
            try
            {
                var bytes = 0;
                try
                {
                    bytes = Conn.EndReceive(iAr);
                }
                catch
                {
                    mDisconnect();
                    return;
                }

                var data = Encoding.Default.GetString(dataBuffering, 0, bytes);

                if (data.Length > 0) dArrival(data);
            }
            catch
            {
            }
            mDisconnect();
        }

        private void dArrival(string Data)
        {
            try
            {
                var Params = Data.Split(Convert.ToChar(1));
                var header = PurpleEmulator.FilterInjectionChars(Params[0]);
                var param = PurpleEmulator.FilterInjectionChars(Params[1]);

                GameClient clientByUserId;
                uint userId;
                switch (header)
                {
                    case "sa":
                        var Message = param;

                        var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatMessageComposer"));
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendString("Server: " + Message);
                        serverMessage.AppendInteger(0);
                        PurpleEmulator.GetGame().GetClientManager().SendStaffMessageRCON(serverMessage);
                        break;

                    case "ha":
                        var HotelAlert = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                        HotelAlert.AppendString($"{param}\r\n- {"Hotel Management"}");
                        PurpleEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(HotelAlert);
                        break;

                    case "alert":
                        var pUserId = param.Split(' ')[0];
                        var pMessage = param.Split(' ')[1];

                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(uint.Parse(pUserId));
                        if (clientByUserId == null) return;
                        clientByUserId.SendNotif(pMessage);
                        break;

                    case "kill":
                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(uint.Parse(param));
                        if (clientByUserId != null) clientByUserId.Disconnect("MUS Disconnection");
                        break;

                    case "updatediamonds":
                        var UserId = uint.Parse(param);
                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(UserId);

                        if (clientByUserId == null) return;

                        int diamonds;
                        using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                        {
                            DataRow row;
                            dbClient.SetQuery("SELECT diamonds FROM users WHERE id = " + UserId);
                            row = dbClient.GetRow();
                            if (row == null) return;

                            diamonds = Convert.ToInt32(row["diamonds"]);
                        }
                        clientByUserId.GetHabbo().Diamonds = diamonds;
                        clientByUserId.GetHabbo().UpdateActivityPointsBalance();
                        break;

                    case "updatemotto":
                        var UserID = uint.Parse(param);
                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(UserID);

                        if (clientByUserId == null) return;

                        string motto;
                        using (var conn = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                        {
                            conn.SetQuery("SELECT motto FROM users WHERE id = " + UserID);
                            motto = conn.GetString();
                        }

                        clientByUserId.GetHabbo().Motto = motto;
                        if (clientByUserId.GetHabbo().InRoom)
                        {
                            var room = clientByUserId.GetHabbo().CurrentRoom;
                            if (room == null) return;

                            var user = room.GetRoomUserManager().GetRoomUserByHabbo(clientByUserId.GetHabbo().Id);
                            if (user == null) return;

                            var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                            message.AppendInteger(user.VirtualId);
                            message.AppendString(clientByUserId.GetHabbo().Look);
                            message.AppendString(clientByUserId.GetHabbo().Gender.ToLower());
                            message.AppendString(motto);
                            message.AppendInteger(clientByUserId.GetHabbo().AchievementPoints);
                            clientByUserId.SendMessage(message);
                        }
                        break;

                    case "addtoinventory":
                        userId = Convert.ToUInt32(param[0]);
                        var furniId = Convert.ToInt32(param[1]);

                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId == null || clientByUserId.GetHabbo() == null || clientByUserId.GetHabbo().GetInventoryComponent() == null) return;

                        clientByUserId.GetHabbo().GetInventoryComponent().UpdateItems(true);
                        clientByUserId.GetHabbo().GetInventoryComponent().SendNewItems((uint) furniId);

                        break;

                    case "updatecredits":
                        userId = Convert.ToUInt32(param[0]);
                        var credits = Convert.ToInt32(param[1]);

                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId != null && clientByUserId.GetHabbo() != null)
                        {
                            clientByUserId.GetHabbo().Credits = credits;
                            clientByUserId.GetHabbo().UpdateCreditsBalance();
                        }
                        return;

                    case "updatesubscription":
                        userId = Convert.ToUInt32(param[0]);

                        clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId == null || clientByUserId.GetHabbo() == null) return;
                        clientByUserId.GetHabbo().GetSubscriptionManager().ReloadSubscription();
                        clientByUserId.GetHabbo().SerializeClub();
                        break;

                    case "update_bans":
                        using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) PurpleEmulator.GetGame().GetBanManager().LoadBans(dbClient);
                        PurpleEmulator.GetGame().GetClientManager().CheckForBanConflicts();
                        break;
                }
            }
            catch
            {
            }
            finally
            {
                mDisconnect();
            }
        }

        private void mDisconnect()
        {
            try
            {
                Conn.Close();
            }
            catch
            {
            }
        }
    }
}