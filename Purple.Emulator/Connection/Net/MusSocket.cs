#region

using System;
using System.Net;
using System.Net.Sockets;

#endregion

namespace AzureSharp.Connection.Net
{
    class MusSocket
    {

        private static Socket Handler;
        private static int _Port;
        private static string _MusHost;

        internal MusSocket(int Port, string musHost)
        {
            _Port = Port;
            _MusHost = musHost;
            Handler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


            Logger.Info("Starting the MusSocket on port " + _Port);
            try
            {
                Handler.Bind(new IPEndPoint(IPAddress.Any, Port));
                Handler.Listen(1000);
                Handler.BeginAccept(ConnRequest, Handler);
            }
            catch (Exception e)
            {
                Logger.Error("Failed to start the MusSocket", e);
            }
        }

        private static void ConnRequest(IAsyncResult iAr)
        {
            try
            {
                var nSocket = ((Socket) iAr.AsyncState).EndAccept(iAr);
                //if (nSocket.RemoteEndPoint.ToString().Split(':')[0] != _MusHost) // Don't allow remote IP!
                //var ip = nSocket.RemoteEndPoint.ToString().Split(':')[0];
                var nConnection = new MusConnection(nSocket);
            }
            catch (Exception e)
            {
                Logger.Error("Exception during MUS connection.", e);
            }

            Handler.BeginAccept(ConnRequest, Handler);
        }
    }
}