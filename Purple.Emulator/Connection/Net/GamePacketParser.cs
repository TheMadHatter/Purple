#region

using System;
using AzureSharp.Connection.Connection;
using AzureSharp.HabboHotel.GameClients;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.Connection.Net
{
    /// <summary>
    /// Class GamePacketParser.
    /// </summary>
    public class GamePacketParser : IDataParser
    {
        /// <summary>
        /// The logger.

        /// <summary>
        /// Delegate HandlePacket
        /// </summary>
        /// <param name="message">The message.</param>
        public delegate void HandlePacket(ClientMessage message);

        /// <summary>
        /// The _current client
        /// </summary>
        private readonly GameClient _currentClient;

        /// <summary>
        /// The _con
        /// </summary>
        private ConnectionInformation _con;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePacketParser"/> class.
        /// </summary>
        /// <param name="me">Me.</param>
        internal GamePacketParser(GameClient me)
        {
            _currentClient = me;
        }

        /// <summary>
        /// Handles the packet data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void HandlePacketData(byte[] data)
        {
            if (data.Length == 0 || _currentClient == null) return;

            var pos = 0;
            short messageId = 0;

            try
            {
                for (pos = 0; pos < data.Length;)
                {
                    var length = HabboEncoding.DecodeInt32(new[] {data[pos++], data[pos++], data[pos++], data[pos++]});

                    if (length < 2 || length > 4096) return; //broken packet! might be better to disconnect EVERYTHING

                    messageId = HabboEncoding.DecodeInt16(new[] {data[pos++], data[pos++]});

                    var packetContent = new byte[length - 2];

                    for (var i = 0; i < packetContent.Length && pos < data.Length; i++) packetContent[i] = data[pos++];

                    using (var clientMessage = new ClientMessage(messageId, packetContent))
                    {
                        _currentClient.GetMessageHandler().HandleRequest(clientMessage);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error($"packet handling ----> {messageId}", exception);

                _con.Dispose();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //todo: mem checking
            //_currentClient = null;
            //_con = null;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new GamePacketParser(_currentClient);
        }

        /// <summary>
        /// Sets the connection.
        /// </summary>
        /// <param name="con">The con.</param>
        public void SetConnection(ConnectionInformation con)
        {
            _con = con;
        }
    }
}