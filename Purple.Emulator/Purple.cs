﻿#region

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using AzureSharp.Configuration;
using AzureSharp.Connection.Net;
using Purple.Database;
using Purple.Encryption;
using AzureSharp.HabboHotel;
using AzureSharp.HabboHotel.Groups;
using AzureSharp.HabboHotel.Misc;
using AzureSharp.HabboHotel.Pets;
using AzureSharp.HabboHotel.Users;
using AzureSharp.HabboHotel.Users.Messenger;
using AzureSharp.HabboHotel.Users.UserDataManagement;
using AzureSharp.Messages;
using AzureSharp.Messages.Parsers;
using System.Xml.XmlConfiguration;
using MySql.Data.MySqlClient;
using Timer = System.Timers.Timer;

#endregion

namespace AzureSharp
{
	internal static class PurpleEmulator
    {

        internal static string ServerLanguage = "english";
        internal static string Version => typeof(PurpleEmulator).Assembly.GetName().Version.ToString(4);
        internal static int LiveCurrencyType = 105, ConsoleTimer = 2000;
        internal static bool IsLive, DebugMode, ConsoleTimerOn;
        internal static uint StaffAlertMinRank = 4, FriendRequestLimit = 1000;
        internal static Dictionary<uint, uint> MutedUsersByFilter;
        internal static DatabaseManager Manager;
        internal static ConfigData ConfigData;
        internal static DateTime ServerStarted;
        internal static Dictionary<uint, List<OfflineMessage>> OfflineMessages;
        internal static Timer Timer;
        internal static CultureInfo CultureInfo;
        public static readonly ConcurrentDictionary<uint, Habbo> UsersCached = new ConcurrentDictionary<uint, Habbo>();
        private static ConnectionHandling connectionManager;
        private static Encoding defaultEncoding;
        private static Game game;
        internal static bool ShutdownStarted { get; set; }

        internal static Habbo GetHabboById(uint userId)
        {
            try
            {
                var clientByUserId = GetGame().GetClientManager().GetClientByUserId(userId);
                if (clientByUserId != null)
                {
                    var habbo = clientByUserId.GetHabbo();
                    if (habbo != null && habbo.Id > 0)
                    {
                        UsersCached.AddOrUpdate(userId, habbo, (key, value) => habbo);
                        return habbo;
                    }
                }
                else
                {
                    var userData = UserDataFactory.GetUserData((int) userId);
                    if (UsersCached.ContainsKey(userId)) return UsersCached[userId];

                    if (userData == null || userData.User == null) return null;

                    UsersCached.TryAdd(userId, userData.User);
                    userData.User.InitInformation(userData);
                    return userData.User;
                }
            }
            catch (Exception e)
            {
                Logger.Error("Habbo GetHabboForId", e);
            }
            return null;
        }

        internal static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Console.Clear();
            Logger.Info($"Console Cleared in: {DateTime.Now} Next Time on: {ConsoleTimer} Seconds ");
            GC.Collect();
            Timer.Start();
        }

		public static void Main(string[] args)
		{
			Console.WriteLine ("  _____                  _                                       |\n |  __ \\   BETA 1.001   | |                                      |\n | |__) |   _ _ __ _ __ | | ___                                  |\n |  ___/ | | | '__| '_ \\| |/ _ \\         By The Mad Hatter       |\n | |   | |_| | |  | |_) | |  __/                                 |\n |_|    \\__,_|_|  | .__/|_|\\___|        Based on AzureSharp      |\n ________________ | | _____________/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\_/\n                  |_|\n");

            #region Precheck

            ServerStarted = DateTime.Now;
            defaultEncoding = Encoding.Default;
            MutedUsersByFilter = new Dictionary<uint, uint>();
            ChatEmotions.Initialize();

            #endregion Precheck

            #region Database Connection

            CultureInfo = CultureInfo.CreateSpecificCulture("en-GB");
            try
            {
                ConfigurationData.Load(Path.Combine(Application.StartupPath, "Settings/main.ini"), false);
                ConfigurationData.Load(Path.Combine(Application.StartupPath, "Settings/Welcome/settings.ini"), true);

                var mySqlConnectionStringBuilder = new MySqlConnectionStringBuilder {Server = ConfigurationData.GetString("db.hostname", "localhost"), Port = (uint)ConfigurationData.GetInt("db.port", 3306), UserID = ConfigurationData.GetString("db.username", "root"), Password = ConfigurationData.GetString("db.password", ""), Database = ConfigurationData.GetString("db.name", "azure"), MinimumPoolSize = (uint) ConfigurationData.GetInt("db.pool.minsize", 1), MaximumPoolSize = (uint)ConfigurationData.GetInt("db.pool.maxsize", 500), Pooling = true, AllowZeroDateTime = true, ConvertZeroDateTime = true, DefaultCommandTimeout = 300u, ConnectionTimeout = 10u};
                var mySqlConnectionStringBuilder2 = mySqlConnectionStringBuilder;
                Manager = new DatabaseManager(mySqlConnectionStringBuilder2.ToString());
                using (var queryReactor = GetDatabaseManager().GetQueryReactor())
                {
                    ConfigData = new ConfigData(queryReactor);
                    PetCommandHandler.Init(queryReactor);
                    PetLocale.Init(queryReactor);
                    OfflineMessages = new Dictionary<uint, List<OfflineMessage>>();
                    OfflineMessage.InitOfflineMessages(queryReactor);
                }

                #endregion Database Connection

                #region Packets Registering

                ConsoleTimer = ConfigurationData.GetInt("console.clear.time");
                ConsoleTimerOn = ConfigurationData.GetBool("console.clear.enabled");
                FriendRequestLimit = (uint) ConfigurationData.GetInt("client.maxrequests");


                LibraryParser.Incoming = new Dictionary<int, LibraryParser.StaticRequestHandler>();
                LibraryParser.Library = new Dictionary<string, string>();
                LibraryParser.Outgoing = new Dictionary<string, int>();
                LibraryParser.Config = new Dictionary<string, string>();

                LibraryParser.RegisterLibrary();
                LibraryParser.RegisterOutgoing();
                LibraryParser.RegisterIncoming();
                LibraryParser.RegisterConfig();

                #endregion Packets Registering

                #region Game Initalizer

                ExtraSettings.RunExtraSettings();
                FurniDataParser.SetCache();
                CrossDomainPolicy.Set();
                game = new Game();
                game.GetNavigator().LoadNewPublicRooms();
                game.ContinueLoading();
                FurniDataParser.Clear();

                #endregion Game Initalizer

                #region Text Parser

                TextManager.Load();
                Logger.Info("Loaded Text Manager succesfully");

                #endregion text Parser

                #region Environment SetUp

                if (ConsoleTimerOn) Logger.Info("Console Clear Timer is Enabled, with " + ConsoleTimer + " Seconds.");

                connectionManager = new ConnectionHandling(ConfigurationData.GetInt("game.tcp.port", 30000), ConfigurationData.GetInt("game.tcp.poolsize", 100), ConfigurationData.GetInt("game.tcp.conperip", 5), ConfigurationData.GetBool("game.tcp.antiddos"), ConfigurationData.GetBool("game.tcp.enablenagles"));

                if (LibraryParser.Config["Crypto.Enabled"] == "true")
                {
                    Handler.Initialize(LibraryParser.Config["Crypto.RSA.N"], LibraryParser.Config["Crypto.RSA.D"], LibraryParser.Config["Crypto.RSA.E"]);

                    Logger.Info("Started RSA crypto service");
                }
                else
                {
                    Logger.Info("The encryption system is disabled. This affects badly to the safety.");
                }

                Logger.Info("Starting the ConnectionManager on port " + ConfigurationData.GetInt("game.tcp.port", 30000));

                connectionManager.Init();
                connectionManager.Start();

                new MusSocket(ConfigurationData.GetInt("mus.tcp.port"), ConfigurationData.GetString("mus.tcp.bindip"));

                LibraryParser.Initialize();

                #endregion Environment SetUp

                #region Tasks

                if (ConsoleTimerOn)
                {
                    Timer = new Timer {Interval = ConsoleTimer};
                    Timer.Elapsed += TimerElapsed;
                    Timer.Start();
                }

                StaffAlertMinRank = (uint)ConfigurationData.GetInt("game.sa_min_rank", 4);

                if (ConfigurationData.GetBool("system.debug")) DebugMode = true;

                Logger.Info("Purple Emulator ready.");
                Logger.Info("To close the emulator. Press Ctrl + C");
                IsLive = true;
            }
            catch (Exception e)
            {
				Logger.Fatal("Failed to load Purple Emulator.", e);
                Logger.Info("For more information, check the logs.");
                Logger.Info("Press a key to close...");
                Console.ReadKey();
                Environment.Exit(1);
            }

            #endregion Tasks and MusSystem
        }
			
        public static string GetGroupDateJoinString(long timeStamp)
        {
            var time = Utility.UnixToDateTime(timeStamp).ToString("MMMM/dd/yyyy", CultureInfo).Split('/');
            return $"{time[0].Substring(0, 3)} {time[1]}, {time[2]}";
        }

        internal static Habbo GetHabboForName(string userName)
        {
            try
            {
                using (var queryReactor = GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("SELECT id FROM users WHERE username = @user");
                    queryReactor.AddParameter("user", userName);
                    var integer = queryReactor.GetInteger();
                    if (integer > 0)
                    {
                        var result = GetHabboById((uint) integer);
                        return result;
                    }
                }
            }
            catch
            {
            }
            return null;
        }

        internal static ConfigData GetDbConfig()
        {
            return ConfigData;
        }
			
        internal static Encoding GetDefaultEncoding()
        {
            return defaultEncoding;
        }

        internal static ConnectionHandling GetConnectionManager()
        {
            return connectionManager;
        }

        internal static Game GetGame()
        {
            return game;
        }
			
        internal static string FilterInjectionChars(string input)
        {
            input = input.Replace('\u0001', ' ');
            input = input.Replace('\u0002', ' ');
            input = input.Replace('\u0003', ' ');
            input = input.Replace('\t', ' ');
            return input;
        }

        internal static DatabaseManager GetDatabaseManager()
        {
            return Manager;
        }

        internal static void PerformShutDown()
        {
            PerformShutDown(false);
        }

        internal static void PerformRestart()
        {
            PerformShutDown(true);
        }
			
        internal static void PerformShutDown(bool restart)
        {
            var now = DateTime.Now;
            GetGame().GetCacheManager().Dispose();

            ShutdownStarted = true;

            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString("disconnection");
            serverMessage.AppendInteger(2);
            serverMessage.AppendString("title");
            serverMessage.AppendString("HEY EVERYONE!");
            serverMessage.AppendString("message");
            serverMessage.AppendString(restart ? "<b>The hotel is shutting down for a break.<)/b>\nYou may come back later.\r\n<b>So long!</b>" : "<b>The hotel is shutting down for a break.</b><br />You may come back soon. Don't worry, everything's going to be saved..<br /><b>So long!</b>\r\n~ This session was powered by PurpleEmulator");
            GetGame().GetClientManager().QueueBroadcaseMessage(serverMessage);

            game.StopGameLoop();
            game.GetRoomManager().RemoveAllRooms();
            GetGame().GetClientManager().CloseAll();

            GetConnectionManager().Destroy();

            foreach (Guild group in game.GetGroupManager().Groups.Values) group.UpdateForum();

            using (var queryReactor = Manager.GetQueryReactor())
            {
                queryReactor.RunFastQuery("UPDATE users SET online = '0'");
                queryReactor.RunFastQuery("UPDATE rooms_data SET users_now = 0");
                queryReactor.RunFastQuery("TRUNCATE TABLE users_rooms_visits");
            }

            connectionManager.Destroy();
            game.Destroy();

            try
            {
                Manager.Destroy();
                Logger.Info("Game Manager destroyed");
            }
            catch (Exception e)
            {
				Logger.Error("Purple.cs PerformShutDown GameManager", e);
            }

            var span = DateTime.Now - now;
            Logger.Info("Elapsed " + Utility.TimeSpanToString(span) + "ms on Shutdown Proccess");

            if (!restart) Logger.Info("Goodbye!");
            else Logger.Info("Restarting...");

            IsLive = false;

            if (restart) Process.Start(Assembly.GetEntryAssembly().Location);
            Environment.Exit(0);
        }
    }
}