﻿#region

using System.Collections.Generic;
using System.Linq;
using AzureSharp.Configuration;
using AzureSharp.HabboHotel.Catalogs;
using AzureSharp.HabboHotel.Groups;
using AzureSharp.HabboHotel.Groups.Structs;
using AzureSharp.HabboHotel.Rooms;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.Messages.Handlers
{
    /// <summary>
    /// Class GameClientMessageHandler.
    /// </summary>
    internal partial class GameClientMessageHandler
    {
        /// <summary>
        /// Serializes the group purchase page.
        /// </summary>
        internal void SerializeGroupPurchasePage()
        {
            var list = new HashSet<RoomData>(Session.GetHabbo().UsersRooms.Where(x => x.Group == null));

            Response.Init(LibraryParser.OutgoingRequest("GroupPurchasePageMessageComposer"));
            Response.AppendInteger(10);
            Response.AppendInteger(list.Count);

            foreach (var current2 in list) NewMethod(current2);

            Response.AppendInteger(5);
            Response.AppendInteger(10);
            Response.AppendInteger(3);
            Response.AppendInteger(4);
            Response.AppendInteger(25);
            Response.AppendInteger(17);
            Response.AppendInteger(5);
            Response.AppendInteger(25);
            Response.AppendInteger(17);
            Response.AppendInteger(3);
            Response.AppendInteger(29);
            Response.AppendInteger(11);
            Response.AppendInteger(4);
            Response.AppendInteger(0);
            Response.AppendInteger(0);
            Response.AppendInteger(0);
            SendResponse();
        }

        /// <summary>
        /// Serializes the group purchase parts.
        /// </summary>
        internal void SerializeGroupPurchaseParts()
        {
            Response.Init(LibraryParser.OutgoingRequest("GroupPurchasePartsMessageComposer"));
            Response.AppendInteger(PurpleEmulator.GetGame().GetGroupManager().Bases.Count);
            foreach (var current in PurpleEmulator.GetGame().GetGroupManager().Bases)
            {
                Response.AppendInteger(current.Id);
                Response.AppendString(current.Value1);
                Response.AppendString(current.Value2);
            }
            Response.AppendInteger(PurpleEmulator.GetGame().GetGroupManager().Symbols.Count);
            foreach (var current2 in PurpleEmulator.GetGame().GetGroupManager().Symbols)
            {
                Response.AppendInteger(current2.Id);
                Response.AppendString(current2.Value1);
                Response.AppendString(current2.Value2);
            }
            Response.AppendInteger(PurpleEmulator.GetGame().GetGroupManager().BaseColours.Count);
            foreach (var current3 in PurpleEmulator.GetGame().GetGroupManager().BaseColours)
            {
                Response.AppendInteger(current3.Id);
                Response.AppendString(current3.Colour);
            }
            Response.AppendInteger(PurpleEmulator.GetGame().GetGroupManager().SymbolColours.Count);
            foreach (GroupSymbolColours current4 in PurpleEmulator.GetGame().GetGroupManager().SymbolColours.Values)
            {
                Response.AppendInteger(current4.Id);
                Response.AppendString(current4.Colour);
            }
            Response.AppendInteger(PurpleEmulator.GetGame().GetGroupManager().BackGroundColours.Count);
            foreach (GroupBackGroundColours current5 in PurpleEmulator.GetGame().GetGroupManager().BackGroundColours.Values)
            {
                Response.AppendInteger(current5.Id);
                Response.AppendString(current5.Colour);
            }
            SendResponse();
        }

        /// <summary>
        /// Purchases the group.
        /// </summary>
        internal void PurchaseGroup()
        {
            if (Session == null || Session.GetHabbo().Credits < 10) return;

            var gStates = new List<int>();
            var name = Request.GetString();
            var description = Request.GetString();
            var roomid = Request.GetUInteger();
            var color = Request.GetInteger();
            var num3 = Request.GetInteger();
            var unused = Request.GetInteger();
            var guildBase = Request.GetInteger();
            var guildBaseColor = Request.GetInteger();
            var num6 = Request.GetInteger();
            var roomData = PurpleEmulator.GetGame().GetRoomManager().GenerateRoomData(roomid);
            if (roomData.Owner != Session.GetHabbo().UserName) return;

            for (var i = 0; i < num6 * 3; i++)
            {
                var item = Request.GetInteger();
                gStates.Add(item);
            }

            var image = PurpleEmulator.GetGame().GetGroupManager().GenerateGuildImage(guildBase, guildBaseColor, gStates);
            Guild group;
            PurpleEmulator.GetGame().GetGroupManager().CreateGroup(name, description, roomid, image, Session, !PurpleEmulator.GetGame().GetGroupManager().SymbolColours.Contains(color) ? 1 : color, !PurpleEmulator.GetGame().GetGroupManager().BackGroundColours.Contains(num3) ? 1 : num3, out group);

            Session.SendMessage(CatalogPacket.PurchaseOk(0u, "CREATE_GUILD", 10));
            Response.Init(LibraryParser.OutgoingRequest("GroupRoomMessageComposer"));
            Response.AppendInteger(roomid);
            Response.AppendInteger(group.Id);
            SendResponse();
            roomData.Group = group;
            roomData.GroupId = group.Id;
            roomData.SerializeRoomData(Response, Session, true, false);

            if (!Session.GetHabbo().InRoom || Session.GetHabbo().CurrentRoom.RoomId != roomData.Id)
            {
                Session.GetMessageHandler().PrepareRoomForUser(roomData.Id, roomData.PassWord);
                Session.GetHabbo().CurrentRoomId = roomData.Id;
            }

            if (Session.GetHabbo().CurrentRoom != null && !Session.GetHabbo().CurrentRoom.LoadedGroups.ContainsKey(group.Id)) Session.GetHabbo().CurrentRoom.LoadedGroups.Add(group.Id, group.Badge);
            if (CurrentLoadingRoom != null && !CurrentLoadingRoom.LoadedGroups.ContainsKey(group.Id)) CurrentLoadingRoom.LoadedGroups.Add(group.Id, group.Badge);

            if (CurrentLoadingRoom != null)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RoomGroupMessageComposer"));
                serverMessage.AppendInteger(CurrentLoadingRoom.LoadedGroups.Count);

                foreach (var current in CurrentLoadingRoom.LoadedGroups)
                {
                    serverMessage.AppendInteger(current.Key);
                    serverMessage.AppendString(current.Value);
                }
                CurrentLoadingRoom.SendMessage(serverMessage);
            }

            if (CurrentLoadingRoom == null || Session.GetHabbo().FavouriteGroup != @group.Id) return;
            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("ChangeFavouriteGroupMessageComposer"));
            serverMessage2.AppendInteger(CurrentLoadingRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id).VirtualId);
            serverMessage2.AppendInteger(@group.Id);
            serverMessage2.AppendInteger(3);
            serverMessage2.AppendString(@group.Name);
            CurrentLoadingRoom.SendMessage(serverMessage2);
        }

        /// <summary>
        /// Serializes the group information.
        /// </summary>
        internal void SerializeGroupInfo()
        {
            var groupId = Request.GetInteger();
            var newWindow = Request.GetBool();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);

            if (group == null) return;

            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, newWindow);
        }

        /// <summary>
        /// Serializes the group members.
        /// </summary>
        internal void SerializeGroupMembers()
        {
            var groupId = Request.GetInteger();
            var page = Request.GetInteger();
            var searchVal = Request.GetString();
            var reqType = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, reqType, Session, searchVal, page);
            SendResponse();
        }

        /// <summary>
        /// Makes the group admin.
        /// </summary>
        internal void MakeGroupAdmin()
        {
            var num = Request.GetInteger();
            var num2 = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(num);

            if (Session.GetHabbo().Id != group.CreatorId || !group.Members.ContainsKey(num2) || group.Admins.ContainsKey(num2)) return;

            group.Members[num2].Rank = 1;
            group.Admins.Add(num2, group.Members[num2]);
            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, 1u, Session, "", 0);
            SendResponse();
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId);

            if (room != null)
            {
                var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(PurpleEmulator.GetHabboById(num2).UserName);
                if (roomUserByHabbo != null)
                {
                    if (!roomUserByHabbo.Statusses.ContainsKey("flatctrl 1")) roomUserByHabbo.AddStatus("flatctrl 1", "");
                    Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                    Response.AppendInteger(1);
                    roomUserByHabbo.GetClient().SendMessage(GetResponse());
                    roomUserByHabbo.UpdateNeeded = true;
                }
            }
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat("UPDATE groups_members SET rank='1' WHERE group_id=", num, " AND user_id=", num2, " LIMIT 1;"));
            }
        }

        /// <summary>
        /// Removes the group admin.
        /// </summary>
        internal void RemoveGroupAdmin()
        {
            var num = Request.GetInteger();
            var num2 = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(num);
            if (Session.GetHabbo().Id != group.CreatorId || !group.Members.ContainsKey(num2) || !group.Admins.ContainsKey(num2)) return;
            group.Members[num2].Rank = 0;
            group.Admins.Remove(num2);
            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, 0u, Session, "", 0);
            SendResponse();
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId);
            if (room != null)
            {
                var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(PurpleEmulator.GetHabboById(num2).UserName);
                if (roomUserByHabbo != null)
                {
                    if (roomUserByHabbo.Statusses.ContainsKey("flatctrl 1")) roomUserByHabbo.RemoveStatus("flatctrl 1");
                    Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                    Response.AppendInteger(0);
                    roomUserByHabbo.GetClient().SendMessage(GetResponse());
                    roomUserByHabbo.UpdateNeeded = true;
                }
            }
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat("UPDATE groups_members SET rank='0' WHERE group_id=", num, " AND user_id=", num2, " LIMIT 1;"));
            }
        }

        /// <summary>
        /// Accepts the membership.
        /// </summary>
        internal void AcceptMembership()
        {
            var GroupId = Request.GetInteger();
            var UserId = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
            if (Session.GetHabbo().Id != group.CreatorId && !group.Admins.ContainsKey(Session.GetHabbo().Id) && !group.Requests.Contains(UserId)) return;
            if (group.Members.ContainsKey(UserId))
            {
                group.Requests.Remove(UserId);
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery($"DELETE FROM groups_requests WHERE group_id = '{GroupId}' AND user_id = '{UserId}' LIMIT 1");
                }
                return;
            }

            group.Requests.Remove(UserId);
            group.Members.Add(UserId, new GroupUser(UserId, GroupId, 0, Utility.GetUnixTimeStamp()));
            group.Admins.Add(UserId, group.Members[UserId]);

            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, 0u, Session, "", 0);
            SendResponse();

            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery($"DELETE FROM groups_requests WHERE group_id = '{GroupId}' AND user_id = '{UserId}' LIMIT 1");
            }
            using (var queryreactor2 = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor2.RunFastQuery($"INSERT INTO groups_members (group_id, user_id, rank, date_join) VALUES ('{GroupId}','{UserId}','0','{Utility.GetUnixTimeStamp()}')");
            }
        }

        /// <summary>
        /// Declines the membership.
        /// </summary>
        internal void DeclineMembership()
        {
            var groupId = Request.GetInteger();
            var userId = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);

            if (Session.GetHabbo().Id != group.CreatorId && !group.Admins.ContainsKey(Session.GetHabbo().Id) && !group.Requests.Contains(userId)) return;

            group.Requests.Remove(userId);

            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, 2u, Session, "", 0);
            SendResponse();
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId);
            if (room != null)
            {
                var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(PurpleEmulator.GetHabboById(userId).UserName);
                if (roomUserByHabbo != null)
                {
                    if (roomUserByHabbo.Statusses.ContainsKey("flatctrl 1")) roomUserByHabbo.RemoveStatus("flatctrl 1");
                    roomUserByHabbo.UpdateNeeded = true;
                }
            }
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery("DELETE FROM groups_requests WHERE group_id=" + groupId + " AND user_id=" + userId);
            }
        }

        /// <summary>
        /// Joins the group.
        /// </summary>
        internal void JoinGroup()
        {
            var GroupId = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);

            if (group.Members.ContainsKey(Session.GetHabbo().Id)) return;

            if (group.State == 0u)
            {
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Concat("INSERT INTO groups_members (user_id, group_id, date_join) VALUES (", Session.GetHabbo().Id, ",", GroupId, ",", Utility.GetUnixTimeStamp(), ")"));
                    queryReactor.RunFastQuery(string.Concat("UPDATE users_stats SET favourite_group=", GroupId, " WHERE id= ", Session.GetHabbo().Id, " LIMIT 1"));
                }
                group.Members.Add(Session.GetHabbo().Id, new GroupUser(Session.GetHabbo().Id, group.Id, 0, Utility.GetUnixTimeStamp()));
                Session.GetHabbo().UserGroups.Add(group.Members[Session.GetHabbo().Id]);
            }
            else
            {
                using (var queryreactor2 = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor2.RunFastQuery(string.Concat("INSERT INTO groups_requests (user_id, group_id) VALUES (", Session.GetHabbo().Id, ",", GroupId, ")"));
                }
                group.Requests.Add(Session.GetHabbo().Id);
            }

            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
        }

        /// <summary>
        /// Removes the member.
        /// </summary>
        internal void RemoveMember()
        {
            var num = Request.GetInteger();
            var num2 = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(num);
            if (num2 == Session.GetHabbo().Id)
            {
                if (group.Members.ContainsKey(num2)) group.Members.Remove(num2);
                if (group.Admins.ContainsKey(num2)) group.Admins.Remove(num2);
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Concat("DELETE FROM groups_members WHERE user_id=", num2, " AND group_id=", num, " LIMIT 1"));
                }
                PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
                if (Session.GetHabbo().FavouriteGroup == num)
                {
                    Session.GetHabbo().FavouriteGroup = 0;
                    using (var queryreactor2 = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryreactor2.RunFastQuery($"UPDATE users_stats SET favourite_group=0 WHERE id={num2} LIMIT 1");
                    Response.Init(LibraryParser.OutgoingRequest("FavouriteGroupMessageComposer"));
                    Response.AppendInteger(Session.GetHabbo().Id);
                    Session.GetHabbo().CurrentRoom.SendMessage(Response);
                    Response.Init(LibraryParser.OutgoingRequest("ChangeFavouriteGroupMessageComposer"));
                    Response.AppendInteger(0);
                    Response.AppendInteger(-1);
                    Response.AppendInteger(-1);
                    Response.AppendString("");
                    Session.GetHabbo().CurrentRoom.SendMessage(Response);
                    if (group.AdminOnlyDeco == 0u)
                    {
                        var roomUserByHabbo = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId).GetRoomUserManager().GetRoomUserByHabbo(PurpleEmulator.GetHabboById(num2).UserName);
                        if (roomUserByHabbo == null) return;
                        roomUserByHabbo.RemoveStatus("flatctrl 1");
                        Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                        Response.AppendInteger(0);
                        roomUserByHabbo.GetClient().SendMessage(GetResponse());
                    }
                }
                return;
            }
            if (Session.GetHabbo().Id != group.CreatorId || !group.Members.ContainsKey(num2)) return;
            group.Members.Remove(num2);
            if (group.Admins.ContainsKey(num2)) group.Admins.Remove(num2);
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
            Response.Init(LibraryParser.OutgoingRequest("GroupMembersMessageComposer"));
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupMembers(Response, group, 0u, Session, "", 0);
            SendResponse();
            using (var queryreactor3 = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor3.RunFastQuery(string.Concat("DELETE FROM groups_members WHERE group_id=", num, " AND user_id=", num2, " LIMIT 1;"));
            }
        }

        /// <summary>
        /// Makes the fav.
        /// </summary>
        internal void MakeFav()
        {
            var groupId = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            if (group == null) return;
            if (!group.Members.ContainsKey(Session.GetHabbo().Id)) return;
            Session.GetHabbo().FavouriteGroup = group.Id;
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, false);
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat("UPDATE users_stats SET favourite_group =", @group.Id, " WHERE id=", Session.GetHabbo().Id, " LIMIT 1;"));
            }
            Response.Init(LibraryParser.OutgoingRequest("FavouriteGroupMessageComposer"));
            Response.AppendInteger(Session.GetHabbo().Id);
            Session.SendMessage(Response);
            if (Session.GetHabbo().CurrentRoom != null)
            {
                if (!Session.GetHabbo().CurrentRoom.LoadedGroups.ContainsKey(group.Id))
                {
                    Session.GetHabbo().CurrentRoom.LoadedGroups.Add(group.Id, group.Badge);
                    Response.Init(LibraryParser.OutgoingRequest("RoomGroupMessageComposer"));
                    Response.AppendInteger(Session.GetHabbo().CurrentRoom.LoadedGroups.Count);
                    foreach (var current in Session.GetHabbo().CurrentRoom.LoadedGroups)
                    {
                        Response.AppendInteger(current.Key);
                        Response.AppendString(current.Value);
                    }
                    Session.GetHabbo().CurrentRoom.SendMessage(Response);
                }
            }
            Response.Init(LibraryParser.OutgoingRequest("ChangeFavouriteGroupMessageComposer"));
            Response.AppendInteger(0);
            Response.AppendInteger(group.Id);
            Response.AppendInteger(3);
            Response.AppendString(group.Name);
            Session.SendMessage(Response);
        }

        /// <summary>
        /// Removes the fav.
        /// </summary>
        internal void RemoveFav()
        {
            Request.GetUInteger();
            Session.GetHabbo().FavouriteGroup = 0;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryReactor.RunFastQuery($"UPDATE users_stats SET favourite_group=0 WHERE id={Session.GetHabbo().Id} LIMIT 1;");
            Response.Init(LibraryParser.OutgoingRequest("FavouriteGroupMessageComposer"));
            Response.AppendInteger(Session.GetHabbo().Id);
            Session.SendMessage(Response);
            Response.Init(LibraryParser.OutgoingRequest("ChangeFavouriteGroupMessageComposer"));
            Response.AppendInteger(0);
            Response.AppendInteger(-1);
            Response.AppendInteger(-1);
            Response.AppendString("");
            Session.SendMessage(Response);
        }

        /// <summary>
        /// Publishes the forum thread.
        /// </summary>
        internal void PublishForumThread()
        {
            if (Utility.GetUnixTimeStamp() - Session.GetHabbo().LastSqlQuery < 20) return;
            var groupId = Request.GetInteger();
            var threadId = Request.GetUInteger();
            var subject = Request.GetString();
            var content = Request.GetString();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            if (group == null || !group.HasForum) return;
            var timestamp = Utility.GetUnixTimeStamp();
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                if (threadId != 0)
                {
                    dbClient.SetQuery($"SELECT * FROM groups_forums_posts WHERE id = {threadId}");
                    var Row = dbClient.GetRow();
                    var Post = new GroupForumPost(Row);
                    if (Post.Locked || Post.Hidden)
                    {
                        Session.SendNotif(TextManager.GetText("forums_cancel"));
                        return;
                    }
                }
                Session.GetHabbo().LastSqlQuery = Utility.GetUnixTimeStamp();
                dbClient.SetQuery("INSERT INTO groups_forums_posts (group_id, parent_id, timestamp, poster_id, poster_name, poster_look, subject, post_content) VALUES (@gid, @pard, @ts, @pid, @pnm, @plk, @subjc, @content)");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("pard", threadId);
                dbClient.AddParameter("ts", timestamp);
                dbClient.AddParameter("pid", Session.GetHabbo().Id);
                dbClient.AddParameter("pnm", Session.GetHabbo().UserName);
                dbClient.AddParameter("plk", Session.GetHabbo().Look);
                dbClient.AddParameter("subjc", subject);
                dbClient.AddParameter("content", content);
                threadId = (uint) dbClient.GetInteger();
            }
            group.ForumScore += 0.25;
            group.ForumLastPosterName = Session.GetHabbo().UserName;
            group.ForumLastPosterId = Session.GetHabbo().Id;
            group.ForumLastPosterTimestamp = timestamp;
            group.ForumMessagesCount++;
            group.UpdateForum();
            if (threadId == 0)
            {
                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumNewThreadMessageComposer"));
                Message.AppendInteger(groupId);
                Message.AppendInteger(threadId);
                Message.AppendInteger(Session.GetHabbo().Id);
                Message.AppendString(subject);
                Message.AppendString(content);
                Message.AppendBool(false);
                Message.AppendBool(false);
                Message.AppendInteger(Utility.GetUnixTimeStamp() - timestamp);
                Message.AppendInteger(1);
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Message.AppendInteger(1);
                Message.AppendString("");
                Message.AppendInteger(Utility.GetUnixTimeStamp() - timestamp);
                Message.AppendByte(1);
                Message.AppendInteger(1);
                Message.AppendString("");
                Message.AppendInteger(42); //useless
                Session.SendMessage(Message);
            }
            else
            {
                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumNewResponseMessageComposer"));
                Message.AppendInteger(groupId);
                Message.AppendInteger(threadId);
                Message.AppendInteger(group.ForumMessagesCount);
                Message.AppendInteger(0);
                Message.AppendInteger(Session.GetHabbo().Id);
                Message.AppendString(Session.GetHabbo().UserName);
                Message.AppendString(Session.GetHabbo().Look);
                Message.AppendInteger(Utility.GetUnixTimeStamp() - timestamp);
                Message.AppendString(content);
                Message.AppendByte(0);
                Message.AppendInteger(0);
                Message.AppendString("");
                Message.AppendInteger(0);
                Session.SendMessage(Message);
            }
        }

        /// <summary>
        /// Updates the state of the thread.
        /// </summary>
        internal void UpdateThreadState()
        {
            var GroupId = Request.GetInteger();
            var ThreadId = Request.GetUInteger();
            var Pin = Request.GetBool();
            var Lock = Request.GetBool();
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery($"SELECT * FROM groups_forums_posts WHERE group_id = '{GroupId}' AND id = '{ThreadId}' LIMIT 1;");
                var Row = dbClient.GetRow();
                var Group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
                if (Row != null)
                {
                    if ((uint) Row["poster_id"] == Session.GetHabbo().Id || Group.Admins.ContainsKey(Session.GetHabbo().Id))
                    {
                        dbClient.SetQuery($"UPDATE groups_forums_posts SET pinned = @pin , locked = @lock WHERE id = {ThreadId};");
                        dbClient.AddParameter("pin", Pin ? "1" : "0");
                        dbClient.AddParameter("lock", Lock ? "1" : "0");
                        dbClient.RunQuery();
                    }
                }

                var Thread = new GroupForumPost(Row);
                if (Thread.Pinned != Pin)
                {
                    var Notif = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                    Notif.AppendString(Pin ? "forums.thread.pinned" : "forums.thread.unpinned");
                    Notif.AppendInteger(0);
                    Session.SendMessage(Notif);
                }
                if (Thread.Locked != Lock)
                {
                    var Notif2 = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                    Notif2.AppendString(Lock ? "forums.thread.locked" : "forums.thread.unlocked");
                    Notif2.AppendInteger(0);
                    Session.SendMessage(Notif2);
                }
                if (Thread.ParentId != 0) return;
                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumThreadUpdateMessageComposer"));
                Message.AppendInteger(GroupId);
                Message.AppendInteger(Thread.Id);
                Message.AppendInteger(Thread.PosterId);
                Message.AppendString(Thread.PosterName);
                Message.AppendString(Thread.Subject);
                Message.AppendBool(Pin);
                Message.AppendBool(Lock);
                Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                Message.AppendInteger(Thread.MessageCount + 1);
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Message.AppendInteger(1);
                Message.AppendString("");
                Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                Message.AppendByte(Thread.Hidden ? 10 : 1);
                Message.AppendInteger(1);
                Message.AppendString(Thread.Hider);
                Message.AppendInteger(0);
                Session.SendMessage(Message);
            }
        }

        /// <summary>
        /// Alters the state of the forum thread.
        /// </summary>
        internal void AlterForumThreadState()
        {
            var GroupId = Request.GetInteger();
            var ThreadId = Request.GetUInteger();
            var StateToSet = Request.GetInteger();
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery($"SELECT * FROM groups_forums_posts WHERE group_id = '{GroupId}' AND id = '{ThreadId}' LIMIT 1;");
                var Row = dbClient.GetRow();
                var Group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
                if (Row != null)
                {
                    if ((uint) Row["poster_id"] == Session.GetHabbo().Id || Group.Admins.ContainsKey(Session.GetHabbo().Id))
                    {
                        dbClient.SetQuery($"UPDATE groups_forums_posts SET hidden = @hid WHERE id = {ThreadId};");
                        dbClient.AddParameter("hid", StateToSet == 10 ? "1" : "0");
                        dbClient.RunQuery();
                    }
                }
                var Thread = new GroupForumPost(Row);
                var Notif = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                Notif.AppendString(StateToSet == 10 ? "forums.thread.hidden" : "forums.thread.restored");
                Notif.AppendInteger(0);
                Session.SendMessage(Notif);
                if (Thread.ParentId != 0) return;
                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumThreadUpdateMessageComposer"));
                Message.AppendInteger(GroupId);
                Message.AppendInteger(Thread.Id);
                Message.AppendInteger(Thread.PosterId);
                Message.AppendString(Thread.PosterName);
                Message.AppendString(Thread.Subject);
                Message.AppendBool(Thread.Pinned);
                Message.AppendBool(Thread.Locked);
                Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                Message.AppendInteger(Thread.MessageCount + 1);
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Message.AppendString("");
                Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                Message.AppendByte(StateToSet);
                Message.AppendInteger(0);
                Message.AppendString(Thread.Hider);
                Message.AppendInteger(0);
                Session.SendMessage(Message);
            }
        }

        /// <summary>
        /// Reads the forum thread.
        /// </summary>
        internal void ReadForumThread()
        {
            var GroupId = Request.GetInteger();
            var ThreadId = Request.GetUInteger();
            var StartIndex = Request.GetInteger();
            var EndIndex = Request.GetInteger();
            var Group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
            if (Group == null || !Group.HasForum) return;
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery($"SELECT * FROM groups_forums_posts WHERE group_id = '{GroupId}' AND parent_id = '{ThreadId}' OR id = '{ThreadId}' ORDER BY timestamp ASC;");
                var Table = dbClient.GetTable();
                if (Table == null) return;
                var b = Table.Rows.Count <= 20 ? Table.Rows.Count : 20;
                var posts = new List<GroupForumPost>();
                var i = 1;
                while (i <= b)
                {
                    var Row = Table.Rows[i - 1];
                    if (Row == null)
                    {
                        b--;
                        continue;
                    }
                    var thread = new GroupForumPost(Row);
                    if (thread.ParentId == 0 && thread.Hidden) return;
                    posts.Add(thread);
                    i++;
                }

                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumReadThreadMessageComposer"));
                Message.AppendInteger(GroupId);
                Message.AppendInteger(ThreadId);
                Message.AppendInteger(StartIndex);
                Message.AppendInteger(b);
                var indx = 0;
                foreach (var Post in posts)
                {
                    Message.AppendInteger(indx++ - 1);
                    Message.AppendInteger(indx - 1);
                    Message.AppendInteger(Post.PosterId);
                    Message.AppendString(Post.PosterName);
                    Message.AppendString(Post.PosterLook);
                    Message.AppendInteger(Utility.GetUnixTimeStamp() - Post.Timestamp);
                    Message.AppendString(Post.PostContent);
                    Message.AppendByte(0);
                    Message.AppendInteger(0);
                    Message.AppendString(Post.Hider);
                    Message.AppendInteger(0);
                }
                Session.SendMessage(Message);
            }
        }

        /// <summary>
        /// Gets the group forum thread root.
        /// </summary>
        internal void GetGroupForumThreadRoot()
        {
            var GroupId = Request.GetInteger();
            var StartIndex = Request.GetInteger();
            var EndIndex = Request.GetInteger();
            var Group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
            if (Group == null || !Group.HasForum) return;
            using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery($"SELECT * FROM groups_forums_posts WHERE group_id = '{GroupId}' AND parent_id = 0 ORDER BY timestamp DESC;");
                var Table = dbClient.GetTable();
                if (Table == null)
                {
                    var Messages = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumThreadRootMessageComposer"));
                    Messages.AppendInteger(GroupId);
                    Messages.AppendInteger(0);
                    Messages.AppendInteger(0);
                    Session.SendMessage(Messages);
                    return;
                }
                var b = Table.Rows.Count <= 20 ? Table.Rows.Count : 20;
                var Threads = new List<GroupForumPost>();
                var i = 1;
                while (i <= b)
                {
                    var Row = Table.Rows[i - 1];
                    if (Row == null)
                    {
                        b--;
                        continue;
                    }
                    var thread = new GroupForumPost(Row);
                    Threads.Add(thread);
                    i++;
                }
                Threads = Threads.OrderByDescending(x => x.Pinned).ToList();
                var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumThreadRootMessageComposer"));
                Message.AppendInteger(GroupId);
                Message.AppendInteger(StartIndex);
                Message.AppendInteger(b);
                foreach (var Thread in Threads)
                {
                    Message.AppendInteger(Thread.Id);
                    Message.AppendInteger(Thread.PosterId);
                    Message.AppendString(Thread.PosterName);
                    Message.AppendString(Thread.Subject);
                    Message.AppendBool(Thread.Pinned);
                    Message.AppendBool(Thread.Locked);
                    Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                    Message.AppendInteger(Thread.MessageCount + 1);
                    Message.AppendInteger(0);
                    Message.AppendInteger(0);
                    Message.AppendInteger(0);
                    Message.AppendString("");
                    Message.AppendInteger(Utility.GetUnixTimeStamp() - Thread.Timestamp);
                    Message.AppendByte(Thread.Hidden ? 10 : 1);
                    Message.AppendInteger(0);
                    Message.AppendString(Thread.Hider);
                    Message.AppendInteger(0);
                }
                Session.SendMessage(Message);
            }
        }

        /// <summary>
        /// Gets the group forum data.
        /// </summary>
        internal void GetGroupForumData()
        {
            var GroupId = Request.GetInteger();
            var Group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
            if (Group == null || !Group.HasForum) return;
            Session.SendMessage(Group.ForumDataMessage(Session.GetHabbo().Id));
        }

        /// <summary>
        /// Gets the group forums.
        /// </summary>
        internal void GetGroupForums()
        {
            var SelectType = Request.GetInteger();
            var StartIndex = Request.GetInteger();
            var EndIndex = Request.GetInteger();
            var Message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumListingsMessageComposer"));
            Message.AppendInteger(SelectType);
            if (SelectType < 0 || SelectType > 2)
            {
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Message.AppendInteger(0);
                Session.SendMessage(Message);
                return;
            }
            var GroupList = new List<Guild>();
            var FinalGroupList = new List<Guild>();
            switch (SelectType)
            {
                case 0:
                case 1:
                    using (var dbClient = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("SELECT id FROM groups_data WHERE has_forum = '1' AND forum_Messages_count > 0 ORDER BY forum_Messages_count DESC LIMIT 75;");
                        var Table = dbClient.GetTable();
                        if (Table == null) return;
                        Message.AppendInteger(Table.Rows.Count);
                        Message.AppendInteger(StartIndex);
                        var b = Table.Rows.Count <= 20 ? Table.Rows.Count : 20;
                        var Groups = new List<Guild>();
                        var i = 1;
                        while (i <= b)
                        {
                            var Row = Table.Rows[i - 1];
                            if (Row == null)
                            {
                                b--;
                                continue;
                            }
                            var GroupId = int.Parse(Row["id"].ToString());
                            var Guild = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
                            if (Guild == null || !Guild.HasForum)
                            {
                                b--;
                                continue;
                            }
                            Groups.Add(Guild);
                            i++;
                        }
                        Message.AppendInteger(b);
                        foreach (var Group in Groups) Group.SerializeForumRoot(Message);
                        Session.SendMessage(Message);
                    }
                    break;

                case 2:
                    foreach (var GU in Session.GetHabbo().UserGroups)
                    {
                        var AGroup = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GU.GroupId);
                        if (AGroup == null) continue;
                        if (AGroup.HasForum) GroupList.Add(AGroup);
                    }
                    try
                    {
                        FinalGroupList = GroupList.OrderByDescending(x => x.ForumMessagesCount).Skip(StartIndex).Take(20).ToList();
                        Message.AppendInteger(GroupList.Count);
                        Message.AppendInteger(StartIndex);
                        Message.AppendInteger(FinalGroupList.Count);
                    }
                    catch
                    {
                        Message.AppendInteger(0);
                        Message.AppendInteger(0);
                        Message.AppendInteger(0);
                        Session.SendMessage(Message);
                        return;
                    }
                    foreach (var Group in FinalGroupList) Group.SerializeForumRoot(Message);
                    Session.SendMessage(Message);
                    break;
            }
        }

        /// <summary>
        /// Manages the group.
        /// </summary>
        internal void ManageGroup()
        {
            var groupId = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            if (group == null) return;
            if (!@group.Admins.ContainsKey(Session.GetHabbo().Id) && @group.CreatorId != Session.GetHabbo().Id && !Session.GetHabbo().HasFuse("admin")) return;
            Response.Init(LibraryParser.OutgoingRequest("GroupDataEditMessageComposer"));
            Response.AppendInteger(0);
            Response.AppendBool(true);
            Response.AppendInteger(@group.Id);
            Response.AppendString(@group.Name);
            Response.AppendString(@group.Description);
            Response.AppendInteger(@group.RoomId);
            Response.AppendInteger(@group.Colour1);
            Response.AppendInteger(@group.Colour2);
            Response.AppendInteger(@group.State);
            Response.AppendInteger(@group.AdminOnlyDeco);
            Response.AppendBool(false);
            Response.AppendString("");
            var array = @group.Badge.Replace("b", "").Split('s');
            Response.AppendInteger(5);
            var num = 5 - array.Length;

            var num2 = 0;
            var array2 = array;
            foreach (var text in array2)
            {
                Response.AppendInteger(text.Length >= 6 ? uint.Parse(text.Substring(0, 3)) : uint.Parse(text.Substring(0, 2)));
                Response.AppendInteger(text.Length >= 6 ? uint.Parse(text.Substring(3, 2)) : uint.Parse(text.Substring(2, 2)));
                if (text.Length < 5) Response.AppendInteger(0);
                else if (text.Length >= 6) Response.AppendInteger(uint.Parse(text.Substring(5, 1)));
                else Response.AppendInteger(uint.Parse(text.Substring(4, 1)));
            }
            while (num2 != num)
            {
                Response.AppendInteger(0);
                Response.AppendInteger(0);
                Response.AppendInteger(0);
                num2++;
            }
            Response.AppendString(@group.Badge);
            Response.AppendInteger(@group.Members.Count);
            SendResponse();
        }

        /// <summary>
        /// Updates the name of the group.
        /// </summary>
        internal void UpdateGroupName()
        {
            var num = Request.GetInteger();
            var text = Request.GetString();
            var text2 = Request.GetString();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(num);
            if (group == null) return;
            if (group.CreatorId != Session.GetHabbo().Id || !Session.GetHabbo().HasFuse("admin")) return;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery($"UPDATE groups_data SET `name`=@name, `desc`=@desc WHERE id={num} LIMIT 1");
                queryReactor.AddParameter("name", text);
                queryReactor.AddParameter("desc", text2);
                queryReactor.RunQuery();
            }
            group.Name = text;
            group.Description = text2;
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, Session.GetHabbo().CurrentRoom, false);
        }

        /// <summary>
        /// Updates the group badge.
        /// </summary>
        internal void UpdateGroupBadge()
        {
            var guildId = Request.GetInteger();
            var guild = PurpleEmulator.GetGame().GetGroupManager().GetGroup(guildId);
            if (guild.CreatorId != Session.GetHabbo().Id || !Session.GetHabbo().HasFuse("admin")) return;
            if (guild != null)
            {
                var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(guild.RoomId);
                if (room != null)
                {
                    Request.GetInteger();
                    var Base = Request.GetInteger();
                    var baseColor = Request.GetInteger();
                    Request.GetInteger();
                    var guildStates = new List<int>();

                    for (var i = 0; i < 12; i++)
                    {
                        var item = Request.GetInteger();
                        guildStates.Add(item);
                    }
                    var badge = PurpleEmulator.GetGame().GetGroupManager().GenerateGuildImage(Base, baseColor, guildStates);
                    guild.Badge = badge;
                    Response.Init(LibraryParser.OutgoingRequest("RoomGroupMessageComposer"));
                    Response.AppendInteger(room.LoadedGroups.Count);
                    foreach (var current2 in room.LoadedGroups)
                    {
                        Response.AppendInteger(current2.Key);
                        Response.AppendString(current2.Value);
                    }
                    room.SendMessage(Response);
                    PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(guild, Response, Session, room, false);
                    using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.SetQuery($"UPDATE groups_data SET badge = @badgi WHERE id = {guildId}");
                        queryReactor.AddParameter("badgi", badge);
                        queryReactor.RunQuery();
                    }

                    if (Session.GetHabbo().CurrentRoom != null)
                    {
                        Session.GetHabbo().CurrentRoom.LoadedGroups[guildId] = guild.Badge;
                        Response.Init(LibraryParser.OutgoingRequest("RoomGroupMessageComposer"));
                        Response.AppendInteger(Session.GetHabbo().CurrentRoom.LoadedGroups.Count);
                        foreach (var current in Session.GetHabbo().CurrentRoom.LoadedGroups)
                        {
                            Response.AppendInteger(current.Key);
                            Response.AppendString(current.Value);
                        }
                        Session.GetHabbo().CurrentRoom.SendMessage(Response);
                        PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(guild, Response, Session, Session.GetHabbo().CurrentRoom, false);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the group colours.
        /// </summary>
        internal void UpdateGroupColours()
        {
            var groupId = Request.GetInteger();
            var num = Request.GetInteger();
            var num2 = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            if (group == null) return;
            if (group.CreatorId != Session.GetHabbo().Id || !Session.GetHabbo().HasFuse("admin")) return;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat("UPDATE groups_data SET colour1=@num, colour2=@num2 WHERE id=@group_id LIMIT 1"));
                queryReactor.AddParameter("num", num);
                queryReactor.AddParameter("num2", num2);
                queryReactor.AddParameter("group_id", @group.Id);
                queryReactor.RunQuery();
            }
            group.Colour1 = num;
            group.Colour2 = num2;
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, Session.GetHabbo().CurrentRoom, false);
        }

        /// <summary>
        /// Updates the group settings.
        /// </summary>
        internal void UpdateGroupSettings()
        {
            var groupId = Request.GetInteger();
            var num = Request.GetUInteger();
            var num2 = Request.GetUInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            if (group == null) return;
            if (group.CreatorId != Session.GetHabbo().Id || !Session.GetHabbo().HasFuse("admin")) return;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat("UPDATE groups_data SET state=@num, admindeco=@num2 WHERE id=@group_id LIMIT 1"));
                queryReactor.AddParameter("num", num);
                queryReactor.AddParameter("num2", num2);
                queryReactor.AddParameter("group_id", @group.Id);
                queryReactor.RunQuery();
            }
            group.State = num;
            group.AdminOnlyDeco = num2;
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId);
            if (room != null)
            {
                foreach (var current in room.GetRoomUserManager().GetRoomUsers())
                {
                    if (room.RoomData.OwnerId != current.UserId && !group.Admins.ContainsKey(current.UserId) && group.Members.ContainsKey(current.UserId))
                    {
                        if (num2 == 1u)
                        {
                            current.RemoveStatus("flatctrl 1");
                            Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                            Response.AppendInteger(0);
                            current.GetClient().SendMessage(GetResponse());
                        }
                        else
                        {
                            if (num2 == 0u && !current.Statusses.ContainsKey("flatctrl 1"))
                            {
                                current.AddStatus("flatctrl 1", "");
                                Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                                Response.AppendInteger(1);
                                current.GetClient().SendMessage(GetResponse());
                            }
                        }
                        current.UpdateNeeded = true;
                    }
                }
            }
            PurpleEmulator.GetGame().GetGroupManager().SerializeGroupInfo(group, Response, Session, Session.GetHabbo().CurrentRoom, false);
        }

        /// <summary>
        /// Requests the leave group.
        /// </summary>
        internal void RequestLeaveGroup()
        {
            var GroupId = Request.GetInteger();
            var UserId = Request.GetUInteger();
            var Guild = PurpleEmulator.GetGame().GetGroupManager().GetGroup(GroupId);
            if (Guild == null || Guild.CreatorId == UserId) return;
            if (UserId == Session.GetHabbo().Id || Guild.Admins.ContainsKey(Session.GetHabbo().Id))
            {
                Response.Init(LibraryParser.OutgoingRequest("GroupAreYouSureMessageComposer"));
                Response.AppendInteger(UserId);
                Response.AppendInteger(0);
                SendResponse();
            }
        }

        /// <summary>
        /// Confirms the leave group.
        /// </summary>
        internal void ConfirmLeaveGroup()
        {
            var Guild = Request.GetInteger();
            var UserId = Request.GetUInteger();
            var byeGuild = PurpleEmulator.GetGame().GetGroupManager().GetGroup(Guild);
            if (byeGuild == null) return;
            if (byeGuild.CreatorId == UserId)
            {
                Session.SendNotif(TextManager.GetText("user_room_video_true"));
                return;
            }
            var type = 3;
            if (UserId == Session.GetHabbo().Id || byeGuild.Admins.ContainsKey(Session.GetHabbo().Id))
            {
                GroupUser memberShip;
                if (byeGuild.Members.ContainsKey(UserId))
                {
                    memberShip = byeGuild.Members[UserId];
                    type = 3;
                    Session.GetHabbo().UserGroups.Remove(memberShip);
                    byeGuild.Members.Remove(UserId);
                }
                else if (byeGuild.Admins.ContainsKey(UserId))
                {
                    memberShip = byeGuild.Admins[UserId];
                    type = 1;
                    Session.GetHabbo().UserGroups.Remove(memberShip);
                    byeGuild.Admins.Remove(UserId);
                }
                else return;
                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Concat("DELETE FROM groups_members WHERE user_id=", UserId, " AND group_id=", Guild, " LIMIT 1"));
                }
                var byeUser = PurpleEmulator.GetHabboById(UserId);
                if (byeUser != null)
                {
                    Response.Init(LibraryParser.OutgoingRequest("GroupConfirmLeaveMessageComposer"));
                    Response.AppendInteger(Guild);
                    Response.AppendInteger(type);
                    Response.AppendInteger(byeUser.Id);
                    Response.AppendString(byeUser.UserName);
                    Response.AppendString(byeUser.Look);
                    Response.AppendString("");
                    SendResponse();
                }
                if (byeUser != null && byeUser.FavouriteGroup == Guild)
                {
                    byeUser.FavouriteGroup = 0;
                    using (var queryreactor2 = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryreactor2.RunFastQuery($"UPDATE users_stats SET favourite_group=0 WHERE id={UserId} LIMIT 1");
                    var Room = Session.GetHabbo().CurrentRoom;

                    Response.Init(LibraryParser.OutgoingRequest("FavouriteGroupMessageComposer"));
                    Response.AppendInteger(byeUser.Id);
                    if (Room != null) Room.SendMessage(Response);
                    else SendResponse();
                }

                Response.Init(LibraryParser.OutgoingRequest("GroupRequestReloadMessageComposer"));
                Response.AppendInteger(Guild);
                SendResponse();
            }
        }

        /// <summary>
        /// News the method.
        /// </summary>
        /// <param name="current2">The current2.</param>
        private void NewMethod(RoomData current2)
        {
            Response.AppendInteger(current2.Id);
            Response.AppendString(current2.Name);
            Response.AppendBool(false);
        }

        internal void UpdateForumSettings()
        {
            var guild = Request.GetInteger();
            var whoCanRead = Request.GetInteger();
            var whoCanPost = Request.GetInteger();
            var whoCanThread = Request.GetInteger();
            var whoCanMod = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(guild);
            if (group == null) return;
            group.WhoCanRead = whoCanRead;
            group.WhoCanPost = whoCanPost;
            group.WhoCanThread = whoCanThread;
            group.WhoCanMod = whoCanMod;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("UPDATE groups_data SET who_can_read = @who_can_read, who_can_post = @who_can_post, who_can_thread = @who_can_thread, who_can_mod = @who_can_mod WHERE id = @group_id");
                queryReactor.AddParameter("group_id", group.Id);
                queryReactor.AddParameter("who_can_read", whoCanRead);
                queryReactor.AddParameter("who_can_post", whoCanPost);
                queryReactor.AddParameter("who_can_thread", whoCanThread);
                queryReactor.AddParameter("who_can_mod", whoCanMod);
                queryReactor.RunQuery();
            }
            Session.SendMessage(group.ForumDataMessage(Session.GetHabbo().Id));
        }

        internal void DeleteGroup()
        {
            var groupId = Request.GetInteger();
            var group = PurpleEmulator.GetGame().GetGroupManager().GetGroup(groupId);
            var room = PurpleEmulator.GetGame().GetRoomManager().GetRoom(group.RoomId);
            var roomData = room?.RoomData ?? PurpleEmulator.GetGame().GetRoomManager().GenerateRoomData(group.RoomId);

            if (roomData?.Group == null)
            {
                Session.SendNotif(TextManager.GetText("command_group_has_no_room"));
            }
            else if (group.CreatorId == Session.GetHabbo().Id || Session.GetHabbo().Rank > 5)
            {
                foreach (GroupUser user in group.Members.Values)
                {
                    var clientByUserId = PurpleEmulator.GetGame().GetClientManager().GetClientByUserId(user.Id);
                    var habbo = clientByUserId?.GetHabbo();

                    if (habbo == null) continue;

                    habbo.UserGroups.Remove(user);

                    // Remove from the list.
                    Response.Init(LibraryParser.OutgoingRequest("GroupConfirmLeaveMessageComposer"));
                    Response.AppendInteger(group.Id);
                    Response.AppendInteger(3);
                    Response.AppendInteger(habbo.Id);
                    Response.AppendString(habbo.UserName);
                    Response.AppendString(habbo.Look);
                    Response.AppendString("");
                    SendResponse();

                    // Remove from favorites.
                    if (habbo.FavouriteGroup != @group.Id) continue;
                    habbo.FavouriteGroup = 0;

                    var roomOfMember = clientByUserId.GetHabbo()?.CurrentRoom;
                    if (roomOfMember == null) continue;
                    Response.Init(LibraryParser.OutgoingRequest("FavouriteGroupMessageComposer"));
                    Response.AppendInteger(habbo.Id);
                    roomOfMember.SendMessage(Response);
                }

                PurpleEmulator.GetGame().GetGroupManager().DeleteGroup(group.Id);

                roomData.Group = null;
                roomData.GroupId = 0;

                using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery($"UPDATE rooms_data SET group_id = 0 WHERE id = {roomData.Id}");
                }


                if (room != null)
                {
                    room.LoadedGroups.Remove(groupId);

                    var deleteGroup = new ServerMessage(LibraryParser.OutgoingRequest("GroupDeletedMessageComposer"));
                    deleteGroup.AppendInteger(groupId);
                    room.SendMessage(deleteGroup);
                }
            }
        }
    }
}