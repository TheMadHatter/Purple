﻿#region

using System.Collections.Generic;
using System.Linq;
using AzureSharp.HabboHotel.Items;
using AzureSharp.HabboHotel.SoundMachine;
using AzureSharp.HabboHotel.SoundMachine.Composers;
using AzureSharp.Messages.Parsers;

#endregion

namespace AzureSharp.Messages.Handlers
{
    /// <summary>
    /// Class GameClientMessageHandler.
    /// </summary>
    internal partial class GameClientMessageHandler
    {
        /// <summary>
        /// Retrieves the song identifier.
        /// </summary>
        internal void RetrieveSongID()
        {
            var text = Request.GetString();
            var songId = SongManager.GetSongId(text);
            if (songId != 0u)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RetrieveSongIDMessageComposer"));
                serverMessage.AppendString(text);
                serverMessage.AppendInteger(songId);
                Session.SendMessage(serverMessage);
            }
        }

        /// <summary>
        /// Gets the music data.
        /// </summary>
        internal void GetMusicData()
        {
            var num = Request.GetInteger();
            var list = new List<SongData>();

            {
                for (var i = 0; i < num; i++)
                {
                    var song = SongManager.GetSong(Request.GetUInteger());
                    if (song != null) list.Add(song);
                }
                Session.SendMessage(JukeboxComposer.Compose(list));
                list.Clear();
            }
        }

        /// <summary>
        /// Adds the playlist item.
        /// </summary>
        internal void AddPlaylistItem()
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentRoom == null) return;
            var currentRoom = Session.GetHabbo().CurrentRoom;
            if (!currentRoom.CheckRights(Session, true, false)) return;
            var roomMusicController = currentRoom.GetRoomMusicController();
            if (roomMusicController.PlaylistSize >= roomMusicController.PlaylistCapacity) return;
            var num = Request.GetUInteger();
            var item = Session.GetHabbo().GetInventoryComponent().GetItem(num);
            if (item == null || item.BaseItem.InteractionType != Interaction.MusicDisc) return;
            var songItem = new SongItem(item);
            var num2 = roomMusicController.AddDisk(songItem);
            if (num2 < 0) return;
            songItem.SaveToDatabase(currentRoom.RoomId);
            Session.GetHabbo().GetInventoryComponent().RemoveItem(num, true);
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor()) queryReactor.RunFastQuery($"UPDATE items_rooms SET user_id='0' WHERE id={num} LIMIT 1");
            Session.SendMessage(JukeboxComposer.Compose(roomMusicController.PlaylistCapacity, roomMusicController.Playlist.Values.ToList()));
        }

        /// <summary>
        /// Removes the playlist item.
        /// </summary>
        internal void RemovePlaylistItem()
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentRoom == null) return;
            var currentRoom = Session.GetHabbo().CurrentRoom;
            if (!currentRoom.GotMusicController()) return;
            var roomMusicController = currentRoom.GetRoomMusicController();
            var songItem = roomMusicController.RemoveDisk(Request.GetInteger());
            if (songItem == null) return;
            songItem.RemoveFromDatabase();
            Session.GetHabbo().GetInventoryComponent().AddNewItem(songItem.ItemId, songItem.BaseItem.ItemId, songItem.ExtraData, 0, false, true, 0, 0, songItem.SongCode);
            Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery($"UPDATE items_rooms SET user_id='{Session.GetHabbo().Id}' WHERE id='{songItem.ItemId}' LIMIT 1;");
            }
            Session.SendMessage(JukeboxComposer.SerializeSongInventory(Session.GetHabbo().GetInventoryComponent().SongDisks));
            Session.SendMessage(JukeboxComposer.Compose(roomMusicController.PlaylistCapacity, roomMusicController.Playlist.Values.ToList()));
        }

        /// <summary>
        /// Gets the disks.
        /// </summary>
        internal void GetDisks()
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().GetInventoryComponent() == null) return;
            if (Session.GetHabbo().GetInventoryComponent().SongDisks.Count == 0) return;
            Session.SendMessage(JukeboxComposer.SerializeSongInventory(Session.GetHabbo().GetInventoryComponent().SongDisks));
        }

        /// <summary>
        /// Gets the playlists.
        /// </summary>
        internal void GetPlaylists()
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentRoom == null) return;
            var currentRoom = Session.GetHabbo().CurrentRoom;
            if (!currentRoom.GotMusicController()) return;
            var roomMusicController = currentRoom.GetRoomMusicController();
            Session.SendMessage(JukeboxComposer.Compose(roomMusicController.PlaylistCapacity, roomMusicController.Playlist.Values.ToList()));
        }
    }
}