﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureSharp.HabboHotel.GameClients;

namespace AzureSharp.Security
{
    /// <summary>
    /// An very basic WordFilter.
    /// </summary>
    internal static class Filter
    {
        private static bool _loading = false;

        /// <summary>
        /// All the banned words.
        /// </summary>
        internal static Dictionary<string, string> Words = new Dictionary<string, string>();

        /// <summary>
        /// Bobbaifies the given string.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        internal static string Bobbaify(GameClient session, string message)
        {
            return _loading ? message : Words.Aggregate(message, (current, word) => current.Replace(word.Key, word.Value));
        }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        public static void Load()
        {
            Words.Clear();

            _loading = true;
            using (var queryReactor = PurpleEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT word, replacement FROM wordfilter ORDER BY CHAR_LENGTH(word) DESC");

                var table = queryReactor.GetTable();
                if (table == null) return;
                foreach (DataRow dataRow in table.Rows)
                {
                    var word = (string) dataRow["word"];
                    var replacement = dataRow["replacement"] is DBNull ? "bobba" : (string)dataRow["replacement"];
                    Words.Add(word, replacement);
                }
            }
            _loading = false;
        }
    }
}
