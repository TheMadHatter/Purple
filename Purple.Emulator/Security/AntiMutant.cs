﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using AzureSharp.Configuration;

namespace AzureSharp.Security
{
    /// <summary>
    /// AntiMutant.
    /// 
    /// Created by Gerard.
    /// This isn't the best written code, but it does the job.
    /// </summary>
    internal class AntiMutant
    {
        /// <summary>
        /// The set.
        /// </summary>
        private class Set
        {
            public readonly string PaletteId;
            public readonly int Colors;
            public readonly string Gender;

            public Set(string palleteId, int colors, string gender)
            {
                PaletteId = palleteId;
                Colors = colors;
                Gender = gender;
            }
        }

        /// <summary>
        /// The color palletes.
        /// </summary>
        private readonly Dictionary<string, List<string>> _colorPalettes;

        /// <summary>
        /// All the clothes.
        /// </summary>
        private readonly Dictionary<string, Set> _sets;

        public AntiMutant()
        {
            _colorPalettes = new Dictionary<string, List<string>>();
            _sets = new Dictionary<string, Set>();
        }

        /// <summary>
        /// Load the figuredata.
        /// </summary>
        public void Load()
        {
            var webClient = new WebClient();
            var xmlString = webClient.DownloadString(ExtraSettings.FiguredataUrl);

            using (var reader = XmlReader.Create(new StringReader(xmlString)))
            {
                if (reader.ReadToFollowing("colors"))
                {
                    using (var palletteTree = reader.ReadSubtree())
                    {
                        while (palletteTree.ReadToFollowing("palette"))
                        {
                            var id = palletteTree.GetAttribute("id");
                            if (id == null) continue;
                            using (var colorsTree = palletteTree.ReadSubtree())
                            {
                                if (_colorPalettes.ContainsKey(id)) continue;
                                var colors = new List<string>();
                                while (colorsTree.ReadToFollowing("color"))
                                {
                                    var color = colorsTree.GetAttribute("id");
                                    if (colors.Contains(color)) continue;
                                    colors.Add(color);
                                }
                                _colorPalettes.Add(id, colors);
                            }
                        }
                    }
                }

                if (reader.ReadToFollowing("sets"))
                {
                    var settypeTree = reader.ReadSubtree();
                    while (settypeTree.ReadToFollowing("settype"))
                    {
                        var type = settypeTree.GetAttribute("type");
                        var paletteid = settypeTree.GetAttribute("paletteid");
                        if (type == null || paletteid == null) continue;

                        var setTree = reader.ReadSubtree();
                        while (setTree.ReadToFollowing("set"))
                        {
                            var id = setTree.GetAttribute("id");
                            var gender = setTree.GetAttribute("gender");
                            if (id == null || gender == null) continue;
                            if (_sets.ContainsKey(id)) continue;
                            var partTree = reader.ReadSubtree();
                            var colors = 0;
                            while (partTree.ReadToFollowing("part"))
                            {
                                int index;
                                var value = setTree.GetAttribute("colorindex");
                                if (value == null || !int.TryParse(value, out index)) continue;
                                if (index > colors) colors = index;
                            }

                            _sets.Add(id, new Set(paletteid, colors, gender));
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Check if the look is valid.
        /// </summary>
        /// <param name="look"></param>
        /// <param name="gender"></param>
        /// <returns>True if the look is valid.</returns>
        public bool Check(string look, string gender)
        {
            if (gender != "M" && gender != "F") return false;

            var parts = look.Split('.');
            var types = new List<string>();
            foreach (var part in parts)
            {
                var data = part.Split(new[] { '-' }, 3);

                if (data.Length < 3) return false;
                types.Add(data[0]);

                var id = data[1];
                if (!_sets.ContainsKey(id)) return false;

                var set = _sets[id];
                if (!set.Gender.Equals("U") && !gender.Equals(set.Gender)) return false;

                var palette = _colorPalettes[set.PaletteId];
                if (set.Colors > 0)
                {
                    var colors = data[2].Split('-');
                    if (colors.Length != set.Colors) return false;
                    if (colors.Any(color => !palette.Contains(color))) return false;
                }
                else
                {
                    if (data[2].Contains('-') || !palette.Contains(data[2])) return false;
                }
            }

            return types.Contains("lg") && types.Contains("hd") && (gender.Equals("M") || types.Contains("ch"));
        }
    }
}
