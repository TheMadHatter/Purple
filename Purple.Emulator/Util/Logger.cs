﻿using System;

namespace System
{
	public static class Logger
	{
		public static void Info(string str)
		{
			Console.WriteLine (str);
		}

		public static void Debug(string str)
		{
			Console.WriteLine (str);
		}

		public static void Warn(string str)
		{
			Console.WriteLine (str);
		}

		public static void Warn(string str, Exception e)
		{
			Console.WriteLine (str + "\n" + e.ToString());
		}

		public static void Error(string str, Exception e)
		{
			Console.WriteLine (str + "\n" + e.ToString ());
		}

		public static void Error(string str)
		{
			Console.WriteLine (str);
		}

		public static void Fatal(string str, Exception e)
		{
			Console.WriteLine (str + "\n" + e.ToString ());
		}
	}
}

